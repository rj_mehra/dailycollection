global.isLocal = true;
var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var process = require('process');
var users = require('./routes/users');
var ugadmin = require('./routes/ugadmin');
var uglco = require('./routes/uglco');
var lco = require('./routes/lco');
var ugmso = require('./routes/ugmso');
var mso = require('./routes/mso');
var routes = require('./routes/index');
var session = require('express-session');
var LCOLoginModel = require('./models/LCOLogin.js');
var MSOLoginModel = require('./models/MSOLogin.js');
var AdminLoginModel = require('./models/AdminLogin.js');
var CustomerPaymentModel = require('./models/CustomerPayments.js');
var MSODataModel = require('./models/MSOData.js');
var LCODataModel = require('./models/LCOData.js');
var errorLogModal = require('./models/errorLog.js');
var UGLCOLoginModel = require('./models/UGLCOLogin.js');
var MessageLogModel = require('./models/MessageLogs.js');
var CustomerDetailsModel = require('./models/CustomerDetails.js');
var AgentLoginModel = require('./models/AgentLogin.js');
var AgentDataModel = require('./models/AgentData.js');
var request = require('request');
global.moment = require('moment');
var MAIN_URI = "http://103.15.179.45:8085/MessagingGateway/SendTransSMS?Username=CATVCGT&Password=catvt20716&MessageType=txt&Mobile=";
var MOBILE_NO = '7208347027';
var BALANCE = "";
var SENDER_URI = "&SenderID=CATVCG&Message=";
var MESSAGE_TEMPLATE = "ALPHA TO CHARLIE";
require('./util/constants');
global.APP_TIME_ZONE = "Asia/Kolkata";
global.agentLogin = false;
var agent = require('./routes/agent');
var ugagent = require('./routes/ugagent');
var CronJob = require('cron').CronJob;


var sessionOptions = {
    secret: 'kyc_secret',
    resave: true,
    cookie: { maxAge: 60000 * 60 }, //5 mins
    saveUninitialized: false
}

function MSO() {

};

function LCO() {

};

function Admin() {

};

function UGMSO() {

};

function UGLCO() {

};

function UGAdmin() {

};

function Agent() {

};

function sendSMS(mso, template) {
    console.log("MSG SEND");
    console.log(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE);
    request
        .get(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE)
        .on('response', function(response) {
            console.log(response.statusCode); // 200
            console.log(response.headers['content-type']); // 'image/png'
            if (response.statusCode == 200) {
                //res.status(200).json({response:response});
                MessageLogModel.create({
                    MSOId: mso.MSOId,
                    sentTo: mso.phone1,
                    MessageContent: template,
                    MessageType: "EOD Message",
                    status: "Success"
                }, function(err, msg) {
                    if (err) {

                    } else {
                        console.log("EOD message send");
                    }
                });

            }
            /*
            else{
              res.status(500).json({response:response});
            }*/
        })
}
var busboy = require('connect-busboy'); //middleware for form/file upload
var path = require('path'); //used for file path
var fs = require('fs-extra');
var app = express();

// view engine setup
app.use(session(sessionOptions));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(busboy());
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/', lco);
app.use('/', mso);
app.use('/', ugadmin);
app.use('/', uglco);
app.use('/', ugmso);
app.use('/users', users);
app.use('/ugagents', ugagent);
app.use('/agents', agent);
global.message = "";
global.uploaded = false;
global.msoadded = false;
global.lcoadded = false;
global.agentadded = false;

global.sessionInitiated = false;
global.adminSessionInitiated = false;

app.route('/upload')
    .post(function(req, res, next) {
        var fstream;
        req.pipe(req.busboy);
        req.busboy.on('file', function(fieldname, file, filename) {
            console.log("Uploading: " + filename);
            console.log("/public/" + filename);
            //Path where image will be uploaded
            fstream = fs.createWriteStream(__dirname + "/public/excels/" + filename);
            file.pipe(fstream);
            fstream.on('close', function() {
                console.log("Upload Finished of " + filename);
                if (global.DCCustomerUpload) {
                    res.redirect('/save_excel/bulkupload/' + filename);
                } else if (global.GSPCustomerUpload) {
                    res.redirect('/CustomerGSP/save_excel/bulkupload/' + filename);
                }

                //res.redirect('back');           //where to go next
            });
        });
    });

function getEOD(MSOIdArray) {
    var dateNow = moment(new Date()).format("DD/MM/YYYY");
    var startDate = moment().startOf('day');
    var endDate = moment().endOf('day');
    console.log("start and end date : " + startDate + " and " + endDate);
    console.log("MSO Array " + MSOIdArray + " and length : " + MSOIdArray.length);
    for (var i = 0; i < MSOIdArray.length; i++) {
        console.log("in EOD loop for " + MSOIdArray[i]);
        MSODataModel.findOne({ MSOId: MSOIdArray[i] }, function(err, mso) {
            if (err) {
                console.log("MSO Error : " + err);
            } else {
                console.log("MSO Found : " + mso.MSOId + " is " + mso.name)
                CustomerPaymentModel.find({ MSOId: mso.MSOId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } }, function(err, pay) {
                    if (err) {

                    } else {
                        var total_amt = 0;
                        var cash = 0;
                        var cheque = 0;
                        console.log("payment length : " + pay.length);
                        for (var j = 0; j < pay.length; j++) {
                            if (pay[j].mode === "Cash") {
                                cash = cash + parseFloat(pay[j].paidAmount);
                            } else if (pay[j].mode === "Cheque") {
                                cheque = cheque + parseFloat(pay[j].paidAmount);
                            }
                            total_amt = total_amt + parseFloat(pay[j].paidAmount);
                        }
                        CustomerDetailsModel.aggregate(
                            [{
                                    $match: {
                                        MSOId: mso.MSOId
                                    }
                                },
                                {
                                    $group: {
                                        _id: "$MSOId",
                                        count: { $sum: '$balanceAmount' }
                                    }
                                }
                            ],
                            function(err, agent) {
                                if (err) {
                                    console.log("Error ");
                                } else {
                                    if (agent.length > 0) {
                                        console.log("json : " + agent.count)
                                        var os = agent[0].count;
                                    } else {
                                        var os = 0;
                                    }
                                    console.log("os amt : for : " + mso.name + " is " + os);
                                    console.log("total collection for " + mso.name + " is " + total_amt);
                                    MESSAGE_TEMPLATE = "Operator Name : " + mso.name +
                                        "\nCollection Date : " + dateNow + "\n" +
                                        "Total Collection[Cash] : Rs. " + cash +
                                        "\nTotal Collection[Cheque] : Rs. " + cheque +
                                        "\nTotal Collection : Rs. " + total_amt +
                                        "\nTotal Outstanding Amt : Rs. " + os;
                                    MOBILE_NO = mso.phone1;
                                    console.log("phone number : " + MOBILE_NO);
                                    console.log("Message template : " + MESSAGE_TEMPLATE);
                                    sendSMS(mso, MESSAGE_TEMPLATE);
                                }
                            });

                        //  console.log("payment for mso : "+mso.name+" is "+JSON.stringify(pay))
                    }
                })
            }
        })
    }
}
var renewalJob = new CronJob({
    cronTime: '00 00 23 * * *', //00:00:01 .i.e first second of each day
    onTick: function() {
        var MSOIdArray = new Array();

        console.log("in auto Msg");
        var startDate = moment().startOf('day');
        var endDate = moment().endOf('day');
        console.log("start and end date : " + startDate + " and " + endDate);

        MSODataModel.find({ custType: "DC" }, function(err, mso) {
            if (err) {
                console.log("Error  : " + err);
            } else {
                console.log("MSO length and names : " + mso.length + " and \n" + JSON.stringify(mso))
                for (var j = 0; j < mso.length; j++) {
                    console.log("DC MSO Name : " + mso[j].MSOId)
                    MSOIdArray.push(mso[j].MSOId)

                    // CustomerPaymentModel.find({MSOId:MSOName,status:"PAID"},function(err,pay){
                    //   if(err){

                    //   }else{
                    //       console.log("payment for mso : "+MSOName+" is "+JSON.stringify(pay))
                    //   }
                    // })
                }
                getEOD(MSOIdArray);
            }
        })
    },
    start: true, //denotes whether cron job is up or not
    timeZone: 'Asia/Kolkata'
});
renewalJob.start();

function diff_months(dt2, dt1) {

    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
    diff /= (60 * 60 * 24 * 7 * 4);
    return Math.abs(Math.round(diff));

}
var demoJob = new CronJob({
    cronTime: '00 57 11 * * *', //00:00:01 .i.e first second of each day
    onTick: function() {

        dt1 = new Date();
        dt2 = new Date(2015, 02, 28);
        console.log("One : " + diff_months(dt1, dt2));

        dt1 = new Date("June 13, 2014 08:11:00");
        dt2 = new Date("October 19, 2014 11:13:00");
        console.log("Two : " + diff_months(dt1, dt2));
        console.log("moment months : " + moment(new Date()).diff("2015-02-28", "months"))

    },
    start: true, //denotes whether cron job is up or not
    timeZone: 'Asia/Kolkata'
});
demoJob.start();

app.post('/authenticate', function(req, res) {
    var sess = req.session;
    console.log(sess);
    console.log("user name : " + req.body.username);
    MSOLoginModel.findOne({
        MSOId: req.body.username,
        password: req.body.password
    }, function(err, user) {
        if (err) {

        }
        if (!user) {
            LCOLoginModel.findOne({
                LCOId: req.body.username,
                password: req.body.password
            }, function(err, user) {
                if (err) {

                }
                if (!user) {
                    AdminLoginModel.findOne({
                        username: req.body.username,
                        password: req.body.password
                    }, function(err, user) {
                        if (err) {

                        }
                        if (!user) {
                            global.login = false;
                            global.loginError = true;
                            //res.redirect('/');
                            //For Agent
                            AgentLoginModel.findOne({
                                agentId: req.body.username,
                                password: req.body.password
                            }, function(err, user) {
                                if (err) {

                                }
                                if (!user) {
                                    res.redirect('/');
                                } else {
                                    var agent = new Agent();
                                    agent.LCOId = user.LCOId;
                                    AgentDataModel.findOne({ agentId: user.agentId }, function(err, lco1) {
                                        if (err) {

                                        } else {
                                            if (user.custType === "UG") {
                                                console.log("in UG Agent");
                                                global.agentLogin = true;
                                                var uglco = new UGLCO();
                                                uglco.LCOId = user.LCOId;
                                                uglco.name = lco1.name;
                                                uglco.MSOId = user.MSOId;
                                                uglco.agentId = lco1.agentId;
                                                var date = new Date();
                                                date = moment(date).format("DD-MM-YYYY   HH.mm A")
                                                uglco.date = date;
                                                sess.uglco = uglco;
                                                //console.log(shortid.generate());
                                                global.loginError = false;
                                                global.login = true;
                                                console.log(JSON.stringify(sess));
                                                res.redirect('dashboard_UGLCO');
                                            }
                                        }
                                    });

                                }
                            });
                            //End

                        } else {
                            if (user.custType === "DC") {
                                sess.lco = undefined;
                                var admin = new Admin();
                                admin.name = user.username;
                                sess.admin = admin;
                                //console.log(shortid.generate());
                                global.loginError = false;
                                global.login = true;
                                console.log(JSON.stringify(sess));
                                res.redirect('dashboard_admin');
                            } else if (user.custType === "UG") {
                                console.log("in UGSP : " + user.custType);
                                var ugadmin = new UGAdmin();
                                ugadmin.name = user.username;
                                var date = new Date();
                                date = moment(date).format("DD-MM-YYYY   HH.mm A")
                                ugadmin.date = date;
                                sess.ugadmin = ugadmin;
                                global.loginError = false;
                                global.login = true;
                                console.log(JSON.stringify(sess));
                                res.redirect('dashboard_ugadmin');
                            }

                        }
                    });
                } else {
                    var lco = new LCO();
                    lco.LCOId = user.LCOId;
                    LCODataModel.findOne({ LCOId: user.LCOId }, function(err, lco1) {
                        if (err) {

                        } else {
                            if (user.custType == "DC") {
                                console.log("LCO found " + JSON.stringify(lco1));
                                sess.admin = undefined;
                                sess.mso = undefined;
                                sess.lco = lco;
                                lco.LCOId = user.LCOId;
                                lco.MSOId = lco1.MSOId;
                                lco.LCOName = lco1.name;
                                //console.log(shortid.generate());
                                global.loginError = false;
                                global.login = true;
                                console.log(JSON.stringify(sess));
                                res.redirect('dashboard_lco');

                            } else if (user.custType === "UG") {
                                console.log("in UG LCO");
                                var uglco = new UGLCO();
                                uglco.LCOId = user.LCOId;
                                uglco.name = lco1.name;
                                uglco.MSOId = user.MSOId;
                                var date = new Date();
                                date = moment(date).format("DD-MM-YYYY   HH.mm A")
                                uglco.date = date;
                                sess.uglco = uglco;
                                //console.log(shortid.generate());
                                global.loginError = false;
                                global.login = true;
                                console.log(JSON.stringify(sess));
                                global.agentLogin = false;
                                res.redirect('dashboard_UGLCO');
                            }
                        }
                    });

                }
            });
        } else {
            MSODataModel.findOne({ MSOId: user.MSOId }, function(err, user) {
                if (err) {

                } else {
                    if (user.custType == "DC") {
                        var mso = new MSO();
                        mso.MSOId = user.MSOId;
                        sess.mso = mso;
                        global.MSOId = user.MSOId;
                        global.loginErrorMessage = "";
                        global.sessionInitiated = true;
                        console.log(JSON.stringify(sess));
                        global.loginError = false;
                        global.login = true;
                        res.redirect('dashboard_mso');
                    } else if (user.custType === "UG") {
                        var mso = new UGMSO();
                        mso.MSOId = user.MSOId;
                        mso.name = user.name;
                        var date = new Date();
                        date = moment(date).format("DD-MM-YYYY   HH.mm A")
                        mso.date = date;
                        sess.ugmso = mso;
                        global.MSOId = user.MSOId;
                        global.loginErrorMessage = "";
                        global.sessionInitiated = true;
                        console.log(JSON.stringify(sess));
                        global.loginError = false;
                        global.login = true;
                        res.redirect('dashboard_UGMSO');
                    }
                }
            });

        }
    });
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.render('page_404');
    next(err);
});


// load mongoose package
var mongoose = require('mongoose');
// Use native Node promises
mongoose.Promise = global.Promise;



// connect to MongoDB
// mongoose.connect('mongodb://ispadmin:cableguy@$cg104@localhost:27017/ispdb')
mongoose.connect('mongodb://dcadmin:cableguy$cg104@localhost:27017/dcdb')
    .then(() => console.log('connection succesful'))
    .catch((err) => console.error(err));


// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    // res.render('page_500');
    console.log("500 error " + err);
});
var debug = require('debug')('dailycollection:server');
var http = require('http');

/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '14000');
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

module.exports = app;