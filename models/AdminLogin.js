//
//Model for Admin login
var mongoose = require('mongoose');
var AdminLogin = new mongoose.Schema({
    username: String,
    password: String,
    custType: String,
    msgLimit: Number,
    created_at: { type: Date, default: Date.now }
}, { collection: 'AdminLogin' });
module.exports = mongoose.model('AdminLogin', AdminLogin);