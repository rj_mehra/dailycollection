//Model for Agent login
var mongoose = require('mongoose');

//var autoIncrement = require('mongoose-auto-increment');

//var connection = mongoose.createConnection("mongodb://localhost/testapi");

//autoIncrement.initialize(connection);
var AgentLogin = new mongoose.Schema({
    name: String,
    password: String,
    LCOId: String,
    MSOId: String,
    DistId: String,
    custType: String,
    LCOName: String,
    LCOAdd: String,
    MSOName: String,
    agentId: String,
    isOpen: { type: Boolean, default: false },
    IMEI: { type: String, default: 'unassigned' },
    created_at: { type: Date, default: Date.now }
}, { collection: 'AgentLogin' });

module.exports = mongoose.model('AgentLogin', AgentLogin);