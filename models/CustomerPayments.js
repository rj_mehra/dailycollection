var mongoose = require('mongoose');
//String flatNo,bldgName,area,landmark,city,state;
//String contact,altContact,email,operatorName;
//String customerPhotoUrl,idProofUrl,addressProofUrl,signUrl;
var CustomerPaymentDetails = new mongoose.Schema({
    id: String,
    customerId: String,
    name: String,
    address: String,
    plotNo: String,
    paidAmount: String,
    drAmt: { type: String, default: "0" },
    status: { type: String, default: "PAID" },
    balance: String,
    remarks: String,
    netPlotAmount: String,
    ledgerType: { type: String, default: "PAYMENT" },
    regnCharges: { type: String, default: "NA" },
    addnCharges: { type: String, default: "NA" },
    mode: { type: String, default: "Cash" },
    chequeNo: { type: String, default: "NA" },
    bankName: { type: String, default: "NA" },
    agentId: String,
    MSOId: String,
    LCOId: String,
    chequeDate: { type: String, default: "NA" },
    created_at: { type: Date, default: Date.now }
}, { collection: 'CustomerPaymentDetails' });
module.exports = mongoose.model('CustomerPaymentDetails', CustomerPaymentDetails);