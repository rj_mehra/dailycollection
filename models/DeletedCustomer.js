//
//Model for Admin login
var mongoose = require('mongoose');
var DeletedCustomer = new mongoose.Schema({
    LCOId: String,
    MSOId: String,
    customerId: String,
    name: { type: String, default: "NA" },
    address: { type: String, default: "NA" },
    plotNo: { type: String, default: "NA" },
    mobileNo: { type: String, default: "NA" },
    altNo: { type: String, default: "NA" },
    netPlotAmount: { type: String, default: "NA" },
    totalPaidAmount: { type: Number, default: "0" },
    balanceAmount: { type: Number, default: "0" },
    addressProof: { type: String, default: "NA" },
    idProof: { type: String, default: "NA" },
    customerPhoto: { type: String, default: "NA" },
    signProof: { type: String, default: "NA" },
    deletedBy: { type: String },
    created_at: { type: Date, default: Date.now }
}, { collection: 'DeletedCustomer' });
module.exports = mongoose.model('DeletedCustomer', DeletedCustomer);