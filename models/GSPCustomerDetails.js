//
//Model for Admin login
var mongoose = require('mongoose');
var GSPCustomerDetails = new mongoose.Schema({
    LCOId: String,
    MSOId: String,
    DistId: String,
    customerId: String,
    name: { type: String, default: "NA" },
    address: { type: String, default: "NA" },
    phone: { type: String, default: "NA" },
    mobileNo: { type: String, default: "NA" },
    balanceAmount: { type: Number, default: 0 },
    paidAmt: { type: Number, default: 0 },
    planName: { type: String, default: "NA" },
    planAmt: { type: Number, default: 0 },
    sgst: { type: Number, default: 0 },
    cgst: { type: Number, default: 0 },
    igst: { type: Number, default: 0 },
    joinId: { type: String, default: "NA" },
    planDate: { type: String, default: "NA" },
    dueDate: { type: String, default: "NA" },
    tenure: { type: String, default: "NA" },
    salesman: String,
    emiPaid: { type: Number, default: 0 },
    noofemi: { type: Number, default: 0 },
    emi: { type: Number, default: 0 },
    planType: { type: String, default: "in" },
    created_at: { type: Date, default: Date.now }
}, { collection: 'GSPCustomerDetails' });
module.exports = mongoose.model('GSPCustomerDetails', GSPCustomerDetails);