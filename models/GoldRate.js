//Model for Agent Data
var mongoose = require('mongoose');
var GoldRate = new mongoose.Schema({
    id: String,
    price: { type: Number, default: 0 },
    date: { type: Date, default: Date.now }
}, { collection: 'GoldRate' });

module.exports = mongoose.model('GoldRate', GoldRate);