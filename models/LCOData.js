//Model for Agent Data
var mongoose = require('mongoose');
var LCOData = new mongoose.Schema({
    LCOId: String,
    MSOId: String,
    DistId: String,
    name: String,
    address: String,
    phone1: String,
    phone2: String,
    msgLimit: Number,
    password: String,
    contactPerson: String,
    idCreated: { type: Boolean, default: false },
    custType: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'LCOData' });

module.exports = mongoose.model('LCOData', LCOData);