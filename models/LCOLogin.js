//
//Model for Admin login
var mongoose = require('mongoose');
var LCOLogin = new mongoose.Schema({
    LCOId: String,
    MSOId: String,
    DistId: String,
    password: String,
    custType: String,
    msgLimit: Number,
    idCreated: { type: Boolean, default: false },
    created_at: { type: Date, default: Date.now }
}, { collection: 'LCOLogin' });
module.exports = mongoose.model('LCOLogin', LCOLogin);