var mongoose = require('mongoose');
//String flatNo,bldgName,area,landmark,city,state;
//String contact,altContact,email,operatorName;
//String customerPhotoUrl,idProofUrl,addressProofUrl,signUrl;
var LedgerReport = new mongoose.Schema({
    receiptId: String,
    customerId: String,
    name: String,
    address: String,
    LCOId: String,
    MSOId: String,
    plotNo: String,
    netPlotAmount: String,
    paidAmount: String,
    drAmt: { type: String, default: "0" },
    status: { type: String, default: "PAID" },
    ledgerType: { type: String, default: "PAYMENT" },
    revokeAmount: { type: String, default: "0" },
    balance: String,
    agentId: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'LedgerReport' });
module.exports = mongoose.model('LedgerReport', LedgerReport);