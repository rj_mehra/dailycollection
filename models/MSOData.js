//Model for Agent Data
var mongoose = require('mongoose');
var MSOData = new mongoose.Schema({
    MSOId: String,
    name: String,
    address: String,
    phone1: String,
    phone2: String,
    password: String,
    custType: String,
    contactPerson: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'MSOData' });

module.exports = mongoose.model('MSOData', MSOData);