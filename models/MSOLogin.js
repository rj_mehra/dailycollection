//
//Model for Admin login
var mongoose = require('mongoose');
var MSOLogin = new mongoose.Schema({
    MSOId: String,
    password: String,
    custType: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'MSOLogin' });
module.exports = mongoose.model('MSOLogin', MSOLogin);