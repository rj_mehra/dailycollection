//Model for Center Login
var mongoose = require('mongoose');
var MessageLogs = new mongoose.Schema({
    MSOId: String,
    MSOId: { type: String, default: "NA" },
    status: String,
    sentTo: String,
    MessageContent: String,
    MessageType: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'MessageLogs' });
module.exports = mongoose.model('MessageLogs', MessageLogs);