//
//Model for Admin login
var mongoose = require('mongoose');
var PaymentDetails = new mongoose.Schema({
    LCOId: String,
    CustomerId: String,
    name: String,
    address: String,
    plotNo: String,
    mobileNo: String,
    altNo: String,
    netPlotAmount: String,
    totalAmount: String,
    currentPaidAmount: String,
    balanceAmount: String,
    receiptId: String,
    mode: { type: String, default: "Cash" },
    chequeNo: { type: String, default: "NA" },
    bankName: { type: String, default: "NA" },
    chequeDate: { type: String, default: "NA" },
    updated_at: { type: Date, default: Date.now },
    created_at: { type: Date, default: Date.now }
}, { collection: 'PaymentDetails' });
module.exports = mongoose.model('PaymentDetails', PaymentDetails);