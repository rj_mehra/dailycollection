//Model for Agent Data
var mongoose = require('mongoose');
var UGAgentLogin = new mongoose.Schema({
    agentId: String,
    name: String,
    LCOId: String,
    MSOId: { type: String, default: "NA" },
    LCOName: String,
    MSOName: String,
    LCOPhone: String,
    LCOAdd: String,
    MSOAdd: String,
    address: String,
    phone: String,
    password: String,
    custType: String,
    isOpen: { type: Boolean, default: false },
    IMEI: { type: String, default: 'unassigned' },
    created_at: { type: Date, default: Date.now }
}, { collection: 'UGAgentLogin' });

module.exports = mongoose.model('UGAgentLogin', UGAgentLogin);