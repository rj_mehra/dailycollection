//Model for Agent Data
var mongoose = require('mongoose');
var UGDistLogin = new mongoose.Schema({
    DistId: String,
    MSOId: String,
    name: String,
    address: String,
    phone1: String,
    phone2: String,
    password: String,
    custType: String,
    idCreated: { type: Boolean, default: false },
    contactPerson: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'UGDistLogin' });

module.exports = mongoose.model('UGDistLogin', UGDistLogin);