//Model for Agent Data
var mongoose = require('mongoose');
var UGLCOLogin = new mongoose.Schema({
    LCOId: String,
    MSOId: String,
    DistId: String,
    name: String,
    address: String,
    phone1: String,
    phone2: String,
    custType: String,
    password: String,
    idCreated: { type: Boolean, default: false },
    contactPerson: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'UGLCOLogin' });

module.exports = mongoose.model('UGLCOLogin', UGLCOLogin);