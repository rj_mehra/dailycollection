//Model for Agent Data
var mongoose = require('mongoose');
var UGMSOLogin = new mongoose.Schema({
    MSOId: String,
    name: String,
    address: String,
    phone1: String,
    phone2: String,
    password: String,
    custType: String,
    contactPerson: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'UGMSOLogin' });

module.exports = mongoose.model('UGMSOLogin', UGMSOLogin);