var mongoose = require('mongoose');
var Version = new mongoose.Schema({
    appName: String,
    version: String,
    status: String,
    updatedBy: { type: String, default: "ADMIN" },
    created_at: { type: Date, default: Date.now }
}, { collection: 'Version' });

module.exports = mongoose.model('Version', Version);