//
//Model for Admin login
var mongoose = require('mongoose');
var errorLog = new mongoose.Schema({
    LCOId: String,
    custName: String,
    error: String,
    created_at: { type: Date, default: Date.now }
}, { collection: 'errorLog' });
module.exports = mongoose.model('errorLog', errorLog);