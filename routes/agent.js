var express = require('express');
var router = express.Router();
var AgentLoginModel = require('../models/AgentLogin.js');
var AgentDataModel = require('../models/AgentData.js');
var CustomerDetailsModel = require('../models/CustomerDetails.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var CustomerPaymentModel = require('../models/CustomerPayments.js');
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var VersionModel = require('../models/Version.js');
var MessageLogModel = require('../models/MessageLogs.js');
var request = require('request');
var getUid = require('get-uid');
var MAIN_URI = "http://103.15.179.45:8085/MessagingGateway/SendTransSMS?Username=CATVCGT&Password=catvt20716&MessageType=txt&Mobile=";
var MOBILE_NO = '7208347027';
var BALANCE = "";
var SENDER_URI = "&SenderID=CATVCG&Message=";
var MESSAGE_TEMPLATE = "ALPHA TO CHARLIE";
var LCODataModel = require('../models/LCOData.js');
var shortid = require('shortid');

function CustomerDetail() {

}

function customerPayment() {

};

function sendSMS(lco, mobile, template) {
    console.log("MSG SEND");
    console.log(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE);
    request
        .get(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE)
        .on('response', function(response) {
            console.log(response.statusCode); // 200
            console.log(response.headers['content-type']); // 'image/png'
            if (response.statusCode == 200) {

                MessageLogModel.create({
                    MSOId: lco.MSOId,
                    LCOId: lco.LCOId,
                    sentTo: mobile,
                    MessageContent: template,
                    MessageType: "Payment Message",
                    status: "Success"
                }, function(err, msg) {
                    if (err) {

                    } else {
                        console.log("Payment message send");
                    }
                });

            }
            /*else{
              res.status(500).json({response:response});
            }*/
        })
}


function isAgentValid(agentId, IMEI) {
    AgentLoginModel.find({ agentId: agentId, IMEI: IMEI }, function(err, agent) {
        console.log('agent find ' + agent);
        if (agent.length == 1) {
            console.log('agent svalid ');
            return true;
        } else {
            console.log('agent invalid ')
            return false;
        }
    });
}

//authentication for the agent login from android
router.post('/authenticate', function(req, res, next) {
    var requestJSON = req.body;
    console.dir('req body auth ' + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: requestJSON.username, password: requestJSON.password }, function(err, agent) {
        console.dir('agent ' + agent);
        if (err) {
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 0) {
            //Not found
            res.status(403).json({ "status": 403, "agent": agent });
        } else {

            //console.dir(agent[0].username);
            var agentIMEI = agent[0].IMEI;
            var openIMEI = agent[0].isOpen;
            if (openIMEI == true) {
                console.log("in open : " + openIMEI)
                AgentLoginModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                    if (err) {
                        res.status(500).json({ "status": 500 });
                    } else {
                        AgentDataModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                            if (err) {
                                res.status(500).json({ "status": 500 });
                            } else {
                                AgentLoginModel.findOne({ agentId: requestJSON.username, password: requestJSON.password }, function(err, agent) {
                                    if (err) {
                                        res.status(500).json({ "status": 500 });
                                    }
                                    if (agent) {
                                        AgentLoginModel.findOne({
                                            agentId: requestJSON.username
                                        }, function(err, agent) {
                                            if (err) {
                                                res.status(500).json({ "status": 500 });
                                            } else {

                                                res.status(200).json({ "status": 200, agent: agent });
                                            }
                                        })
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                console.log(requestJSON.IMEI);
                console.log("agen IMEI : " + agent[0].IMEI);
                console.log("is agent IMEI open : " + openIMEI);
                AgentLoginModel.find({ IMEI: requestJSON.IMEI }, function(err, agent) {
                    //conflicting
                    if (agent.length > 0) {
                        AgentLoginModel.find({ agentId: requestJSON.username, password: requestJSON.password, IMEI: requestJSON.IMEI },
                            function(err, agent) {
                                if (agent.length == 1) {
                                    AgentDataModel.findOne({ agentId: requestJSON.username },
                                        function(err, agent) {
                                            if (err) {
                                                res.status(500).json({ "status": 500 });
                                            } else {
                                                res.status(200).json({ "status": 200, agent: agent });
                                            }
                                        });

                                } else {
                                    res.status(409).json({ "status": 409 });
                                }
                            });
                    } else {
                        console.log("in unassigned");
                        if (agentIMEI == "unassigned") {
                            AgentLoginModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                                if (err) {
                                    res.status(500).json({ "status": 500 });
                                } else {
                                    AgentDataModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                                        if (err) {
                                            res.status(500).json({ "status": 500 });
                                        } else {
                                            AgentLoginModel.findOne({ agentId: requestJSON.username, password: requestJSON.password },
                                                function(err, agent) {
                                                    if (err) {
                                                        res.status(500).json({ "status": 500 });
                                                    }
                                                    if (agent) {
                                                        AgentDataModel.findOne({
                                                            agentId: requestJSON.username
                                                        }, function(err, agent) {
                                                            if (err) {
                                                                res.status(500).json({ "status": 500 });
                                                            } else {

                                                                res.status(200).json({ "status": 200, agent: agent })
                                                            }
                                                        })
                                                    }
                                                });
                                        }
                                    });
                                }
                            });
                        } else if (agentIMEI !== requestJSON.imei) {
                            if (openIMEI == true) {
                                console.log("in open : " + openIMEI)
                                AgentLoginModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                                    if (err) {
                                        res.status(500).json({ "status": 500 });
                                    } else {
                                        AgentDataModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                                            if (err) {
                                                res.status(500).json({ "status": 500 });
                                            } else {
                                                AgentLoginModel.findOne({ agentId: requestJSON.username, password: requestJSON.password }, function(err, agent) {
                                                    if (err) {
                                                        res.status(500).json({ "status": 500 });
                                                    }
                                                    if (agent) {
                                                        AgentLoginModel.findOne({
                                                            agentId: requestJSON.username
                                                        }, function(err, agent) {
                                                            if (err) {
                                                                res.status(500).json({ "status": 500 });
                                                            } else {

                                                                res.status(200).json({ "status": 200, agent: agent });
                                                            }
                                                        })
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            } else {
                                res.status(409).json({ "status": 409 });
                            }
                        } else {
                            res.status(200).json({ "status": 200, agent: agent[0] });
                        }
                    }

                });
            }

        }
    });

});

router.post('/customerList', function(req, res, next) {
    var agentId = req.body.agentId;
    var IMEI = req.body.imei;
    var LCOId = req.body.LCOId;
    console.dir('req body cust ' + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: agentId, IMEI: IMEI }, function(err, agent) {
        console.log('agent find ' + agent);
        if (agent.length == 1) {
            console.log('agentvalid ');
            var requestJSON = req.body;
            console.dir('req body ' + JSON.stringify(req.body));
            CustomerDetailsModel.find({ LCOId: req.body.LCOId }, function(err, customers) {
                if (err) {
                    res.status(500).json({ "status": 500 });
                } else {
                    var customerArray = new Array();
                    for (var i = 0; i < customers.length; i++) {
                        var customer = new CustomerDetail();
                        customer.customerId = customers[i].customerId;
                        customer.name = customers[i].name;
                        customer.LCOId = customers[i].LCOId;
                        customer.MSOId = customers[i].MSOId;
                        customer.address = customers[i].address;
                        customer.plotNo = customers[i].plotNo;
                        customer.mobileNo = customers[i].mobileNo;
                        customer.altNo = customers[i].altNo;
                        customer.netPlotAmount = customers[i].netPlotAmount;
                        customer.totalPaidAmount = customers[i].totalPaidAmount;
                        customer.balanceAmount = customers[i].balanceAmount;
                        customerArray.push(customer);
                        //console.log(customerArray);
                    }
                    console.log(customerArray[0]);
                    res.status(200).json({ "status": 200, customers: customerArray });
                }
            }).sort({ name: 1 });
        } else {
            res.status(403).json({ "status": 403 });
            console.log('agent invalid ')
            return false;
        }
    });
});


router.post('/payment/customer', function(req, res, next) {
    console.dir("JSON: " + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: req.body.agentId, IMEI: req.body.imei }, function(err, agent) {
        if (err) {
            console.log(err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 1) {
            console.log("in agent");
            CustomerDetailsModel.find({ customerId: req.body.customerId }, function(err, cust) {
                console.log(cust.length)
                if (err) {
                    console.log(err);
                    res.status(500).json({ "status": 500 });
                } else if (cust.length == 1) {
                    console.log("in customer");
                    var receiptId = "CG-" + getUid();
                    var custPay = new customerPayment();
                    custPay.id = receiptId;
                    custPay.customerId = req.body.customerId;
                    custPay.name = req.body.name;
                    custPay.address = req.body.address;
                    custPay.plotNo = req.body.plotNo;
                    custPay.mobileNo = req.body.mobileNo;
                    custPay.altNo = req.body.altNo;
                    custPay.netPlotAmount = req.body.netPlotAmount;
                    custPay.paidAmount = req.body.paidAmount;
                    custPay.mode = req.body.mode;
                    var balance = parseFloat(cust[0].balanceAmount) - parseFloat(req.body.paidAmount);
                    console.log("details : " + cust[0].balanceAmount + " and " + req.body.paidAmount + " balace : " + balance);
                    custPay.balance = balance;
                    custPay.chequeNo = req.body.chequeNo;
                    custPay.bankName = req.body.bankName;
                    custPay.agentId = req.body.agentId;
                    custPay.MSOId = req.body.MSOId;
                    custPay.LCOId = req.body.LCOId;
                    custPay.remarks = req.body.remarks;
                    custPay.chequeDate = req.body.chequeDate;
                    console.log("customer ID : " + custPay.customerId);
                    CustomerPaymentModel.create(
                        custPay,
                        function(err, custPayment) {
                            if (err) {
                                console.log("Error 1 " + err);
                                res.status(500).json({ "status": 500 });
                            } else {
                                console.log("customer ID : " + custPay.customerId);
                                console.log("customer net payable : " + custPay.netPlotAmount);
                                console.log("customer paid amount : " + custPay.paidAmount);
                                CustomerDetailsModel.update({ customerId: custPay.customerId }, { $inc: { balanceAmount: -parseFloat(custPay.paidAmount), totalPaidAmount: parseFloat(custPay.paidAmount) } },
                                    function(err, customer) {
                                        if (err) {
                                            console.log("Error 2" + err);
                                        } else {
                                            console.log("customer : " + JSON.stringify(customer))
                                            console.log("customer ID in update : " + custPay.customerId);
                                            CustomerDetailsModel.findOne({ "customerId": custPay.customerId }, function(err, customerDetails) {
                                                if (err) {
                                                    console.log("Error 3 " + err);
                                                } else {
                                                    console.log("in customer Details");
                                                    console.log("updated Details : " + JSON.stringify(customerDetails));
                                                    custPay.balance = customerDetails.balanceAmount;
                                                    console.log("balance : " + custPay.balance);
                                                    BALANCE = custPay.balance;
                                                    LedgerReportModel.create({
                                                        receiptId: receiptId,
                                                        customerId: custPay.customerId,
                                                        name: custPay.name,
                                                        address: custPay.address,
                                                        plotNo: custPay.plotNo,
                                                        netPlotAmount: custPay.netPlotAmount,
                                                        MSOId: custPay.MSOId,
                                                        LCOId: custPay.LCOId,
                                                        paidAmount: custPay.paidAmount,
                                                        balance: BALANCE,
                                                        agentId: custPay.agentId,
                                                        created_at: custPayment.created_at
                                                    }, function(err, ledger) {
                                                        if (err) {
                                                            console.log("Error 5 : " + err);
                                                        } else {
                                                            console.log("MSO : " + custPay.MSOId)

                                                            LCODataModel.find({ LCOId: custPay.LCOId }, function(err, lco) {
                                                                if (err) {

                                                                } else {
                                                                    MSODataModel.find({ MSOId: lco[0].MSOId }, function(err, mso) {
                                                                        if (err) {

                                                                        } else {
                                                                            console.log("MSO : " + JSON.stringify(mso));

                                                                            console.log(JSON.stringify(ledger));
                                                                            res.status(200).json({ "status": 200, "paymentId": receiptId, "cust": cust, "mso": mso, "lco": lco });

                                                                        }
                                                                    });


                                                                }
                                                            })



                                                        }
                                                    });

                                                }
                                            });
                                        }
                                    });
                                //For Send Payment SMS

                                LCODataModel.findOne({
                                    LCOId: req.body.LCOId,
                                }, function(err, lco) {
                                    if (err) {
                                        console.log("Error 3" + err);

                                    } else {
                                        if (lco) {
                                            console.log("mobile number : " + custPay.mobileNo)
                                            MESSAGE_TEMPLATE = 'Dear Customer, Received with thanks Rs. ' + custPay.paidAmount + '/- In ' + req.body.mode + ', Receipt No. :' +
                                                receiptId + '.Your outstanding balance Rs.' + BALANCE + '/- Thanks from ' + lco.name;

                                            // MESSAGE_TEMPLATE = 'Dear ' + cust.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' + req.body.mode + ', Receipt No. :' +
                                            //     receiptId + '.Your outstanding balance Rs.' + BALANCE + '/- Thanks from ' + lco.name;
                                            //   if(custPay.mode=="Cash"){
                                            //   MESSAGE_TEMPLATE='Dear '+req.body.name+', Received with thanks Rs. '+custPay.paidAmount+'/- In Cash, Receipt No. :'
                                            //   +receiptId+'.Your outstanding balance Rs.'+custPay.balance+'/- Thanks from Isp network';
                                            //   MOBILE_NO=cust.contact;
                                            //   sendSMS(isp);
                                            //
                                            // }
                                            // else{
                                            //   MESSAGE_TEMPLATE='Dear '+req.body.name+', Received with thanks Rs. '+custPay.paidAmount+'/- In Cheque with number' +req.body.chequeNo +' , Receipt No. :'
                                            //   +receiptId+'.Your outstanding balance Rs.'+custPay.balance+'/- Thanks from Isp network';
                                            //   MOBILE_NO=cust.contact;
                                            //   sendSMS(isp);
                                            //
                                            // }
                                            CustomerDetailsModel.findOne({ "customerId": custPay.customerId }, function(err, customerDetails) {
                                                if (err) {
                                                    console.log("Error 4 : " + err);
                                                } else {
                                                    console.log("in customer Details")
                                                    console.log("updated Details : " + JSON.stringify(customerDetails));
                                                    custPay.balance = customerDetails.balanceAmount;
                                                    console.log("balance : " + custPay.balance);
                                                    BALANCE = custPay.balance;
                                                    if (custPay.mode === "Cash") {
                                                        console.log("balance in lco : " + BALANCE)
                                                            // MESSAGE_TEMPLATE = 'Dear ' + custPay.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                            //     req.body.mode + ', Receipt No. :' +
                                                            //     receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                        MESSAGE_TEMPLATE = 'Dear Customer, Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                            req.body.mode + ', Receipt No. :' +
                                                            receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;

                                                    } else if (custPay.mode === "Cheque") {

                                                        MESSAGE_TEMPLATE = 'Dear Customer, Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                            req.body.mode + ' (Bank Name:  ' + custPay.bankName + ', Cheque No. :' + custPay.chequeNo + '),  Receipt No. :' +
                                                            receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                        // MESSAGE_TEMPLATE = 'Dear ' + custPay.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                        //     req.body.mode + ' (Bank Name:  ' + custPay.bankName + ', Cheque No. :' + custPay.chequeNo + '),  Receipt No. :' +
                                                        //     receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                    }
                                                    MOBILE_NO = custPay.mobileNo;
                                                    console.log("mob : " + MOBILE_NO)
                                                    sendSMS(lco, MOBILE_NO, MESSAGE_TEMPLATE);
                                                }
                                            });

                                            // MOBILE_NO="7208347027";
                                        }
                                    }
                                })
                            }
                        });
                } else {
                    console.log("customer not found");
                    res.status(403).json({ "status": 403 });

                }
            });
        }
    });

});

router.post('/terminal_report', function(req, res, next) {
    console.dir("JSON: " + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: req.body.agentId, IMEI: req.body.imei }, function(err, agent) {
        if (err) {
            console.log(err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 1) {
            var agentId = req.body.agentId;
            var startDate = req.body.startDate;
            var endDate = req.body.endDate;
            startDate = moment(startDate).startOf('day').format();
            endDate = moment(endDate).endOf('day').format();
            console.log("dates : " + startDate + " and " + endDate);
            CustomerPaymentModel.find({ agentId: req.body.agentId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } },
                function(err, pay) {
                    if (err) {
                        res.status(500).json({ "status": 500 });
                    } else {
                        var total_amt = 0;
                        var cash = 0;
                        var chequeCount = 0;
                        var cashCount = 0;
                        var cheque = 0;
                        console.log("payment length : " + pay.length);
                        for (var j = 0; j < pay.length; j++) {
                            if (pay[j].mode === "Cash") {
                                cashCount = cashCount + 1;
                                cash = cash + parseFloat(pay[j].paidAmount);
                            } else if (pay[j].mode === "Cheque") {
                                chequeCount = chequeCount + 1;
                                cheque = cheque + parseFloat(pay[j].paidAmount);
                            }
                            total_amt = total_amt + parseFloat(pay[j].paidAmount);
                        }
                        res.status(200).json({ "status": 200, "cashCount": cashCount, "chequeCount": chequeCount, "cash": cash, "cheque": cheque, "total_amt": total_amt });
                    }
                })


        }
    })
});

router.post('/reprint_last_transaction', function(req, res, next) {
    console.dir("JSON: " + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: req.body.agentId, IMEI: req.body.imei }, function(err, agent) {
        if (err) {
            console.log(err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 1) {
            CustomerPaymentModel.find({ agentId: req.body.agentId, status: "PAID" }, function(err, payment) {
                if (err) {
                    res.status(500).json({ "status": 500 });
                    console.log("Error in fetching Last transaction " + err);
                } else {
                    console.log("payment length : " + payment.length);
                    console.log("last transaction : " + JSON.stringify(payment));
                    var recDate = moment(payment[0].created_at).format("DD/MM/YYYY HH.mm.ss");
                    console.log("date : " + recDate);
                    res.status(200).json({ "status": 200, "reprint": payment });
                }

            }).sort({ $natural: -1 }).limit(1);
        }

    })
});

router.post('/version_details', function(req, res, next) {
    VersionModel.findOne({ status: "NEW" }, function(err, version) {
        if (err) {
            res.status(500).json({ "status": 500 });
        } else {
            res.status(200).json({ "status": 200, "version": version });
        }
    })
})

module.exports = router;