var express = require('express');
var router = express.Router();
var fs = require("fs");
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var AgentLoginModel = require('../models/AgentLogin.js');
var AgentDataModel = require('../models/AgentData.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var CustomerDetailsModel = require('../models/CustomerDetails.js');
var CustomerPaymentModel = require('../models/CustomerPayments.js');
var DeletedCustomerDetais = require('../models/DeletedCustomer.js');
var AdminLoginModel = require('../models/AdminLogin.js');
var VersionModel = require('../models/Version');
var MessageLogModel = require('../models/MessageLogs.js');
var pureArray1 = new Array();
var pureArray = new Array();
var Converter = require("csvtojson").Converter;
var converter = new Converter({ constructResult: false });

function DeletedCustomer() {

};

function isMSOSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.mso == undefined)) {
        console.log("req body " + JSON.stringify(req.body));
        global.MSOId = sess.mso.MSOId;
        return true;
    } else {
        return false;
    }
}

function isLCOSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.lco == undefined)) {
        global.LCOId = sess.lco.LCOId;
        global.MSOId = sess.lco.MSOId;
        global.LCOName = sess.lco.name;
        return true;
    } else {
        return false;
    }
}

function isAdminSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.admin == undefined)) {
        console.log("req body " + JSON.stringify(req.body));
        adminName = sess.admin.name;
        return true;
    } else {
        return false;
    }
}


/* GET home page. */
router.get('/', function(req, res, next) {
    res.redirect('sign-in');
});

router.get('/sign-in', function(req, res, next) {
    /*if(global.login){
      res.render('pages/login',{ title: 'ISP App', message:'' });
    }*/

    console.log(req.session);
    if (global.loginError) {
        res.render('login', { title: 'DC App', message: 'Invalid login credentials' });
    } else {
        res.render('login', { title: 'DC App', message: 'Please fill in login credentials' });
    }

});


//Redirect to dashboard_mso
router.get('/dashboard_mso', function(req, res, next) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (isMSOSessionValid(req)) {
        res.render('mso/index', { title: 'DC', message: global.message, name: "name" });
    } else {
        res.redirect('/');
    }
});

//Redirect to dashboard_admin
router.get('/dashboard_admin', function(req, res, next) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (isAdminSessionValid(req)) {

        res.render('admin/index', {
            title: 'DC',
            message: global.message,
            name: "name" //extra added for avoiding crash
        });

    } else {
        res.redirect('/');
    }
});



//Redirect to dashboard_lco
router.get('/dashboard_lco', function(req, res, next) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (isLCOSessionValid(req)) {

        res.render('lco/index', {
            title: 'DC',
            message: global.message,
            name: "name" //extra added for avoiding crash
        });

    } else {
        res.redirect('/');
    }
});


router.get('/add_mso', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {

        res.render('admin/add_MSO', { title: 'Add MSO', mandatorymessage: 'Please fill in the mandatory details' });

    } else {
        res.redirect('/');
    }
});

router.post('/mso_add', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        var custType = req.body.custType;
        var name = req.body.name;
        var MSOId = req.body.msoId;
        var password = req.body.password;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var contactPerson = req.body.contactPerson;
        console.log("Cust type : " + custType);

        if (!(req.body.password === req.body.passwordconfirm)) {
            global.msoAdded = false;
            global.msoAddError = true;
            res.redirect('add_mso');
            message = 'Passwords don\'t match';
        } else {
            MSOLoginModel.find({ MSOId: MSOId }, function(err, MSO) {
                if (err) {
                    console.dir(err);
                    global.msoAdded = false;
                    global.msoAddError = true;
                    res.redirect('add_mso');
                    message = 'Something went wrong';
                }
                /*else if(content.centerName===undefined||content.address===undefined||
                 content.centerID===undefined){
                 //res.status(400).json({"status":400,"error":"Bad Request"});
                }*/
                else if (MSO.length > 0) {
                    global.msoAdded = false;
                    global.msoAddError = true;
                    //if(agent.agentID==agentID)
                    message = 'MSOId already used';
                    console.dir('Mso ' + MSO);
                    res.redirect('add_mso');
                    //res.status(200).json(post);
                } else {
                    MSOLoginModel.create({ MSOId: MSOId, password: password, custType: "DC" }, function(err, post) {

                        var content = req.body;
                        if (err) {
                            console.dir(err);
                            global.msoAdded = false;
                            global.msoAddError = true;;
                            message = 'Something went wrong';
                        }
                        /*else if(content.centerName===undefined||content.address===undefined||
                         content.centerID===undefined){
                         //res.status(400).json({"status":400,"error":"Bad Request"});
                        }*/
                        else {
                            MSODataModel.create({
                                MSOId: MSOId,
                                name: name,
                                address: address,
                                phone1: phone1,
                                phone2: phone2,
                                contactPerson: contactPerson,
                                password: password,
                                custType: "DC"
                            }, function(err, post) {

                                var content = req.body;
                                if (err) {
                                    console.dir(err);
                                    global.msoAdded = false;
                                    global.msoAddError = true;;
                                    message = 'Something went wrong';
                                }
                                /*else if(content.centerName===undefined||content.address===undefined||
                                 content.centerID===undefined){
                                 //res.status(400).json({"status":400,"error":"Bad Request"});
                                }*/
                                else {


                                    global.msoAdded = true;
                                    res.redirect('add_mso');
                                    message = 'MSO Added Successfully';
                                    //res.status(200).json(post);
                                }
                            });

                        }
                    });
                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/add_lco', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        MSODataModel.find({ custType: "DC" }, function(err, data) {
            if (err) return next(err);
            else {
                msoArray = new Array();
                console.dir("data " + JSON.stringify(data));
                for (var i = 0; i < data.length; i++) {
                    msoArray.push(data[i]);
                }
                console.dir('isdArray ' + msoArray);
                if (global.lcoAdded) {
                    res.render('admin/add_LCO', { title: 'Add LCO', msos: data, message: "LCO Added Successfully" });
                }
                if (global.lcoAddError) {
                    res.render('admin/add_LCO', { title: 'Add LCO', msos: msoArray, message: message });
                } else {
                    res.render('admin/add_LCO', { title: 'Add LCO', msos: msoArray, message: "" });
                }
            }
        });
    } else {
        res.redirect('/sign-in');
    }
});



router.post('/lco_add', function(req, res, next) {
    var sess = req.session;
    //console.log(JSON.stringify(req.session));
    if (isAdminSessionValid(req)) {
        var custType = req.body.custType;
        var name = req.body.name;
        var LCOId = req.body.LCOId;
        var password = req.body.password;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var contactPerson = req.body.contactPerson;
        var MSODetails = req.body.MSOId;
        var array = MSODetails.split('/');
        if (!(req.body.password === req.body.passwordconfirm)) {
            global.lcoAdded = false;
            global.lcoAddError = true;;
            res.redirect('add_lco');
            message = 'Passwords don\'t match';
        } else {
            LCOLoginModel.find({ LCOId: LCOId }, function(err, Lco) {
                if (err) {
                    console.dir(err);
                    global.lcoAdded = false;
                    global.lcoAddError = true;;
                    res.redirect('add_lco');
                    message = 'Something went wrong';
                }
                /*else if(content.centerName===undefined||content.address===undefined||
                 content.centerID===undefined){
                 //res.status(400).json({"status":400,"error":"Bad Request"});
                }*/
                else if (Lco.length > 0) {
                    global.lcoAdded = false;
                    global.lcoAddError = true;
                    //if(agent.agentID==agentID)
                    message = 'LCO Id already used';
                    console.dir('LCO ' + Lco);
                    res.redirect('add_lco');
                    //res.status(200).json(post);
                } else {
                    LCOLoginModel.create({ LCOId: LCOId, password: password, MSOId: array[0], MSOName: array[1], custType: "DC" }, function(err, post) {

                        var content = req.body;
                        if (err) {
                            console.dir(err);
                            global.lcoAdded = false;
                            global.lcoAddError = true;;
                            message = 'Something went wrong';
                        }
                        /*else if(content.centerName===undefined||content.address===undefined||
                         content.centerID===undefined){
                         //res.status(400).json({"status":400,"error":"Bad Request"});
                        }*/
                        else {
                            //  var eodOption=false,smsOption=false,emailOption=false;
                            //  if(eod!==undefined){
                            //    eodOption=true;
                            //  }
                            //  if(sms!==undefined){
                            //    smsOption=true;
                            //  }
                            //  if(email!==undefined){
                            //    emailOption=true;
                            //  }
                            LCODataModel.create({
                                LCOId: LCOId,
                                MSOId: array[0],
                                name: name,
                                address: address,
                                phone1: phone1,
                                phone2: phone2,
                                contactPerson: contactPerson,
                                password: password,
                                custType: "DC"

                            }, function(err, post) {

                                var content = req.body;
                                if (err) {
                                    console.dir(err);
                                    global.lcoAdded = false;
                                    global.lcoAddError = true;;
                                    message = 'Something went wrong';
                                }
                                /*else if(content.centerName===undefined||content.address===undefined||
                                 content.centerID===undefined){
                                 //res.status(400).json({"status":400,"error":"Bad Request"});
                                }*/
                                else {


                                    res.redirect('add_lco');
                                    global.lcoAdded = true;
                                    //res.status(200).json(post);
                                }
                            });

                        }
                    });
                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }

});

router.get('/list_lco', function(req, res, next) {
    if (isAdminSessionValid(req)) {
        LCODataModel.find({ custType: "DC" }, function(err, lcos) {
            if (err) {
                console.log(err);
            } else {
                res.render('admin/LCO_list', { title: 'LCO List', lcos: lcos, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/list_mso', function(req, res, next) {
    if (isAdminSessionValid(req)) {
        MSODataModel.find({ custType: "DC" }, function(err, msos) {
            if (err) {
                console.log(err);
            } else {
                res.render('admin/MSO_list', { title: 'MSO List', msos: msos, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/list_agent', function(req, res, next) {
    if (isAdminSessionValid(req)) {
        AgentDataModel.find({ custType: "DC" }, function(err, agents) {
            if (err) {
                console.log(err);
            } else {
                res.render('admin/agent_list', { title: 'Agent List', message: "", agents: agents, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/add_agent', function(req, res, next) {
    var sess = req.session;
    //console.log(JSON.stringify(req.session));
    if (isAdminSessionValid(req)) {
        LCODataModel.find({ custType: "DC" }, function(err, data) {
            if (err) return next(err);
            else {
                lcoArray = new Array();
                for (var i = 0; i < data.length; i++) {
                    lcoArray.push(data[i]);
                }
                if (global.agentAdded) {
                    res.render('admin/add_agent', { title: 'Add Agent', lcos: lcoArray, message: "Agent Added Successfully" });
                }
                if (global.agentAddError) {
                    res.render('admin/add_agent', { title: 'Add Agent', lcos: lcoArray, message: message });
                } else {
                    res.render('admin/add_agent', { title: 'Add Agent', lcos: lcoArray, message: "" });
                }
                console.dir('lcoArray ' + lcoArray);
            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.post('/agent_add', function(req, res, next) {
    var sess = req.session;
    //console.log(JSON.stringify(req.session));
    if (isAdminSessionValid(req)) {
        var custType = req.body.custType;
        var name = req.body.name;
        var agentId = req.body.agentId;
        var password = req.body.password;
        var LCOId = req.body.LCOId;
        var address = req.body.address;
        var openIMEI = req.body.imei;
        var phone = req.body.phone;
        var array = LCOId.split('/');
        if (!(req.body.password === req.body.passwordconfirm)) {
            global.agentAdded = false;
            global.agentAddError = true;;
            res.redirect('add_agent');
            message = 'Passwords don\'t match';
        } else {
            AgentLoginModel.find({ agentId: agentId }, function(err, Agent) {
                if (err) {
                    console.dir(err);
                    global.agentAdded = false;
                    global.agentAddError = true;;
                    res.redirect('add_agent');
                    message = 'Something went wrong';
                }
                /*else if(content.centerName===undefined||content.address===undefined||
                 content.centerID===undefined){
                 //res.status(400).json({"status":400,"error":"Bad Request"});
                }*/
                else if (Agent.length > 0) {
                    global.agentAdded = false;
                    global.agentAddError = true;
                    //if(agent.agentID==agentID)
                    message = 'Agent Id already used';
                    console.dir('Agent ' + Agent);
                    res.redirect('add_agent');
                    //res.status(200).json(post);
                } else {
                    LCODataModel.findOne({ LCOId: array[0] }, function(err, lco) {
                        if (err) {

                        } else {

                            MSODataModel.findOne({ MSOId: lco.MSOId }, function(err, mso) {
                                if (err) {

                                } else {
                                    var openIMEIo = false;
                                    if (openIMEI !== undefined) {
                                        openIMEIo = true;
                                    }
                                    console.log("MSO : " + lco.MSOId + "mso name : " + mso.name + "LCO id :" + lco.LCOId + "lco nam :" + lco.name)
                                    AgentLoginModel.create({
                                        agentId: agentId,
                                        password: password,
                                        MSOId: lco.MSOId,
                                        LCOName: lco.name,
                                        MSOName: mso.name,
                                        LCOId: array[0],
                                        name: name,
                                        IMEI: "unassigned",
                                        isOpen: openIMEIo,
                                        custType: "DC"
                                    }, function(err, post) {

                                        var content = req.body;
                                        if (err) {
                                            console.dir(err);
                                            global.agentAdded = false;
                                            global.agentAddError = true;
                                            message = 'Something went wrong';
                                        }
                                        /*else if(content.centerName===undefined||content.address===undefined||
                                         content.centerID===undefined){
                                         //res.status(400).json({"status":400,"error":"Bad Request"});
                                        }*/
                                        else {
                                            AgentDataModel.create({
                                                agentId: agentId,
                                                name: name,
                                                LCOPhone: lco.phone1,
                                                MSOId: lco.MSOId,
                                                LCOAdd: lco.address,
                                                MSOAdd: mso.address,
                                                LCOName: lco.name,
                                                MSOName: mso.name,
                                                LCOId: array[0],
                                                address: address,
                                                phone: phone,
                                                IMEI: "unassigned",
                                                isOpen: openIMEIo,
                                                password: password,
                                                custType: "DC"
                                            }, function(err, post) {

                                                var content = req.body;
                                                if (err) {
                                                    console.dir(err);
                                                    global.agentAdded = false;
                                                    global.agentAddError = true;;
                                                    message = 'Something went wrong';
                                                }
                                                /*else if(content.centerName===undefined||content.address===undefined||
                                                 content.centerID===undefined){
                                                 //res.status(400).json({"status":400,"error":"Bad Request"});
                                                }*/
                                                else {


                                                    res.redirect('add_agent');
                                                    global.agentAdded = true;
                                                    //res.status(200).json(post);
                                                }
                                            });

                                        }
                                    });
                                }
                            })


                        }
                    })

                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }

});


router.get('/bulk_upload', function(req, res, next) {
    if (isLCOSessionValid(req)) {

        global.GSPCustomerUpload = false;
        global.DCCustomerUpload = true;
        LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
            if (err) {

            } else {
                global.msoId = lco.MSOId;
                console.log("mso for lco : " + lco.MSOId);
                if (global.uploaded) {
                    res.render('lco/bulk_upload', { title: 'Bulk Upload', uploadMessage: "Successful upload", filename: filename, filepath: filepath, message: "Please select a file to upload" });
                } else {
                    res.render('lco/bulk_upload', { title: 'Bulk Upload', uploadMessage: "Select file to upload", filename: undefined, filepath: undefined, message: "Please select a file to upload" });
                }
            }
        })

    }
});

router.get('/save_excel/bulkupload/:filename', function(req, res, next) {
    // console.log(req.body.lconame);
    filename = req.params.filename;
    console.log("file name : " + filename);
    var csvConverter = new Converter({ constructResult: true });
    pureArray1 = new Array();
    var fileStream = fs.createReadStream("public/excels/" + filename);

    csvConverter.on("end_parsed", function(jsonObj) {
        var length = Object.keys(jsonObj).length;
        console.log("json length : " + Object.keys(jsonObj).length);
        for (var i = 0; i < length; i++) {
            console.log("values : " + JSON.stringify(jsonObj[i]));
            pureArray1.push(jsonObj[i]);
        }
        console.log(pureArray1);
        console.log("Pure Array waala length starting : " + pureArray1.length);
        if (saveInDBBulkUpload()) {
            console.log('DONE');
            //res.redirect('http://localhost:5000/bulk_upload');
            res.redirect('http://35.154.191.8:5000/bulk_upload');
            filename = req.params.filename;
            filepath = "http://35.154.191.8:5000/excels/" + filename;
            //filepath = "http://35.154.191.8:5000/excels/"+ filename;
            console.log(filepath);
            global.uploaded = true;
        } else {
            filename = undefined;
            global.uploadMessage = "Invalid Excel for Collection Upload";

            //res.redirect('http://localhost:5000/bulk_upload');
            res.redirect('http://35.154.191.8:5000/bulk_upload');

            console.log("Invalid Excel");
        }


    });
    fileStream.pipe(csvConverter);
});


//Save the information in the database
function saveInDBBulkUpload() {

    console.log("Customer Details Length " + pureArray1.length);
    for (var i = 0; i < pureArray1.length; i++) {
        var row = pureArray1[i];
        var jsonData = JSON.stringify(row);
        row.LCOId = global.LCOId;


        row.MSOId = global.msoId;
        CustomerDetailsModel.create(pureArray1[i], function(err, post) {
            if (err) {
                console.log("err= " + err);
                //return false;
            } else {}
        });

    }
    console.log("Array " + JSON.stringify(pureArray1[0]));

    pureArray1 = [];
    return true;

};

router.get('/cust_list', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        CustomerDetailsModel.find({ LCOId: global.LCOId }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                res.render('lco/subscriber_list', { title: 'Subscriber List', subs: cust, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});





router.get('/signout', function(req, res, next) {
    var sess = req.session;
    if (sess.admin != undefined) {
        sess.admin = undefined;
    } else if (sess.center != undefined) {
        sess.center = undefined;
    }
    res.render('signout');
});
router.get('/signoutlco', function(req, res, next) {
    var sess = req.session;
    if (sess.lco != undefined) {
        sess.lco = undefined;
    } else if (sess.center != undefined) {
        sess.center = undefined;
    }
    res.render('signout');
});
router.get('/signoutmso', function(req, res, next) {
    var sess = req.session;
    if (sess.mso != undefined) {
        sess.mso = undefined;
    } else if (sess.center != undefined) {
        sess.center = undefined;
    }
    res.render('signout');
});

//MSO

///mso_cust_list
router.get('/mso_cust_list', function(req, res, next) {
    if (isMSOSessionValid(req)) {
        CustomerDetailsModel.aggregate(
            [{
                    $match: {
                        MSOId: global.MSOId
                    }
                },
                {
                    $group: {
                        _id: "$LCOId",
                        count: { $sum: 1 }
                    }
                }
            ],
            function(err, lco) {
                if (err) {
                    console.log("Error ");
                } else {
                    console.log("customerDetails details : " + JSON.stringify(lco));
                    res.render('mso/lco_customerlist', { title: 'LCO Subscriber Count', lco: lco, message: global.manipulateMessage });

                }
            });
    } else {
        res.redirect('sign-in');
    }

});

//mso_customer
router.get('/mso_customer', function(req, res) {
    var lcoId = req.param('lcoId');
    global.selectedLCO = lcoId;
    console.log(lcoId);
    CustomerDetailsModel.find({ LCOId: lcoId }, function(err, cust) {
        if (err) {
            console.log(err);
        } else {
            res.render('mso/subscriber_list', { title: 'Subscriber List', subs: cust });

        }
    });
});

//Added delete customer by Raj

router.post('/customer_manipulate', function(req, res, next) {
    if (isMSOSessionValid(req)) {
        console.log("customer id : " + req.body.subsValue);
        var cust = req.body.subsValue;
        if (Array.isArray(cust)) {
            console.log("customers : " + cust);
        }
        var manipulate = req.body.manipulate;
        if (manipulate === "payment_details") {
            CustomerPaymentModel.find({ customerId: cust }, function(err, payment) {
                if (err) {
                    console.log(err);
                } else {
                    LedgerReportModel.find({ customerId: cust }, function(err, customer) {
                        if (err) {
                            console.log(err);
                        } else {

                            res.render('mso/lco_customerpayments', { title: 'Payment Ledger', pays: payment, cust: customer });
                        }
                    });


                }
            });
        } else if (manipulate === "delete") {
            CustomerDetailsModel.findOne({ customerId: req.body.subsValue }, function(err, custs) {
                if (err) {
                    console.log("error in fetching cust : " + err);
                } else {
                    var cust = new DeletedCustomer();
                    cust.customerId = custs.customerId;
                    cust.LCOId = custs.LCOId;
                    cust.MSOId = custs.MSOId;
                    cust.signProof = custs.signProof;
                    cust.customerPhoto = custs.customerPhoto;
                    cust.idProof = custs.idProof;
                    cust.addressProof = custs.addressProof;
                    cust.balanceAmount = custs.balanceAmount;
                    cust.totalPaidAmount = custs.totalPaidAmount;
                    cust.netPlotAmount = custs.netPlotAmount;
                    cust.altNo = custs.altNo;
                    cust.mobileNo = custs.mobileNo;
                    cust.plotNo = custs.plotNo;
                    cust.name = custs.name;

                    DeletedCustomerDetais.create(cust, function(err, customer) {
                        if (err) {
                            console.log("err in deleting" + err);
                        } else {
                            CustomerDetailsModel.remove({ customerId: req.body.subsValue }, function(err, dele) {
                                if (err) {

                                } else {
                                    console.log("deleted");
                                    CustomerDetailsModel.find({ LCOId: global.selectedLCO }, function(err, cust) {
                                        if (err) {

                                        } else {
                                            res.render('mso/subscriber_list', { title: 'Customer List', message: "Customer Removed Successfully", subs: cust });
                                        }
                                    })
                                }
                            })

                        }
                    })
                }
            })
        } else if (manipulate === "custList") {
            CustomerDetailsModel.find({ LCOId: global.selectedLCO }, function(err, cust) {
                if (err) {

                } else {
                    res.render('mso/subscriber_list', { title: 'Customer List', message: "", subs: cust });
                }
            })
        } else {
            res.redirect('sign-in');
        }
    } else {
        res.redirect('sign-in');
    }

});

//agent_manipulate

router.post('/agent_manipulate', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        var agentId = req.body.agentId;
        console.log("agent ID : " + agentId)
        var action = req.body.manipulate;
        console.log("action to b taken  : " + action);
        if (action == "agent_IMEI") {
            AgentLoginModel.update({ agentId: agentId }, { $set: { IMEI: "unassigned" } }, function(err, agentUpdate) {
                if (err) {

                } else {
                    AgentDataModel.update({ agentId: agentId }, { $set: { IMEI: "unassigned" } }, function(err, agentUpdate) {
                        if (err) {

                        } else {
                            AgentDataModel.find({ custType: "DC" }, function(err, agents) {
                                if (err) {

                                } else {
                                    res.render('admin/agent_list', { title: 'Agent List', message: "IMEI Reset Successfully", agents: agents });
                                }
                            })
                        }
                    })


                }
            })
        } else if (action === "agent_edit") {
            console.log("in agent manipulation of " + agentId);
            AgentDataModel.findOne({ agentId: agentId }, function(err, agent) {
                if (err) {
                    console.log("error : " + err);
                } else {
                    res.render('admin/edit_agent', { title: 'Edit Agent', message: "", agent: agent });
                }
            })

        }
    } else {
        res.redirect('sign-in');
    }
})

router.post('/edit_agent', function(req, res, next) {
    var sess = req.session;
    var agentId = req.body.agentId;
    var name = req.body.name;
    var address = req.body.address;
    var phone = req.body.phone;

    var openIMEI = req.body.imei;
    if (isAdminSessionValid(req)) {
        var openIMEIo = false;
        if (openIMEI !== undefined) {
            openIMEIo = true;
        }
        console.log("agent edit : " + req.body.agentId);
        AgentDataModel.update({ agentId: agentId }, { $set: { name: name, phone: phone, isOpen: openIMEIo } }, function(err, agentU) {
            if (err) {

            } else if (!agentU) {
                AgentDataModel.findOne({ agentId: agentId }, function(err, agent) {
                    if (err) {

                    } else {
                        res.render('admin/edit_agent', { title: 'Edit Agent', message: "Failed to Update Try again", agent: agent });
                    }
                })
            } else {
                AgentLoginModel.update({ agentId: agentId }, { $set: { name: name, isOpen: openIMEIo } }, function(err, agentU) {
                    if (err) {

                    } else {
                        AgentDataModel.findOne({ agentId: agentId }, function(err, agent) {
                            if (err) {

                            } else {

                                res.render('admin/edit_agent', { title: 'Edit Agent', message: "Agent Updated Successfully", agent: agent });
                            }
                        })
                    }
                })

            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_version_details', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        global.insertVersion = false;
        global.versionDetails = false;

        res.render('admin/version_details', { title: 'Version Details', sectionTitle: "App Version", sectionMessage: "" });

    } else {
        res.redirect('/sign-in');
    }
})

router.get('/enable_Insert_version', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        global.versionDetails = false;
        global.insertVersion = true;
        res.render('admin/version_details', { title: 'Version Details', sectionTitle: "App Version", sectionMessage: "" });
    } else {
        res.redirect('/sign-in');
    }
})

router.post('/insert_version', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        //version
        console.log("New Version : " + req.body.version)
        VersionModel.findOne({ version: req.body.version }, function(err, ver) {
            if (err) {

            } else if (!ver) {
                VersionModel.updateMany({ appName: "DC" }, { $set: { status: "OLD" } }, function(err, update) {
                    if (err) {

                    } else {
                        VersionModel.create({ appName: "DC", version: req.body.version, status: "NEW" }, function(err, version) {
                            if (err) {

                            } else {
                                global.versionDetails = false;
                                global.insertVersion = true;
                                res.render('admin/version_details', { title: 'Version Details', sectionTitle: "App Version", sectionMessage: "Application Version Updated Successfully" });
                            }
                        })

                    }
                })

            } else {
                res.render('admin/version_details', { title: 'Version Details', sectionTitle: "App Version", sectionMessage: "Application Version already existing" });
            }
        })

    } else {
        res.redirect('/sign-in');
    }
})

router.get('/get_app_versions', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        VersionModel.find(function(err, version) {
            if (err) {

            } else {
                global.insertVersion = false;
                global.versionDetails = true;
                console.log("versin Details : " + JSON.stringify(version));
                res.render('admin/version_details', { title: 'Version Details', sectionTitle: "App Version", ver: version, sectionMessage: "" });
            }
        })


    } else {
        res.redirect('/sign-in');
    }
})
router.get('/get_password_admin', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        res.render('admin/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/change_password_admin', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        var password = req.body.oldpassword;
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;

        if (newpassword !== confirmpassword) {
            res.render('admin/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "Password and confirm password do no match" });
        } else {
            AdminLoginModel.findOne({ username: global.adminName }, function(err, mso) {
                if (err) {

                } else {
                    if (password !== mso.password) {
                        res.render('admin/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "Incorrect Password entered" });
                    } else {
                        AdminLoginModel.update({ username: global.adminName }, { $set: { password: newpassword } }, function(err, msoUpdate) {
                            if (err) {

                            } else {
                                AdminLoginModel.update({ username: global.adminName }, { $set: { password: newpassword } }, function(err, data) {
                                    if (err) {

                                    } else {
                                        global.changePassword = true;
                                        res.redirect('sign-in');
                                    }
                                })
                            }
                        })
                    }
                }
            });
        }
    } else {
        res.redirect('sign-in');
    }
})

router.get('/admin_mso_customers', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        MSODataModel.find({ custType: "DC" }, function(err, msos) {
            if (err) {
                console.log("Error in fetching MSO : " + err);
            } else {
                res.render('admin/mso_customer_list', { title: 'MSO List', message: "", mso: msos });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/get_admin_mso_customer', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        console.log("selected MSO : " + req.body.MSOId);
        if (req.body.MSOId !== undefined) {
            global.SelectedMSO = req.body.MSOId;
        }

        CustomerDetailsModel.find({ MSOId: global.SelectedMSO }, function(err, cust) {
            if (err) {
                console.log("Error in fetching cust MSO : " + err);
            } else {
                res.render('admin/sub_list', { title: 'Customer List', message: "", subs: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/admin_delete_customer', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        console.log("selected customer : " + req.body.subsValue);
        var manipulate = req.body.manipulate;
        console.log("mani : " + manipulate);
        if (manipulate == "delete") {
            CustomerDetailsModel.findOne({ customerId: req.body.subsValue }, function(err, custs) {
                if (err) {
                    console.log("error in fetching cust : " + err);
                } else {
                    var cust = new DeletedCustomer();
                    cust.customerId = custs.customerId;
                    cust.LCOId = custs.LCOId;
                    cust.MSOId = custs.MSOId;
                    cust.signProof = custs.signProof;
                    cust.customerPhoto = custs.customerPhoto;
                    cust.idProof = custs.idProof;
                    cust.addressProof = custs.addressProof;
                    cust.balanceAmount = custs.balanceAmount;
                    cust.totalPaidAmount = custs.totalPaidAmount;
                    cust.netPlotAmount = custs.netPlotAmount;
                    cust.altNo = custs.altNo;
                    cust.mobileNo = custs.mobileNo;
                    cust.plotNo = custs.plotNo;
                    cust.name = custs.name;

                    DeletedCustomerDetais.create(cust, function(err, customer) {
                        if (err) {
                            console.log("err in deleting" + err);
                        } else {
                            CustomerDetailsModel.remove({ customerId: req.body.subsValue }, function(err, dele) {
                                if (err) {

                                } else {
                                    console.log("deleted");
                                    CustomerDetailsModel.find({ MSOId: global.SelectedMSO }, function(err, cust) {
                                        if (err) {

                                        } else {
                                            res.render('admin/sub_list', { title: 'Customer List', message: "Customer Removed Successfully", subs: cust });
                                        }
                                    })
                                }
                            })

                        }
                    })
                }
            })
        } else if (manipulate == "custList") {
            CustomerDetailsModel.find({ MSOId: global.SelectedMSO }, function(err, cust) {
                if (err) {

                } else {
                    res.render('admin/sub_list', { title: 'Customer List', message: "", subs: cust });
                }
            })
        }
    } else {
        res.redirect('sign-in');
    }
})


router.get('/admin_mso_del_customers', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        MSODataModel.find({ custType: "DC" }, function(err, msos) {
            if (err) {
                console.log("Error in fetching MSO : " + err);
            } else {
                res.render('admin/deleted_mso_list', { title: 'MSO List', message: "", mso: msos });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});

router.post('/get_admin_mso_del_customer', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        console.log("selected MSO : " + req.body.MSOId);
        if (req.body.MSOId !== undefined) {
            global.SelectedMSO = req.body.MSOId;
        }
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day').subtract(1, 'days').format();
            var endDate = moment().startOf('day').add(1, 'days').format();

            console.log("dates in undefined " + startDate + " and " + endDate);
        } else {
            console.log("dates " + startDate + " and " + endDate);
        }
        DeletedCustomerDetais.find({ MSOId: global.SelectedMSO, created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log("Error in fetching cust MSO : " + err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('admin/deleted_sub_list', { title: 'Customer List', message: "", startDate: startDate, endDate: endDate, subs: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});

router.get('/admin_mso_transactions', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        MSODataModel.find({ custType: "DC" }, function(err, msos) {
            if (err) {
                console.log("Error in fetching MSO : " + err);
            } else {
                res.render('admin/mso_transaction_list', { title: 'MSO List', message: "", mso: msos });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

//admin_transaction


router.post('/admin_transaction', function(req, res, next) {
    if (isAdminSessionValid(req)) {
        console.log("selected MSO : " + req.body.MSOId);
        if (req.body.MSOId !== undefined) {
            global.SelectedMSO = req.body.MSOId;
        }
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            startDate = moment().startOf('day');
            endDate = moment().endOf('day');
            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            startDate = moment(startDate).startOf('day');
            endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);

        CustomerPaymentModel.find({ MSOId: global.SelectedMSO, status: "PAID", created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                startDate = startDate.format("DD-MM-YYYY");
                endDate = endDate.format("DD-MM-YYYY");
                res.render('admin/transaction_list', { title: 'Transaction Report', pays: cust, startDate: startDate, endDate: endDate, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/get_payment_admin', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        if (global.getPaymentError) {
            res.render('admin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Incorrect Receipt Id' });
        }
        if (global.editPayment) {
            res.render('admin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Payment Revoked Successfully' });
        }
        if (global.editPaymentError) {
            res.render('admin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Error in Revoking Payment please try again' });
        } else {
            res.render('admin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: '' });
        }
    } else {
        res.redirect('sign-in');
    }
})
router.post('/edit_payment_admin', function(req, res, next) {
    var sess = req.session;

    if (isAdminSessionValid(req)) {
        console.log(req.body.payId);
        global.paymentID = req.body.payId;
        CustomerPaymentModel.findOne({ id: global.paymentID, status: "PAID" }, function(err, pay) {
            if (err) {
                console.log("error in finding Payment " + err);

            } else if (!pay) {
                console.log("in !pay");
                global.getPaymentError = true;
                res.redirect('get_payment_admin');
            } else {
                console.log("Payment Details : " + JSON.stringify(pay));
                global.paidAmount = pay.paidAmount;
                global.customerId = pay.customerId;
                console.log("Paid Amt : " + global.paidAmount);
                res.render('admin/payment_revoke', { title: '', sectionTitle: 'Payment Revoke', payment: pay, sectionMessage: 'Payment Details' });
            }
        });

    } else {
        res.redirect('sign-in');
    }
});


router.post('/payment_edit_admin', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        console.log("payment id : " + global.paymentID);
        CustomerPaymentModel.update({ id: global.paymentID }, { $set: { status: "REVOKE" } }, function(err, pay) {
            if (err) {
                console.log("Payment delete Error" + err);
            } else if (!pay) {
                global.editPaymentError = true;
                global.editPayment = false;
                res.redirect('get_payment_admin');
            } else {

                console.log("Paid Amt in update : " + global.paidAmount);
                CustomerDetailsModel.update({ customerId: global.customerId }, {
                    $inc: { balanceAmount: global.paidAmount, totalPaidAmount: -global.paidAmount }
                }, function(err, cust) {
                    if (err) {

                    } else {
                        CustomerDetailsModel.findOne({ customerId: global.customerId }, function(err, custPay) {
                            if (err) {

                            } else {
                                var startDate = moment().startOf('day').format("DD-MM-YYYY");
                                console.log("cs " + custPay.name);
                                console.log("balance and netplot : " + custPay.balanceAmount)
                                CustomerPaymentModel.create({
                                    id: "NA",
                                    ledgerType: "PAYMENT REVOKE",
                                    status: "REVOKE",
                                    paidAmount: 0,
                                    drAmt: global.paidAmount,
                                    balance: custPay.balanceAmount,
                                    netPlotAmount: custPay.netPlotAmount,
                                    customerId: global.customerId,
                                    name: custPay.name,
                                    MSOId: custPay.MSOId,
                                    LCOId: custPay.LCOId,
                                    AgentId: pay.AgentId,
                                }, function(err, ledger) {
                                    if (err) {

                                    } else {
                                        LedgerReportModel.create({
                                            receiptId: "NA",
                                            ledgerType: "PAYMENT REVOKE",
                                            status: "REVOKE",
                                            paidAmount: 0,
                                            drAmt: global.paidAmount,
                                            balance: custPay.balanceAmount,
                                            netPlotAmount: custPay.netPlotAmount,
                                            customerId: global.customerId,
                                            name: custPay.name,
                                            MSOId: custPay.MSOId,
                                            LCOId: custPay.LCOId,
                                            AgentId: pay.AgentId,
                                        }, function(err, ledger) {
                                            if (err) {

                                            } else {

                                                console.log(JSON.stringify(pay));
                                                global.editPaymentError = false;
                                                global.editPayment = true;
                                                res.redirect('get_payment_admin');
                                            }
                                        })

                                    }
                                })
                            }
                        })


                    }
                })

            }

        });
    } else {
        res.redirect('sign-in');
    }
});

router.get('/get_message_log', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        MessageLogModel.find(function(err, msgLog) {
            if (err) {
                console.log("err " + err);
            } else {
                res.render('admin/message_log', { title: '', sectionTitle: 'Message Log', msg: msgLog, sectionMessage: 'Message Log' });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/mso_manipulate', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        console.log("mso selected : " + req.body.MSOId);
        MSODataModel.findOne({ MSOId: req.body.MSOId }, function(err, mso) {
            if (err) {
                console.log("error : " + err);
            } else {
                res.render('admin/edit_mso', { title: 'Edit MSO', sectionTitle: 'Edit MSO', agent: mso, sectionMessage: 'Edit MSO', message: "" });
            }
        })

    } else {
        res.redirect('sign-in');
    }
})

router.post('/edit_mso', function(req, res, next) {
    var sess = req.session;
    if (isAdminSessionValid(req)) {
        var MSOId = req.body.MSOId;
        var name = req.body.name;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        MSODataModel.update({ MSOId: MSOId }, { $set: { name: name, address: address, phone1: phone1, phone2: phone2 } }, function(err, msoU) {
            if (err) {

            } else {
                AgentDataModel.updateMany({ MSOId: MSOId }, { $set: { MSOName: name, MSOAdd: address } }, function(err, msoLogin) {
                    if (err) {

                    } else {
                        AgentLoginModel.updateMany({ MSOId: MSOId }, { $set: { MSOName: name } }, function(err, agentMso) {
                            if (err) {

                            } else {
                                MSODataModel.findOne({ MSOId: MSOId }, function(err, mso) {
                                    if (err) {

                                    } else {
                                        res.render('admin/edit_mso', { title: 'Edit MSO', sectionTitle: 'Edit MSO', agent: mso, sectionMessage: 'Edit MSO', message: "Details Updated Successfully" });
                                    }
                                })

                            }
                        })
                    }
                })
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

module.exports = router;