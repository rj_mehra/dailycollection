var express = require('express');
var router = express.Router();
var fs = require("fs");
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var AgentLoginModel = require('../models/AgentLogin.js');
var AgentDataModel = require('../models/AgentData.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var CustomerDetailsModel = require('../models/CustomerDetails.js');
var CustomerPaymentModel = require('../models/CustomerPayments.js');
var DeletedCustomerDetais = require('../models/DeletedCustomer.js');
var pureArray1 = new Array();
var pureArray = new Array();
var Converter = require("csvtojson").Converter;
var converter = new Converter({ constructResult: false });

function isLCOSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.lco == undefined)) {
        global.LCOId = sess.lco.LCOId;
        global.MSOId = sess.lco.MSOId;
        global.LCOName = sess.lco.name;
        return true;
    } else {
        return false;
    }
}


function DeletedCustomer() {

};

function NewCustomer() {

};

router.get('/cust_list_lco', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        CustomerDetailsModel.find({ LCOId: global.LCOId }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                res.render('lco/edit_subscriber_list', { title: 'Subscriber List', message: '', subs: cust, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

//edit_subscriber_lco
router.post('/edit_subscriber_lco', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        global.customerID = req.body.subsValue;
        console.log("cust : " + global.customerID);
        CustomerDetailsModel.findOne({ customerId: global.customerID }, function(err, cust) {
            if (err) {

            } else {
                res.render('lco/edit_cust', { title: 'Edit Customer', message: '', msos: cust });
            }
        })


    } else {

    }
})
router.get('/edit_subscriber_lco', function(req, res, next) {
    var sess = req.session;

    if (isLCOSessionValid(req)) {
        CustomerDetailsModel.findOne({ customerId: global.customerID }, function(err, cust) {
            if (err) {
                console.log("Error 1 ");
            } else {
                if (global.editcust) {
                    res.render('lco/edit_cust', { title: 'Edit Customer', message: 'Customer Updated Successfully', msos: cust });
                } else if (global.editcustError) {
                    res.render('lco/edit_cust', { title: 'Edit Customer', message: 'Error in Updating Customer..Please try again later', msos: cust });
                } else {
                    res.render('lco/edit_cust', { title: 'Edit Customer', message: '', msos: cust });
                }

            }
        })
    } else {
        res.redirect('/sign-in');
    }
})


router.post('/edit_cust_lco', function(req, res, next) {
    var sess = req.session;
    if (isLCOSessionValid(req)) {
        var name = req.body.name;
        var id = req.body.id;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var plot = req.body.plot;
        console.log("Customer ID : " + id);
        CustomerDetailsModel.update({ customerId: id }, {
            $set: {
                name: name,
                address: address,
                mobileNo: phone1,
                altNo: phone2,
                plotNo: plot
            }
        }, function(err, cust) {
            if (err) {
                console.log("LCO Update Error" + err);
            } else if (!cust) {
                global.editcustError = true;
                global.editcust = false;
                res.redirect('edit_subscriber_lco');
            } else {
                console.log(JSON.stringify(cust));
                global.editcustError = false;
                global.editcust = true;
                res.redirect('edit_subscriber_lco');
            }

        });
    } else {
        res.redirect('/sign-in');
    }
})


router.get('/lco_transaction', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day');
            var endDate = moment().endOf('day')

            console.log("dates in undefined " + startDate + " and " + endDate);
        } else {
            var startDate = moment(startDate).startOf('day');
            var endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        //console.log("Date : "+startDate);

        CustomerPaymentModel.find({ LCOId: global.LCOId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('lco/lco_transactionreport', { title: 'Transaction Report', pays: cust, startDate: startDate, endDate: endDate, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});
router.post('/lco_transaction', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === '' && endDate === '') {
            var startDate = moment().startOf('day');
            var endDate = moment().endOf('day');
            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {

            startDate = moment(startDate).startOf('day');
            endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);

        CustomerPaymentModel.find({ LCOId: global.LCOId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('lco/lco_transactionreport', { title: 'Transaction Report', pays: cust, startDate: startDate, endDate: endDate, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});


router.get('/lco_outstanding', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        CustomerDetailsModel.find({ LCOId: global.LCOId, balanceAmount: { $gt: 0 } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                res.render('lco/lco_outreport', { title: 'Outstanding Report', cust: cust, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});
//subscriber_manipulate
router.post('/subscriber_manipulate', function(req, res, next) {
    if (isLCOSessionValid(req)) {
        console.log("customer id : " + req.body.subsValue);
        var cust = req.body.subsValue;
        if (Array.isArray(cust)) {
            console.log("customers : " + cust);
        }
        var manipulate = req.body.manipulate;
        console.log("mani " + manipulate)
        if (manipulate === "payment") {
            CustomerPaymentModel.find({ customerId: cust }, function(err, payment) {
                if (err) {
                    console.log(err);
                } else {
                    LedgerReportModel.find({ customerId: cust }, function(err, customer) {
                        if (err) {
                            console.log(err);
                        } else {

                            res.render('lco/lco_ledgerreport', { title: 'Payment Ledger', pays: payment, cust: customer });
                        }
                    });


                }
            });
        } else if (manipulate === "edit") {
            global.customerID = req.body.subsValue;
            console.log("cust : " + global.customerID);
            CustomerDetailsModel.findOne({ customerId: global.customerID }, function(err, cust) {
                if (err) {

                } else {
                    res.render('lco/edit_cust', { title: 'Edit Customer', message: '', msos: cust });
                }
            })

        } else if (manipulate === "delete") {
            CustomerDetailsModel.findOne({ customerId: req.body.subsValue }, function(err, custs) {
                if (err) {
                    console.log("error in fetching cust : " + err);
                } else {
                    var cust = new DeletedCustomer();
                    cust.customerId = custs.customerId;
                    cust.LCOId = custs.LCOId;
                    cust.MSOId = custs.MSOId;
                    cust.signProof = custs.signProof;
                    cust.customerPhoto = custs.customerPhoto;
                    cust.idProof = custs.idProof;
                    cust.addressProof = custs.addressProof;
                    cust.balanceAmount = custs.balanceAmount;
                    cust.totalPaidAmount = custs.totalPaidAmount;
                    cust.netPlotAmount = custs.netPlotAmount;
                    cust.altNo = custs.altNo;
                    cust.mobileNo = custs.mobileNo;
                    cust.plotNo = custs.plotNo;
                    cust.name = custs.name;
                    cust.deletedBy = custs.LCOId;

                    DeletedCustomerDetais.create(cust, function(err, customer) {
                        if (err) {
                            console.log("err in deleting" + err);
                        } else {
                            CustomerDetailsModel.remove({ customerId: req.body.subsValue }, function(err, dele) {
                                if (err) {

                                } else {
                                    console.log("deleted");
                                    CustomerDetailsModel.find({ LCOId: LCOId }, function(err, cust) {
                                        if (err) {

                                        } else {
                                            res.render('lco/subscriber_list', { title: 'Customer List', message: "Customer Removed Successfully", subs: cust });
                                        }
                                    })
                                }
                            })

                        }
                    })
                }
            })
        } else if (manipulate === "custlist") {
            CustomerDetailsModel.find({ LCOId: global.selectedLCO }, function(err, cust) {
                if (err) {

                } else {
                    res.render('lco/subscriber_list', { title: 'Customer List', message: "", subs: cust });
                }
            })
        } else {
            res.redirect('/sign-in');
        }
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/get_password_lco', function(req, res, next) {
    var sess = req.session;
    if (isLCOSessionValid(req)) {
        res.render('lco/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "", message: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/change_password_lco', function(req, res, next) {
    var sess = req.session;
    if (isLCOSessionValid(req)) {
        var password = req.body.oldpassword;
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;

        if (newpassword !== confirmpassword) {
            res.render('lco/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "Password and confirm password do no match" });
        } else {
            LCOLoginModel.findOne({ LCOId: global.LCOId }, function(err, mso) {
                if (err) {

                } else {
                    if (password !== mso.password) {
                        res.render('lco/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "Incorrect Password entered" });
                    } else {
                        LCOLoginModel.update({ LCOId: global.LCOId }, { $set: { password: newpassword } }, function(err, msoUpdate) {
                            if (err) {

                            } else {
                                LCOLoginModel.update({ LCOId: global.LCOId }, { $set: { password: newpassword } }, function(err, data) {
                                    if (err) {

                                    } else {
                                        global.changePassword = true;
                                        res.redirect('sign-in');
                                    }
                                })
                            }
                        })
                    }
                }
            });
        }
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_add_customer_lco', function(req, res, next) {
    var sess = req.session;
    if (isLCOSessionValid(req)) {
        if (global.custAdded) {
            res.render('lco/add_customer', { title: 'Add Customer', sectionTitle: "Add Customer", sectionMessage: message });
        } else if (global.custError) {
            res.render('lco/add_customer', { title: 'Add Customer', sectionTitle: "Add Customer", sectionMessage: message });
        } else {
            res.render('lco/add_customer', { title: 'Add Customer', sectionTitle: "Add Customer", sectionMessage: "" });
        }

    } else {
        res.redirect('sign-in');
    }
})

router.post('/add_customer_lco', function(req, res, next) {
    var sess = req.session;
    if (isLCOSessionValid(req)) {
        LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
            if (err) {

            } else {
                CustomerDetailsModel.find({ customerId: req.body.custId }, function(err, customer) {
                    if (err) {
                        global.custError = true;
                        global.custAdded = false;
                        message = "Error in Creating Customer";
                        res.redirect('get_add_customer_lco');
                    } else if (customer.length > 0) {
                        global.custError = true;
                        global.custAdded = false;
                        message = "Customer Id Already Exists";
                        res.redirect('get_add_customer_lco');
                    } else {
                        console.log("LCO Details : " + lco.LCOId + " and " + lco.MSOId);
                        var cust = new NewCustomer();
                        cust.customerId = req.body.custId;
                        cust.name = req.body.name;
                        cust.address = req.body.address;
                        cust.plotNo = req.body.plotNo;
                        cust.mobileNo = req.body.mobNo;
                        cust.altNo = req.body.altNo;
                        cust.netPlotAmount = req.body.netPlot;
                        cust.totalPaidAmount = req.body.totalPaid;
                        cust.balanceAmount = req.body.balAmt;
                        cust.LCOId = lco.LCOId;
                        cust.MSOId = lco.MSOId;
                        CustomerDetailsModel.create(cust, function(err, custs) {
                            if (err) {
                                global.custError = true;
                                global.custAdded = false;
                                message = "Error in Creating Customer";
                                res.redirect('get_add_customer_lco');

                            } else {
                                global.custError = false;
                                global.custAdded = true;
                                message = "Customer Added Successfully";
                                res.redirect('get_add_customer_lco');
                            }
                        })
                    }
                })



            }
        })
    } else {
        res.redirect('sign-in');
    }
});


router.get('/lco_del_cust', function(req, res, next) {
    var sess = req.session;
    if (isLCOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day').subtract(1, 'days').format();
            var endDate = moment().startOf('day').add(1, 'days').format();

            console.log("dates in undefined " + startDate + " and " + endDate);
        } else {
            console.log("dates " + startDate + " and " + endDate);
        }
        DeletedCustomerDetais.find({ LCOId: LCOId, created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log("Error in fetching cust LCO : " + err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('lco/deleted_sub_list', { title: 'Customer List', message: "", startDate: startDate, endDate: endDate, subs: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});


module.exports = router;