var express = require('express');
var router = express.Router();
var fs = require("fs");
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var AgentLoginModel = require('../models/AgentLogin.js');
var AgentDataModel = require('../models/AgentData.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var CustomerDetailsModel = require('../models/CustomerDetails.js');
var CustomerPaymentModel = require('../models/CustomerPayments.js');
var DeletedCustomerDetais = require('../models/DeletedCustomer.js');
var pureArray1 = new Array();
var pureArray = new Array();
var Converter = require("csvtojson").Converter;
var converter = new Converter({ constructResult: false });

function isMSOSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.mso == undefined)) {
        console.log("req body " + JSON.stringify(req.body));

        global.MSOId = sess.mso.MSOId;
        console.log("MSO : " + global.MSOId);
        return true;
    } else {
        return false;
    }
}


router.get('/mso_lco_transaction', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("in pay" + global.MSOId);
        CustomerPaymentModel.aggregate(
            [{
                    $match: {
                        MSOId: global.MSOId
                    }
                },
                {
                    $group: {
                        _id: "$LCOId",
                        count: { $sum: 1 }
                    }
                }
            ],
            function(err, lco) {
                if (err) {
                    console.log("Error ");
                } else {
                    console.log("MSO : " + JSON.stringify(lco));
                    res.render('mso/lco_transaction_list', { title: 'Transaction Report ', dist: lco, message: '' });

                }
            });
    } else {
        res.redirect('/sign-in');
    }
});


router.get('/mso__lco_outstanding', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("in pay" + global.MSOId);
        CustomerDetailsModel.aggregate(
            [{
                    $match: {
                        MSOId: global.MSOId
                    }
                },
                {
                    $group: {
                        _id: "$LCOId",
                        count: { $sum: 1 }
                    }
                }
            ],
            function(err, lco) {
                if (err) {
                    console.log("Error ");
                } else {
                    console.log("MSO : " + JSON.stringify(lco));
                    res.render('mso/lco_outstanding_list', { title: 'Outstanding Report ', dist: lco, message: '' });

                }
            });
    } else {
        res.redirect('/sign-in');
    }
});

router.post('/mso_transaction', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("Selected LCO : " + req.body.lcoValue);
        var lco = req.body.lcoValue;
        if (lco !== undefined) {
            global.selectedLCO = req.body.lcoValue;
        }

        console.log("LCO : " + global.selectedLCO);
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            startDate = moment().startOf('day');
            endDate = moment().endOf('day');

            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            startDate = moment(startDate).startOf('day');
            endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);
        // global.selectedLCO=req.body.lcoValue;
        // console.log("Selected LCO : "+global.selectedLCO);
        CustomerPaymentModel.find({ LCOId: global.selectedLCO, status: "PAID", created_at: { $gte: startDate, $lte: endDate } },
            function(err, cust) {
                if (err) {
                    console.log(err);
                } else {
                    startDate = moment(startDate).format("YYYY-MM-DD");
                    endDate = moment(endDate).format("YYYY-MM-DD");

                    res.render('mso/mso_lco_transactionreport', { title: 'Transaction Report', startDate: startDate, endDate: endDate, pays: cust });

                }
            });
    } else {
        res.redirect('/sign-in');
    }
});


router.post('/mso_outstanding', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        global.selectedLCO = req.body.lcoValue;
        console.log("Selected LCO : " + global.selectedLCO);
        CustomerDetailsModel.find({ LCOId: global.selectedLCO, balanceAmount: { $gt: 0 } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                res.render('mso/mso_lco_outreport', { title: 'Outstanding Report', cust: cust });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});


router.get('/get_password_mso', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        res.render('mso/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/change_password_mso', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        var password = req.body.oldpassword;
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;

        if (newpassword !== confirmpassword) {
            res.render('mso/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "Password and confirm password do no match" });
        } else {
            MSOLoginModel.findOne({ MSOId: global.MSOId }, function(err, mso) {
                if (err) {

                } else {
                    if (password !== mso.password) {
                        res.render('mso/change_password', { title: '', sectionTitle: "Change Password", sectionMessage: "Incorrect Password entered" });
                    } else {
                        MSOLoginModel.update({ MSOId: global.MSOId }, { $set: { password: newpassword } }, function(err, msoUpdate) {
                            if (err) {

                            } else {
                                MSOLoginModel.update({ MSOId: global.MSOId }, { $set: { password: newpassword } }, function(err, data) {
                                    if (err) {

                                    } else {
                                        global.changePassword = true;
                                        res.redirect('sign-in');
                                    }
                                })
                            }
                        })
                    }
                }
            });
        }
    } else {
        res.redirect('sign-in');
    }
})


router.get('/get_payment_mso', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        if (global.getPaymentError) {
            res.render('mso/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Incorrect Receipt Id', message: "" });
        }
        if (global.editPayment) {
            res.render('mso/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Payment Revoked Successfully', message: "" });
        }
        if (global.editPaymentError) {
            res.render('mso/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Error in Revoking Payment please try again', message: "" });
        } else {
            res.render('mso/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: '', message: "" });
        }
    } else {
        res.redirect('sign-in');
    }
})
router.post('/edit_payment_mso', function(req, res, next) {
    var sess = req.session;

    if (isMSOSessionValid(req)) {
        console.log(req.body.payId);
        global.paymentID = req.body.payId;
        CustomerPaymentModel.findOne({ id: global.paymentID, MSOId: global.MSOId, status: "PAID" }, function(err, pay) {
            if (err) {
                console.log("error in finding Payment " + err);

            } else if (!pay) {
                console.log("in !pay");
                global.getPaymentError = true;
                res.redirect('get_payment_mso');
            } else {
                console.log("Payment Details : " + JSON.stringify(pay));
                global.paidAmount = pay.paidAmount;
                global.customerId = pay.customerId;
                console.log("Paid Amt : " + global.paidAmount);
                res.render('mso/payment_revoke', { title: '', sectionTitle: 'Payment Revoke', payment: pay, sectionMessage: 'Payment Details' });
            }
        });

    } else {
        res.redirect('sign-in');
    }
});

router.get('/mso_lco_del_cust', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        LCODataModel.find({ custType: "DC" }, function(err, lcos) {
            if (err) {
                console.log("Error in fetching MSO : " + err);
            } else {
                res.render('mso/deleted_lco_list', { title: 'LCO List', message: "", lco: lcos });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});

router.post('/get_mso_lco_del_customer', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("selected LCO : " + req.body.LCOId);
        if (req.body.LCOId !== undefined) {
            global.SelectedLCO = req.body.LCOId;
        }
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day').subtract(1, 'days').format();
            var endDate = moment().startOf('day').add(1, 'days').format();

            console.log("dates in undefined " + startDate + " and " + endDate);
        } else {
            console.log("dates " + startDate + " and " + endDate);
        }
        DeletedCustomerDetais.find({ LCOId: global.SelectedLCO, created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log("Error in fetching cust LCO : " + err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('mso/deleted_sub_list', { title: 'Customer List', message: "", startDate: startDate, endDate: endDate, subs: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});

router.post('/payment_edit_mso', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("payment id : " + global.paymentID);
        CustomerPaymentModel.update({ id: global.paymentID }, { $set: { status: "REVOKE" } }, function(err, pay) {
            if (err) {
                console.log("Payment delete Error" + err);
            } else if (!pay) {
                global.editPaymentError = true;
                global.editPayment = false;
                res.redirect('get_payment_mso');
            } else {

                console.log("Paid Amt in update : " + global.paidAmount);
                CustomerDetailsModel.update({ customerId: global.customerId }, {
                    $inc: { balanceAmount: global.paidAmount, totalPaidAmount: -global.paidAmount }
                }, function(err, cust) {
                    if (err) {

                    } else {
                        CustomerDetailsModel.findOne({ customerId: global.customerId }, function(err, custPay) {
                            if (err) {

                            } else {
                                var startDate = moment().startOf('day').format("DD-MM-YYYY");
                                console.log("cs " + custPay.name);
                                console.log("balance and netplot : " + custPay.balanceAmount)
                                CustomerPaymentModel.create({
                                    id: "NA",
                                    ledgerType: "PAYMENT REVOKE",
                                    status: "REVOKE",
                                    paidAmount: 0,
                                    drAmt: global.paidAmount,
                                    balance: custPay.balanceAmount,
                                    netPlotAmount: custPay.netPlotAmount,
                                    customerId: global.customerId,
                                    name: custPay.name,
                                    MSOId: custPay.MSOId,
                                    LCOId: custPay.LCOId,
                                    AgentId: pay.AgentId,
                                }, function(err, ledger) {
                                    if (err) {

                                    } else {

                                        LedgerReportModel.create({
                                            receiptId: "NA",
                                            ledgerType: "PAYMENT REVOKE",
                                            status: "REVOKE",
                                            paidAmount: 0,
                                            drAmt: global.paidAmount,
                                            balance: custPay.balanceAmount,
                                            netPlotAmount: custPay.netPlotAmount,
                                            customerId: global.customerId,
                                            name: custPay.name,
                                            MSOId: custPay.MSOId,
                                            LCOId: custPay.LCOId,
                                            AgentId: pay.AgentId,
                                        }, function(err, ledger) {
                                            if (err) {

                                            } else {

                                                console.log(JSON.stringify(pay));
                                                global.editPaymentError = false;
                                                global.editPayment = true;
                                                res.redirect('get_payment_mso');
                                            }
                                        })
                                    }
                                })
                            }
                        })


                    }
                })

            }

        });
    } else {
        res.redirect('sign-in');
    }
});

//get_mso_payment_revoke

router.get('/get_mso_payment_revoke', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day').subtract(1, 'days').format();
            var endDate = moment().startOf('day').add(1, 'days').format();

            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            // startDate=startDate.toISOString();
            // endDate=endDate.toISOString();
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);
        CustomerPaymentModel.find({ MSOId: global.MSOId, ledgerType: "PAYMENT REVOKE", created_at: { $gte: startDate, $lte: endDate } }, function(err, pay) {
            if (err) {

            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('mso/payment_revoke_report', { title: 'Payment Revoke Report', startDate: startDate, endDate: endDate, pays: pay });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})
router.post('/get_mso_payment_revoke', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day').subtract(1, 'days').format();
            var endDate = moment().startOf('day').add(1, 'days').format();

            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            // startDate=startDate.toISOString();
            // endDate=endDate.toISOString();
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);
        CustomerPaymentModel.find({ MSOId: global.MSOId, ledgerType: "PAYMENT REVOKE", created_at: { $gte: startDate, $lte: endDate } }, function(err, pay) {
            if (err) {

            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('mso/payment_revoke_report', { title: 'Payment Revoke Report', startDate: startDate, endDate: endDate, pays: pay });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_mso_lco_payment', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("in lco wise party ledgr");
        LCODataModel.find({ MSOId: global.MSOId }, function(err, lco) {
            if (err) {
                console.log("Err in lco party " + err);
            } else {
                res.render('mso/lco_party_ledger', { title: '', sectionTitle: 'LCO List', lcos: lco, sectionMessage: 'LCO Details' });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/get_customer_party', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        console.log("Selected LCO : " + req.body.lcoValue);
        CustomerDetailsModel.find({ LCOId: req.body.lcoValue }, function(err, cust) {
            if (err) {
                console.log("error : " + err);
            } else {
                res.render('mso/cust_list_ledger', { title: '', sectionTitle: 'Customer List', subs: cust, sectionMessage: 'Customer Details' });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/get_party_ledger_mso', function(req, res, next) {
    var sess = req.session;
    if (isMSOSessionValid(req)) {
        var cust = req.body.subsValue;
        CustomerPaymentModel.find({ customerId: cust }, function(err, payment) {
            if (err) {
                console.log(err);
            } else {
                LedgerReportModel.find({ customerId: cust }, function(err, customer) {
                    if (err) {
                        console.log(err);
                    } else {

                        res.render('mso/party_ledger', { title: 'Payment Ledger', pays: payment, cust: customer });
                    }
                });


            }
        });
    } else {
        res.redirect('sign-in');
    }
})



module.exports = router;