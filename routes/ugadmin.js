var express = require('express');
var router = express.Router();
var fs = require("fs");
var LCOLoginModel = require('../models/LCOLogin.js');
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var LCODataModel = require('../models/LCOData.js');
var UGMSOLoginModel = require('../models/UGMSOLogin.js');
var UGLCOLoginModel = require('../models/UGLCOLogin.js');
var UGAgentLoginModel = require('../models/UGAgentLogin.js');
var UGDistLoginModel = require('../models/UGDistLogin.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var GSPCustomerModel = require('../models/CustomerDetails.js');
var GSPCustomerPaymentModel = require('../models/CustomerPayments.js');
var DeletedCustomerDetais = require('../models/DeletedCustomer.js');
var AdminLoginModel = require('../models/AdminLogin.js');
var VersionModel = require('../models/Version.js');
var AgentLoginModel = require('../models/AgentLogin.js');
var AgentDataModel = require('../models/AgentData.js');
var GoldRateModel = require('../models/GoldRate.js');
var MessageLogModel = require('../models/MessageLogs.js');
var GSPLedgerReportModel = require('../models/GSPLedgerReport.js');
var GSPGSPCustomerPaymentModel = require('../models/GSPCustomerPayment.js');
var GSPCustomerModel = require('../models/GSPCustomerDetails.js');
var pureArray1 = new Array();
var pureArray = new Array();
var getUid = require('get-uid');
var Converter = require("csvtojson").Converter;
var converter = new Converter({ constructResult: false });

function isUGAdminSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.ugadmin == undefined)) {
        console.log("req body " + JSON.stringify(req.body));
        adminName = sess.ugadmin.name;
        loginDate = sess.ugadmin.date;
        return true;
    } else {
        return false;
    }
}


router.get('/dashboard_ugadmin', function(req, res, next) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (isUGAdminSessionValid(req)) {



        res.render('UGAdmin/index', {
            title: 'DC',
            message: global.message,
            name: "Administrator" //extra added for avoiding crash
        });

    } else {
        res.redirect('/');
    }
});

///add_ugmso
router.get('/add_ugmso', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {

        res.render('UGAdmin/add_MSO', { title: 'Register MSO', mandatorymessage: 'Please fill in the mandatory details' });

    } else {
        res.redirect('/');
    }
});

router.post('/ugmso_add', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var name = req.body.name;
        var MSOId = req.body.msoId;
        var password = req.body.password;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var contactPerson = req.body.contactPerson;

        if (!(req.body.password === req.body.passwordconfirm)) {
            global.msoAdded = false;
            global.msoAddError = true;
            res.redirect('add_ugmso');
            message = 'Password and confirm password must be same';
        } else {
            MSOLoginModel.find({ MSOId: MSOId }, function(err, MSO) {
                if (err) {
                    console.dir(err);
                    global.msoAdded = false;
                    global.msoAddError = true;
                    res.redirect('add_ugmso');
                    message = 'Something went wrong';
                } else if (MSO.length > 0) {
                    global.msoAdded = false;
                    global.msoAddError = true;
                    message = 'MSOId already used';
                    console.dir('Mso ' + MSO);
                    res.redirect('add_ugmso');
                } else {
                    MSODataModel.create({
                        MSOId: MSOId,
                        name: name,
                        address: address,
                        phone1: phone1,
                        phone2: phone2,
                        contactPerson: contactPerson,
                        password: password,
                        custType: "UG"
                    }, function(err, post) {

                        var content = req.body;
                        if (err) {
                            console.dir(err);
                            global.msoAdded = false;
                            global.msoAddError = true;;
                            message = 'Something went wrong';
                        } else {
                            MSOLoginModel.create({ MSOId: MSOId, password: password, custType: "UG" }, function(err, mso) {
                                if (err) {
                                    console.dir(err);
                                    global.msoAdded = false;
                                    global.msoAddError = true;
                                    message = 'Something went wrong';
                                } else {
                                    global.msoAdded = true;
                                    res.redirect('add_ugmso');
                                    message = 'MSO Added Successfully';
                                }
                            })

                        }
                    });
                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }
});


//Add distributor

router.get('/add_ugdist', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        MSODataModel.find({ custType: "UG" }, function(err, data) {
            if (err) return next(err);
            else {
                msoArray = new Array();
                console.dir("data " + JSON.stringify(data));
                for (var i = 0; i < data.length; i++) {
                    msoArray.push(data[i]);
                }
                console.dir('isdArray ' + msoArray);
                if (global.distAdded) {
                    res.render('UGAdmin/add_dist', { title: 'Add Distributor', msos: data, message: "Distributor Added Successfully" });
                }
                if (global.distAddError) {
                    res.render('UGAdmin/add_dist', { title: 'Add Distributor', msos: msoArray, message: message });
                } else {
                    res.render('UGAdmin/add_dist', { title: 'Add Distributor', msos: msoArray, message: "" });
                }
            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

//end of add dist



router.post('/ugdist_add', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var name = req.body.name;
        var DistId = req.body.DistId;
        var password = req.body.password;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var contactPerson = req.body.contactPerson;
        var MSODetails = req.body.MSOId;
        var array = MSODetails.split('/');
        if (!(req.body.password === req.body.passwordconfirm)) {
            global.distAdded = false;
            global.distAddError = true;;
            res.redirect('add_ugdist');
            message = 'Passwords don\'t match';
        } else {
            UGDistLoginModel.find({ DistId: DistId }, function(err, Lco) {
                if (err) {
                    console.dir(err);
                    global.distAdded = false;
                    global.distAddError = true;;
                    res.redirect('add_ugdist');
                    message = 'Something went wrong';
                } else if (Lco.length > 0) {
                    global.distAdded = false;
                    global.distAddError = true;
                    message = 'Dist Id already used';
                    console.dir('Dist ' + Lco);
                    res.redirect('add_ugdist');

                } else {
                    UGDistLoginModel.create({
                        DistId: DistId,
                        MSOId: array[0],
                        name: name,
                        address: address,
                        phone1: phone1,
                        phone2: phone2,
                        contactPerson: contactPerson,
                        password: password,
                        custType: "UG"
                    }, function(err, post) {

                        var content = req.body;
                        if (err) {
                            console.dir(err);
                            global.distAdded = false;
                            global.distAddError = true;;
                            message = 'Something went wrong';
                        } else {
                            res.redirect('add_ugdist');
                            global.distAdded = true;

                        }
                    });
                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }

});


router.get('/get_uglco', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        LCODataModel.find({ idCreated: false, custType: "UG" }, function(err, lco) {
            res.render('UGAdmin/lco_add_list', { title: 'LCO List', lcos: lco, message: "" });
        })
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/get_lco_details_UG', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var lcoId = req.param('lcoId');
        global.selectedLCO = lcoId;
        console.log("in lco id creation.." + lcoId);
        LCODataModel.findOne({ LCOId: global.selectedLCO }, function(err, lco) {
            if (err) {

            } else {
                res.render('UGAdmin/lco_details', { title: 'LCO Details', message: "", lcos: lco });
            }
        })
    } else {
        res.redirect('/sign-in');
    }
})

// //add lco


router.get('/add_uglco', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        UGDistLoginModel.find(function(err, data) {
            if (err) return next(err);
            else {
                msoArray = new Array();
                console.dir("data " + JSON.stringify(data));
                for (var i = 0; i < data.length; i++) {
                    msoArray.push(data[i]);
                }
                console.dir('isdArray ' + msoArray);
                if (global.lcoAdded) {
                    res.render('UGAdmin/add_LCO', { title: 'Add LCO', msos: data, message: "LCO Added Successfully" });
                }
                if (global.lcoAddError) {
                    res.render('UGAdmin/add_LCO', { title: 'Add LCO', msos: msoArray, message: message });
                } else {
                    res.render('UGAdmin/add_LCO', { title: 'Add LCO', msos: msoArray });
                }
            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.post('/uglco_add', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {

        var name = req.body.name;
        var LCOId = getUid();
        var password = req.body.password;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var contactPerson = req.body.contactPerson;
        var MSODetails = req.body.DistId;
        var array = MSODetails.split('/');
        if (!(req.body.password === req.body.passwordconfirm)) {
            global.lcoAdded = false;
            global.lcoAddError = true;;
            res.redirect('add_uglco');
            message = 'Passwords don\'t match';
        } else {
            LCOLoginModel.find({ name: name }, function(err, Lco) {
                if (err) {
                    console.dir(err);
                    global.lcoAdded = false;
                    global.lcoAddError = true;;
                    res.redirect('add_lco');
                    message = 'Something went wrong';
                } else if (Lco.length > 0) {
                    global.lcoAdded = false;
                    global.lcoAddError = true;
                    message = 'LCO Name already used';
                    console.dir('LCO ' + Lco);
                    res.redirect('add_uglco');

                } else {
                    UGDistLoginModel.findOne({ DistId: array[0] }, function(err, dist) {
                        if (err) {
                            console.dir(err);
                            global.lcoAdded = false;
                            global.lcoAddError = true;;
                            message = 'Something went wrong';
                            console.log("Error in finding dist : " + err);
                        } else {
                            console.log("Dist detailds : " + JSON.stringify(dist));
                            LCODataModel.create({
                                LCOId: LCOId,
                                DistId: dist.DistId,
                                MSOId: dist.MSOId,
                                name: name,
                                address: address,
                                phone1: phone1,
                                phone2: phone2,
                                contactPerson: contactPerson,
                                password: password,
                                custType: "UG"
                            }, function(err, post) {

                                var content = req.body;
                                if (err) {
                                    console.dir(err);
                                    global.lcoAdded = false;
                                    global.lcoAddError = true;;
                                    message = 'Something went wrong';
                                } else {
                                    LCOLoginModel.create({ LCOId: LCOId, MSOId: dist.MSOId, DistId: dist.DistId, password: password, custType: "UG" }, function(err, lco) {
                                        if (err) {
                                            console.dir(err);
                                            global.lcoAdded = false;
                                            global.lcoAddError = true;;
                                            message = 'Something went wrong';
                                        } else {
                                            res.redirect('add_uglco');
                                            global.lcoAdded = true;

                                        }
                                    })

                                }
                            });
                        }
                    })

                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }

});

//List Users

router.get('/list_ugdist', function(req, res, next) {
    if (isUGAdminSessionValid(req)) {
        UGDistLoginModel.find(function(err, lcos) {
            if (err) {
                console.log(err);
            } else {
                res.render('UGAdmin/Dist_list', { title: 'Distributor List', lcos: lcos, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/list_ugmso', function(req, res, next) {
    if (isUGAdminSessionValid(req)) {
        MSODataModel.find({ custType: "UG" }, function(err, msos) {
            if (err) {
                console.log(err);
            } else {
                res.render('UGAdmin/MSO_list', { title: 'MSO List', msos: msos, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/list_uglco', function(req, res, next) {
    if (isUGAdminSessionValid(req)) {
        LCODataModel.find({ custType: "UG" }, function(err, msos) {
            if (err) {
                console.log(err);
            } else {
                res.render('UGAdmin/LCO_list', { title: 'LCO List', lcos: msos, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

//add agent

router.get('/add_ugagent', function(req, res, next) {
    var sess = req.session;
    //console.log(JSON.stringify(req.session));
    if (isUGAdminSessionValid(req)) {
        LCODataModel.find({ custType: "UG" }, function(err, data) {
            if (err) return next(err);
            else {
                lcoArray = new Array();
                for (var i = 0; i < data.length; i++) {
                    lcoArray.push(data[i]);
                }
                if (global.agentAdded) {
                    res.render('UGAdmin/add_agent', { title: 'Add Agent', lcos: lcoArray, message: "Agent Added Successfully" });
                }
                if (global.agentAddError) {
                    res.render('UGAdmin/add_agent', { title: 'Add Agent', lcos: lcoArray, message: message });
                } else {
                    res.render('UGAdmin/add_agent', { title: 'Add Agent', lcos: lcoArray, message: "" });
                }
                console.dir('lcoArray ' + lcoArray);
            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.post('/ugagent_add', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var custType = req.body.custType;
        var name = req.body.name;
        var agentId = req.body.agentId;
        var password = req.body.password;
        var LCOId = req.body.LCOId;
        var address = req.body.address;
        var openIMEI = req.body.imei;
        var phone = req.body.phone;
        var array = LCOId.split('/');
        if (!(req.body.password === req.body.passwordconfirm)) {
            global.agentAdded = false;
            global.agentAddError = true;;
            res.redirect('add_ugagent');
            message = 'Passwords don\'t match';
        } else {
            AgentLoginModel.find({ agentId: agentId }, function(err, Agent) {
                if (err) {
                    console.dir(err);
                    global.agentAdded = false;
                    global.agentAddError = true;;
                    res.redirect('add_ugagent');
                    message = 'Something went wrong';
                } else if (Agent.length > 0) {
                    global.agentAdded = false;
                    global.agentAddError = true;
                    message = 'Agent Id already used';
                    console.dir('Agent ' + Agent);
                    res.redirect('add_ugagent');
                } else {
                    console.log("LCO : " + array[0]);
                    LCODataModel.findOne({ LCOId: array[0] }, function(err, lco) {
                        if (err) {

                        } else {

                            MSODataModel.findOne({ MSOId: lco.MSOId }, function(err, mso) {
                                if (err) {

                                } else {
                                    var openIMEIo = false;
                                    if (openIMEI !== undefined) {
                                        openIMEIo = true;
                                    }
                                    console.log("MSO : " + lco.MSOId + "mso name : " + mso.name + "LCO id :" + lco.LCOId + "lco nam :" + lco.name)
                                    AgentDataModel.create({
                                        agentId: agentId,
                                        name: name,
                                        LCOPhone: lco.phone1,
                                        MSOId: lco.MSOId,
                                        DistId: lco.DistId,
                                        LCOAdd: lco.address,
                                        MSOAdd: mso.address,
                                        LCOName: lco.name,
                                        MSOName: mso.name,
                                        LCOId: array[0],
                                        address: address,
                                        phone: phone,
                                        IMEI: "unassigned",
                                        isOpen: openIMEIo,
                                        password: password,
                                        custType: "UG"
                                    }, function(err, post) {

                                        var content = req.body;
                                        if (err) {
                                            console.dir(err);
                                            global.agentAdded = false;
                                            global.agentAddError = true;
                                            message = 'Something went wrong';
                                        } else {
                                            AgentLoginModel.create({
                                                    agentId: agentId,
                                                    name: name,
                                                    DistId: lco.DistId,
                                                    LCOAdd: lco.address,
                                                    LCOPhone: lco.phone1,
                                                    MSOId: lco.MSOId,
                                                    LCOAdd: lco.address,
                                                    MSOAdd: mso.address,
                                                    LCOName: lco.name,
                                                    MSOName: mso.name,
                                                    LCOId: array[0],
                                                    address: address,
                                                    phone: phone,
                                                    IMEI: "unassigned",
                                                    isOpen: openIMEIo,
                                                    password: password,
                                                    custType: "UG"
                                                },
                                                function(err, agent) {
                                                    if (err) {
                                                        console.dir(err);
                                                        global.agentAdded = false;
                                                        global.agentAddError = true;
                                                        message = 'Something went wrong';
                                                    } else {
                                                        res.redirect('add_ugagent');
                                                        global.agentAdded = true;
                                                    }
                                                })



                                        }
                                    });
                                }
                            })


                        }
                    })

                }
            });
        }
    } else {
        res.redirect('/sign-in');
    }

});
//list agent

router.get('/list_ugagent', function(req, res, next) {
    if (isUGAdminSessionValid(req)) {
        AgentDataModel.find({ custType: "UG" }, function(err, agents) {
            if (err) {
                console.log(err);
            } else {
                res.render('UGAdmin/agent_list', { title: 'Agent List', message: "", agents: agents, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

//change password
router.get('/get_password_ugadmin', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        res.render('UGAdmin/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/change_password_ugadmin', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var password = req.body.oldpassword;
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;

        if (newpassword !== confirmpassword) {
            res.render('UGAdmin/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "Password and confirm password do no match" });
        } else {
            AdminLoginModel.findOne({ username: global.adminName }, function(err, mso) {
                if (err) {

                } else {
                    if (password !== mso.password) {
                        res.render('UGAdmin/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "Incorrect Password entered" });
                    } else {
                        AdminLoginModel.update({ username: global.adminName }, { $set: { password: newpassword } }, function(err, msoUpdate) {
                            if (err) {

                            } else {
                                AdminLoginModel.update({ username: global.adminName }, { $set: { password: newpassword } }, function(err, data) {
                                    if (err) {

                                    } else {
                                        global.changePassword = true;
                                        res.redirect('sign-in');
                                    }
                                })
                            }
                        })
                    }
                }
            });
        }
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_stakeholder_page', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        res.render('UGAdmin/stake_holder', { title: 'Create Stake Holder', sectionTitle: "Create Stakeholder", sectionMessage: "", message: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_report_page', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        res.render('UGAdmin/management_report', { title: 'Management Reports', sectionTitle: "Management Reports", sectionMessage: "", message: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_update_gold_rate_admin', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        GoldRateModel.find(function(err, rate) {
                if (err) {

                } else {
                    res.render('UGAdmin/gold_rate_report', { title: 'Gold Rate Report', sectionTitle: "Gold Rate Report", sectionMessage: "", agents: rate, message: "" });
                }
            })
            //.sort({date: 1})

    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_new_gold_rate', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        res.render('UGAdmin/new_gold_rate', { title: 'New Gold Rate', sectionTitle: "New Gold Rate", sectionMessage: "", mandatorymessage: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/gold_rate_add', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var goldRate = getUid();
        var rate = req.body.goldRate;
        console.log("new rate : " + rate);
        GoldRateModel.create({ id: goldRate, price: rate }, function(err, gold) {
            if (err) {
                console.log("Err : " + err);
            } else {
                GoldRateModel.find(function(err, rate) {
                    if (err) {

                    } else {
                        res.render('UGAdmin/gold_rate_report', { title: 'Gold Rate Report', sectionTitle: "Gold Rate Report", message: "Rate updated successfully..", sectionMessage: "Rate updated successfully..", agents: rate });
                    }
                })

            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_edit_gold_rate', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var rate = req.param('id');
        global.updatedId = rate;
        console.log("edit waala : " + rate)
        GoldRateModel.findOne({ id: rate },
            function(err, goldRate) {
                if (err) {
                    console.log("error : " + err);
                } else {
                    console.log("gold rate found : " + JSON.stringify(goldRate));
                    res.render('UGAdmin/update_gold_rate', { title: 'Update Gold Rate', sectionTitle: "Update Gold Rate", message: "", mandatorymessage: "", sectionMessage: "", goldRate: goldRate });
                }
            })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/update_gold_rate', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var rate = req.body.goldRate;
        var date = new Date();
        GoldRateModel.update({ id: global.updatedId }, { $set: { price: rate, date: date } }, function(err, rate) {
            if (err) {
                console.log("err in updating rate : " + err);
            } else {
                GoldRateModel.findOne({ id: global.updatedId }, function(err, rate) {
                    if (err) {

                    } else {
                        GoldRateModel.find(function(err, rate) {
                            if (err) {

                            } else {
                                res.render('UGAdmin/gold_rate_report', { title: 'Update Gold Rate', sectionTitle: "Update Gold Rate", message: "Rate Updated Successfully", mandatorymessage: "", sectionMessage: "", agents: rate });
                            }
                        })

                    }
                })

            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/change_lco_code', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var LCOId = req.body.LCOId;
        var name = req.body.name;
        var address = req.body.address;
        var phone1 = req.body.phone1;
        var phone2 = req.body.phone2;
        var contactPerson = req.body.contactPerson;
        LCODataModel.find({ LCOId: req.body.LCOId }, function(err, lco) {
            if (err) {
                console.log(err);
            } else if (lco.length > 0) {
                console.log("LCO Id Already Existing..");

                LCODataModel.findOne({ LCOId: global.selectedLCO }, function(err, lco) {
                    if (err) {

                    } else {
                        res.render('UGAdmin/lco_details', { title: 'LCO Details', message: "LCO Id Already Exists", lcos: lco });
                    }
                })
            } else {
                console.log("creation pending");
                LCODataModel.updateMany({ custType: "UG", LCOId: global.selectedLCO }, { $set: { LCOId: LCOId, name: name, address: address, phone1: phone1, phone2: phone2, contactPerson: contactPerson, idCreated: true } }, function(err, lcoData) {
                    if (err) {
                        console.log("Error in updating LCOData : " + err);
                    } else {
                        LCOLoginModel.updateMany({ custType: "UG", LCOId: global.selectedLCO }, { $set: { LCOId: LCOId, idCreated: true } }, function(err, lcoLogin) {
                            if (err) {
                                console.log("Error in updating LCOLogin : " + err);

                            } else {
                                GSPCustomerModel.updateMany({ LCOId: global.selectedLCO }, { $set: { LCOId: LCOId } }, function(err, customer) {
                                    if (err) {
                                        console.log("Error in updating Customer : " + err);

                                    } else {
                                        GSPGSPCustomerPaymentModel.updateMany({ LCOId: global.selectedLCO }, { $set: { LCOId: LCOId } }, function(err, payment) {
                                            if (err) {
                                                console.log("Error in updating custPayment : " + err);

                                            } else {
                                                GSPLedgerReportModel.updateMany({ LCOId: global.selectedLCO }, { $set: { LCOId: LCOId } }, function(err, ledger) {
                                                    if (err) {
                                                        console.log("Error in updating custLedger : " + err);

                                                    } else {
                                                        AgentDataModel.updateMany({ custType: "UG", LCOId: global.selectedLCO }, { $set: { LCOId: LCOId, LCOName: name, LCOAdd: address } }, function(err, agentData) {
                                                            if (err) {
                                                                console.log("Error in updating AgentData : " + err);

                                                            } else {
                                                                AgentLoginModel.updateMany({ custType: "UG", LCOId: global.selectedLCO }, { $set: { LCOId: LCOId, LCOName: name, LCOAdd: address } }, function(err, agentLogin) {
                                                                    if (err) {
                                                                        console.log("Error in updating Agent Login : " + err);

                                                                    } else {
                                                                        LCODataModel.find({ idCreated: false }, function(err, lco) {
                                                                            if (err) {

                                                                            } else {
                                                                                res.render('UGAdmin/lco_add_list', { title: 'LCO Details', message: "LCO Id created Successfully", lcos: lco });
                                                                            }
                                                                        })
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/ugagent_manipulate', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var agentId = req.body.agentId;
        console.log("agent ID : " + agentId)
        var action = req.body.manipulate;
        console.log("action to b taken  : " + action);
        if (action == "agent_IMEI") {
            AgentLoginModel.update({ agentId: agentId, custType: "UG" }, { $set: { IMEI: "unassigned" } }, function(err, agentUpdate) {
                if (err) {

                } else {
                    AgentDataModel.update({ agentId: agentId, custType: "UG" }, { $set: { IMEI: "unassigned" } }, function(err, agentUpdate) {
                        if (err) {

                        } else {
                            AgentDataModel.find({ custType: "UG" }, function(err, agents) {
                                if (err) {

                                } else {
                                    res.render('UGAdmin/agent_list', { title: 'Agent List', message: "IMEI Reset Successfully", agents: agents });
                                }
                            })
                        }
                    })


                }
            })
        } else if (action === "change_password") {
            console.log("in agent manipulation of " + agentId);
            AgentDataModel.findOne({ agentId: agentId, custType: "UG" }, function(err, agent) {
                if (err) {
                    console.log("error : " + err);
                } else {
                    res.render('UGAdmin/agent_change_password', { title: 'Edit Agent', message: "", sectionMessage: "", agent: agent });
                }
            })

        }
    } else {
        res.redirect('sign-in');
    }
});

router.post('/change_password_agent', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        AgentDataModel.findOne({ agentId: req.body.id, custType: "UG" }, function(err, agent) {
            if (err) {
                console.log("error : " + err);
            } else {
                console.log("Req : " + JSON.stringify(req.body));
                var password = req.body.password;
                var confirmpassword = req.body.conpassword;
                if (password !== confirmpassword) {
                    res.render('UGAdmin/agent_change_password', { title: 'Edit Agent', sectionMessage: "Password and Confirm Password must be same", message: "", agent: agent });
                } else {
                    AgentLoginModel.update({ agentId: req.body.id }, { $set: { password: password } }, function(err, update) {
                        if (err) {
                            res.render('error');
                        } else {
                            AgentDataModel.update({ agentId: req.body.id }, { $set: { password: password } }, function(err, update) {
                                if (err) {
                                    res.render('error');

                                } else {
                                    AgentDataModel.find({ custType: "UG" }, function(err, agents) {
                                        if (err) {

                                        } else {
                                            res.render('UGAdmin/agent_list', { title: 'Agent List', message: "Password Changed Successfully ", agents: agents });
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            }
        });
    } else {
        res.redirect('sign-in');
    }
})


/**
 * For Customer delete from Admin Panel On 14 Sep 2017
 */

router.get('/customer_delete', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        res.render('UGAdmin/customer_delete', { title: "", sectionTitle: "Customer List", sectionMessage: "" })
    } else {
        res.redirect('sign-in');
    }
});

router.post('/customer_delete', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var custId = req.body.custId;
        GSPCustomerModel.find({ customerId: custId }, function(err, cust) {
            if (err) {
                res.status(200).json({ success: false, msg: "Server Error" });
            } else if (cust.length == 0) {
                res.status(200).json({ success: false, msg: "No Customer Found" });
            } else {
                res.status(200).json({ success: true, data: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});


router.get('/delete_customer/:cust_id/:MSOId', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        var cust_id = req.params.cust_id;
        var mso_id = req.params.MSOId;
        // ErrorLogModel.create({cust})
        GSPCustomerModel.remove({ customerId: cust_id, MSOId: mso_id }, function(err, deleteCust) {
            if (err) {
                res.status(200).json({ success: false, msg: "Error While removing customer .. please try again" });
            } else if (!deleteCust) {
                res.status(200).json({ success: true, msg: "Error While removing customer .. please try again" })
            } else {

                GSPCustomerModel.find({ customerId: cust_id }, function(err, cust) {
                    if (err) {
                        res.status(200).json({ success: false, msg: "Server Error" });
                    } else {
                        res.status(200).json({ success: true, msg: "Customer Removed Successfully", data: [] })
                    }
                })
            }
        })
    } else {
        res.redirect('sign-in');
    }
})

/**
 * END
 */

/**
 * Payment Revoke
 */

router.get('/get_payment_ugadmin', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        if (global.getPaymentError) {
            res.render('UGAdmin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Incorrect Receipt Id' });
        }
        if (global.editPayment) {
            res.render('UGAdmin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Payment Revoked Successfully' });
        }
        if (global.editPaymentError) {
            res.render('UGAdmin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: 'Error in Revoking Payment please try again' });
        } else {
            res.render('UGAdmin/search_payment', { title: '', sectionTitle: 'Payment Revoke', sectionMessage: '' });
        }
    } else {
        res.redirect('sign-in');
    }
})
router.post('/edit_payment_ugadmin', function(req, res, next) {
    var sess = req.session;

    if (isUGAdminSessionValid(req)) {
        console.log(req.body.payId);
        var id = req.body.payId;
        global.paymentID = req.body.payId;
        GSPCustomerPaymentModel.findOne({ receiptId: id, status: "PAID" }, function(err, pay) {
            if (err) {
                console.log("error in finding Payment " + err);

            } else if (!pay) {
                console.log("in !pay");
                global.getPaymentError = true;
                res.redirect('get_payment_ugadmin');
            } else {
                console.log("Payment Details : " + JSON.stringify(pay));
                global.paidAmount = pay.paidAmount;
                global.customerId = pay.customerId;
                console.log("Paid Amt : " + global.paidAmount);
                res.render('UGAdmin/payment_revoke', { title: '', sectionTitle: 'Payment Revoke', payment: pay, sectionMessage: 'Payment Details' });
            }
        });

    } else {
        res.redirect('sign-in');
    }
});


router.post('/payment_edit_ugadmin', function(req, res, next) {
    var sess = req.session;
    if (isUGAdminSessionValid(req)) {
        console.log("payment id : " + global.paymentID);
        GSPCustomerPaymentModel.update({ receiptId: global.paymentID }, { $set: { status: "REVOKE" } }, function(err, pay) {
            if (err) {
                console.log("Payment delete Error" + err);
            } else if (!pay) {
                global.editPaymentError = true;
                global.editPayment = false;
                res.redirect('get_payment_ugadmin');
            } else {

                console.log("Paid Amt in update : " + global.paidAmount);
                GSPCustomerModel.update({ customerId: global.customerId }, {
                    $inc: { balanceAmount: global.paidAmount, totalPaidAmount: -global.paidAmount }
                }, function(err, cust) {
                    if (err) {

                    } else {
                        GSPCustomerModel.findOne({ customerId: global.customerId }, function(err, custPay) {
                            if (err) {

                            } else {
                                var startDate = moment().startOf('day').format("DD-MM-YYYY");
                                console.log("cs " + custPay.name);
                                console.log("balance and netplot : " + custPay.balanceAmount)
                                GSPCustomerPaymentModel.create({
                                    receiptId: "NA",
                                    ledgerType: "PAYMENT REVOKE",
                                    status: "REVOKE",
                                    paidAmount: 0,
                                    drAmt: global.paidAmount,
                                    balance: custPay.balanceAmount,
                                    netPlotAmount: custPay.netPlotAmount,
                                    customerId: global.customerId,
                                    name: custPay.name,
                                    MSOId: custPay.MSOId,
                                    LCOId: custPay.LCOId,
                                    AgentId: pay.AgentId,
                                }, function(err, ledger) {
                                    if (err) {

                                    } else {
                                        LedgerReportModel.create({
                                            receiptId: "NA",
                                            ledgerType: "PAYMENT REVOKE",
                                            status: "REVOKE",
                                            paidAmount: 0,
                                            drAmt: global.paidAmount,
                                            balance: custPay.balanceAmount,
                                            netPlotAmount: custPay.netPlotAmount,
                                            customerId: global.customerId,
                                            name: custPay.name,
                                            MSOId: custPay.MSOId,
                                            LCOId: custPay.LCOId,
                                            AgentId: pay.AgentId,
                                        }, function(err, ledger) {
                                            if (err) {

                                            } else {

                                                console.log(JSON.stringify(pay));
                                                global.editPaymentError = false;
                                                global.editPayment = true;
                                                res.redirect('get_payment_ugadmin');
                                            }
                                        })

                                    }
                                })
                            }
                        })


                    }
                })

            }

        });
    } else {
        res.redirect('sign-in');
    }
});

/**
 * End
 */
router.get('/ugsignout', function(req, res, next) {
    var sess = req.session;
    if (sess.ugadmin != undefined) {
        sess.ugadmin = undefined;
    } else if (sess.center != undefined) {
        sess.center = undefined;
    }
    res.render('signout');
})

module.exports = router;