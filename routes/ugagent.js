var express = require('express');
var router = express.Router();
var AgentLoginModel = require('../models/AgentLogin.js');
var AgentDataModel = require('../models/AgentData.js');
var GSPCustomerModel = require('../models/GSPCustomerDetails.js');
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var VersionModel = require('../models/Version.js');
var GSPLedgerReportModel = require('../models/GSPLedgerReport.js');
var GSPCustomerPaymentModel = require('../models/GSPCustomerPayment.js');
var request = require('request');
var getUid = require('get-uid');
var MAIN_URI = "http://103.15.179.45:8085/MessagingGateway/SendTransSMS?Username=CATVCGT&Password=catvt20716&MessageType=txt&Mobile=";
var MOBILE_NO = '7208347027';
var BALANCE = "";
var SENDER_URI = "&SenderID=CATVCG&Message=";
var MESSAGE_TEMPLATE = "ALPHA TO CHARLIE";
var LCODataModel = require('../models/LCOData.js');
var shortid = require('shortid');
var MOBILE_NO = '7208347027';
var BALANCE = "";
var SENDER_URI = "&SenderID=CATVCG&Message=";
var MESSAGE_TEMPLATE = "ALPHA TO CHARLIE";
var MessageLogModel = require('../models/MessageLogs.js');

function CustomerDetail() {

}

function customerPayment() {

};

function sendSMS(lco, mobile, template) {
    console.log("MSG SEND");
    console.log(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE);
    request
        .get(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE)
        .on('response', function(response) {
            console.log(response.statusCode); // 200
            console.log(response.headers['content-type']); // 'image/png'
            if (response.statusCode == 200) {

                MessageLogModel.create({
                    MSOId: lco.MSOId,
                    LCOId: lco.LCOId,
                    sentTo: mobile,
                    MessageContent: template,
                    MessageType: "Payment Message",
                    status: "Success"
                }, function(err, msg) {
                    if (err) {

                    } else {
                        console.log("Payment message send");
                    }
                });

            }
            /*else{
              res.status(500).json({response:response});
            }*/
        })
}

function isGSPAgentValid(agentId, IMEI) {
    AgentLoginModel.find({ agentId: agentId, IMEI: IMEI }, function(err, agent) {
        console.log('agent find ' + agent);
        if (agent.length == 1) {
            console.log('agent svalid ');
            return true;
        } else {
            console.log('agent invalid ')
            return false;
        }
    });
}

router.post('/gsp_authenticate', function(req, res, next) {
    var requestJSON = req.body;
    console.dir('req body auth ' + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: requestJSON.username, password: requestJSON.password, custType: "UG" }, function(err, agent) {
        console.dir('agent ' + agent);
        if (err) {
            console.log("Error : " + err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 0) {
            //Not found
            console.log("Agent Not Found");
            res.status(403).json({ "status": 403, "agent": agent });
        } else {
            var agentIMEI = agent[0].IMEI;
            var openIMEI = agent[0].isOpen;
            console.log(requestJSON.IMEI);
            console.log("agen IMEI : " + agent[0].IMEI);
            console.log("is agent IMEI open : " + openIMEI);
            if (openIMEI == true) {
                AgentLoginModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                    if (err) {
                        res.status(500).json({ "status": 500 });
                    } else {
                        AgentDataModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                            if (err) {
                                res.status(500).json({ "status": 500 });
                            } else {
                                AgentLoginModel.findOne({ agentId: requestJSON.username, password: requestJSON.password }, function(err, agent) {
                                    if (err) {
                                        res.status(500).json({ "status": 500 });
                                    }
                                    if (agent) {
                                        AgentLoginModel.findOne({
                                            agentId: requestJSON.username
                                        }, function(err, agent) {
                                            if (err) {
                                                res.status(500).json({ "status": 500 });
                                            } else {

                                                res.status(200).json({ "status": 200, agent: agent });
                                            }
                                        })
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                AgentLoginModel.find({ IMEI: requestJSON.IMEI }, function(err, agent) {
                    if (err) {
                        res.status(500).json({ "status": 500 });
                    } else {
                        if (agent.length > 0) {
                            AgentLoginModel.find({ agentId: requestJSON.username, password: requestJSON.password, IMEI: requestJSON.IMEI },
                                function(err, agent) {
                                    if (agent.length == 1) {
                                        AgentDataModel.findOne({ agentId: requestJSON.username },
                                            function(err, agent) {
                                                if (err) {
                                                    res.status(500).json({ "status": 500 });
                                                } else {
                                                    res.status(200).json({ "status": 200, agent: agent });
                                                }
                                            });

                                    } else {
                                        res.status(409).json({ "status": 409 });
                                    }
                                });
                        } else {
                            console.log("in unassigned");
                            if (agentIMEI == "unassigned") {
                                AgentLoginModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                                    if (err) {
                                        res.status(500).json({ "status": 500 });
                                    } else {
                                        AgentDataModel.update({ agentId: requestJSON.username }, { IMEI: requestJSON.IMEI }, { upsert: true }, function(err, agent) {
                                            if (err) {
                                                res.status(500).json({ "status": 500 });
                                            } else {
                                                AgentLoginModel.findOne({ agentId: requestJSON.username, password: requestJSON.password },
                                                    function(err, agent) {
                                                        if (err) {
                                                            res.status(500).json({ "status": 500 });
                                                        }
                                                        if (agent) {
                                                            AgentDataModel.findOne({
                                                                agentId: requestJSON.username
                                                            }, function(err, agent) {
                                                                if (err) {
                                                                    res.status(500).json({ "status": 500 });
                                                                } else {

                                                                    res.status(200).json({ "status": 200, agent: agent })
                                                                }
                                                            })
                                                        }
                                                    });
                                            }
                                        });
                                    }
                                });
                            }
                        }
                    }
                })
            }
        }
    });
});

router.post('/gsp_customerList', function(req, res, next) {
    var agentId = req.body.agentId;
    var IMEI = req.body.imei;
    var LCOId = req.body.LCOId;
    var MSOId = req.body.MSOId;

    console.dir('req body cust ' + JSON.stringify(req.body));

    LCODataModel.findOne({ LCOId: LCOId, custType: "UG" }, function(err, lcoDetails) {
        if (err) {
            res.status(500).json({ "status": 500 });
        } else if (!lcoDetails) {
            res.status(403).json({ "status": 403 });
        } else {
            AgentLoginModel.find({ agentId: agentId, IMEI: IMEI }, function(err, agent) {
                console.log('agent find ' + agent);
                if (agent.length == 1) {
                    console.log('agentvalid ');
                    var requestJSON = req.body;
                    console.dir('req body ' + JSON.stringify(req.body));
                    GSPCustomerModel.find({ MSOId: lcoDetails.MSOId, balanceAmount: { $gt: 0 } }, function(err, customers) {
                        if (err) {
                            res.status(500).json({ "status": 500 });
                        } else {
                            var customerArray = new Array();
                            for (var i = 0; i < customers.length; i++) {
                                var customer = new CustomerDetail();
                                customer.customerId = customers[i].customerId;
                                customer.name = customers[i].name;
                                customer.LCOId = customers[i].LCOId;
                                customer.MSOId = customers[i].MSOId;
                                customer.DistId = customers[i].DistId;
                                customer.address = customers[i].address;
                                customer.mobileNo = customers[i].mobileNo;
                                customer.phone = customers[i].phone;
                                customer.planAmt = customers[i].planAmt;
                                customer.paidAmt = customers[i].paidAmt;
                                customer.balanceAmount = customers[i].balanceAmount;
                                customer.planName = customers[i].planName;
                                customer.joinId = customers[i].joinId;
                                // if(customers[i].emi==undefined ){
                                //     customers[i].emi=3000;
                                // }
                                if(customers[i].emi==undefined || customers[i].emi==0){
                                        if(customers[i].planName.includes("15")){
                                            customers[i].emi=parseFloat(customers[i].planAmt/15);
                                        }else if(customers[i].planName.includes("11")){
                                            customers[i].emi=parseFloat(customers[i].planAmt/11);
                                        }else if(customers[i].planName.includes("12")){
                                            customers[i].emi=parseFloat(customers[i].planAmt/12);
                                        }
                                    }
                                customer.emi= customers[i].emi;
                                customer.salesman = customers[i].salesman;
                                customer.planDate = customers[i].planDate;
                                customer.dueDate = customers[i].dueDate;
                                customerArray.push(customer);
                                //console.log(customerArray);
                            }
                            console.log(customerArray[0]);
                            res.status(200).json({ "status": 200, customers: customerArray });
                        }
                    });
                } else {
                    res.status(403).json({ "status": 403 });
                    console.log('agent invalid ')
                    return false;
                }
            });
        }
    })

})

router.post('/ugpayment/customer', function(req, res, next) {
    console.dir("JSON: " + JSON.stringify(req.body));
    var sgst, cgst, amt;
    AgentLoginModel.find({ agentId: req.body.agentId, IMEI: req.body.imei }, function(err, agent) {
        if (err) {
            console.log(err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 1) {
            console.log("in agent");
            GSPCustomerModel.find({ customerId: req.body.customerId, MSOId: req.body.MSOId }, function(err, cust) {
                // console.log(cust.length)
                if (err) {
                    console.log(err);
                    res.status(500).json({ "status": 500 });
                } else if (cust.length == 1) {
                    GSPCustomerPaymentModel.find(function(err, payment) {
                        if (err) {
                            res.status(500).json({ "status": 500 });
                        } else if (payment.length == 0) {

                            if(parseFloat(req.body.paidAmount)<parseFloat(cust.emi)){
                                res.status(200).json({ "status": 200,"success":1})
                            }else
                            {
                                var receiptId = "00001";
                                console.log("in 0 : " + receiptId);
                                var gstAmount = (parseFloat(req.body.paidAmount) * 3) / 103;
                                sgst = gstAmount / 2;
                                cgst = gstAmount / 2;
    
    
                                amt = parseFloat(req.body.paidAmount) - sgst - cgst;
    
                                var custPay = new customerPayment();
                                custPay.receiptId = receiptId;
                                custPay.customerId = req.body.customerId;
                                custPay.name = req.body.name;
                                custPay.address = req.body.address;
                                custPay.mobileNo = req.body.mobileNo;
                                custPay.planName = req.body.planName;
                                custPay.planAmt = req.body.planAmt;
                                custPay.joinId = cust[0].joinId;
                                custPay.paidAmount = req.body.paidAmount;
                                custPay.payMode = req.body.mode;
                                var balance = parseFloat(cust[0].balanceAmount) - parseFloat(req.body.paidAmount);
                                console.log("details : " + cust[0].balanceAmount + " and " + req.body.paidAmount + " balance : " + balance);
                                custPay.balanceAmt = balance;
                                custPay.chequeNo = req.body.chequeNo;
                                custPay.bankName = req.body.bankName;
                                custPay.agentId = req.body.agentId;
                                custPay.MSOId = req.body.MSOId;
                                custPay.sgst = sgst;
                                custPay.cgst = cgst;
                                custPay.amt = amt;
                                custPay.LCOId = cust[0].LCOId;
                                custPay.DistId = cust[0].DistId;
                                custPay.remarks = req.body.remarks;
                                custPay.chequeDate = req.body.chequeDate;
                                console.log("customer ID : " + custPay.customerId);
                                GSPCustomerPaymentModel.create(custPay, function(err, payment) {
                                    if (err) {
                                        res.status(500).json({ "status": 500 });
                                    } else {
                                        GSPLedgerReportModel.create(custPay, function(err, ledger) {
                                            if (err) {
                                                res.status(500).json({ "status": 500 });
                                            } else {
                                                BALANCE = custPay.balanceAmt;
                                                GSPCustomerModel.update({ customerId: custPay.customerId, MSOId: req.body.MSOId }, { $inc: { balanceAmount: -parseFloat(custPay.paidAmount), paidAmt: parseFloat(custPay.paidAmount) } },
                                                    function(err, update) {
                                                        if (err) {
                                                            res.status(500).json({ "status": 500 });
                                                        } else {
                                                            GSPCustomerModel.findOne({ customerId: custPay.customerId, MSOId: req.body.MSOId }, function(err, customerDetails) {
                                                                if (err) {
                                                                    res.status(500).json({ "status": 500 });
                                                                } else {
                                                                    LCODataModel.find({ LCOId: custPay.LCOId, custType: "UG" }, function(err, lco) {
                                                                        if (err) {
    
                                                                        } else {
                                                                            MSODataModel.find({ MSOId: lco[0].MSOId, custType: "UG" }, function(err, mso) {
                                                                                if (err) {
    
                                                                                } else {
                                                                                    console.log("MSO : " + JSON.stringify(mso));
    
                                                                                    console.log(JSON.stringify(ledger));
                                                                                    res.status(200).json({ "status": 200,success:"0", "paymentId": receiptId, "cust": cust, "mso": mso, "lco": lco });
    
                                                                                }
                                                                            });
    
    
                                                                        }
                                                                    })
                                                                }
                                                            });
                                                        }
                                                    });
                                                //For Send Payment SMS
                                                LCODataModel.findOne({
                                                    LCOId: req.body.LCOId,
                                                }, function(err, lco) {
                                                    if (err) {
                                                        console.log("Error 3" + err);
    
                                                    } else {
                                                        if (lco) {
                                                            console.log("mobile number : " + custPay.mobileNo)
                                                                // MESSAGE_TEMPLATE = 'Dear ' + cust.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' + req.body.mode + ', Receipt No. :' +
                                                                //     receiptId + '.Your outstanding balance Rs.' + BALANCE + '/- Thanks from ' + lco.name;
                                                            MESSAGE_TEMPLATE = 'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + custPay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                receiptId + '.Your total due amount is Rs.' + BALANCE + ' as on date ' +moment().format("DD-MM-YYYY hh:mm:ss a");
                                                            //   if(custPay.mode=="Cash"){
                                                            //   MESSAGE_TEMPLATE='Dear '+req.body.name+', Received with thanks Rs. '+custPay.paidAmount+'/- In Cash, Receipt No. :'
                                                            //   +receiptId+'.Your outstanding balance Rs.'+custPay.balance+'/- Thanks from Isp network';
                                                            //   MOBILE_NO=cust.contact;
                                                            //   sendSMS(isp);
                                                            //
                                                            // }
                                                            // else{
                                                            //   MESSAGE_TEMPLATE='Dear '+req.body.name+', Received with thanks Rs. '+custPay.paidAmount+'/- In Cheque with number' +req.body.chequeNo +' , Receipt No. :'
                                                            //   +receiptId+'.Your outstanding balance Rs.'+custPay.balance+'/- Thanks from Isp network';
                                                            //   MOBILE_NO=cust.contact;
                                                            //   sendSMS(isp);
                                                            //
                                                            // }
                                                            GSPCustomerModel.findOne({ customerId: custPay.customerId, MSOId: req.body.MSOId }, function(err, customerDetails) {
                                                                if (err) {
                                                                    console.log("Error 4 : " + err);
                                                                } else {
                                                                    console.log("in customer Details")
                                                                    console.log("updated Details : " + JSON.stringify(customerDetails));
                                                                    custPay.balance = customerDetails.balanceAmount;
                                                                    console.log("balance : " + custPay.balance);
                                                                    BALANCE = custPay.balance;
                                                                    if (custPay.mode === "Cash") {
                                                                        console.log("balance in lco : " + BALANCE)
                                                                        MESSAGE_TEMPLATE =
                                                                            // 'Dear ' + custPay.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                                            //     req.body.mode + ', Receipt No. :' +
                                                                            //     receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                                            'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + custPay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                            receiptId + '.Your total due amount is Rs.' + BALANCE + ' as on date ' + moment().format("DD-MM-YYYY hh:mm:ss a");
                                                                    } else if (custPay.mode === "Cheque") {
                                                                        // MESSAGE_TEMPLATE = 'Dear ' + custPay.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                                        //     req.body.mode + ' (Bank Name:  ' + custPay.bankName + ', Cheque No. :' + custPay.chequeNo + '),  Receipt No. :' +
                                                                        //     receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                                        'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + custPay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                            receiptId + '.Your total due amount is Rs.' + BALANCE + ' as on date ' +moment().format("DD-MM-YYYY hh:mm:ss a");
                                                                    }
                                                                    MOBILE_NO = custPay.mobileNo;
                                                                    console.log("mob : " + MOBILE_NO)
                                                                    sendSMS(lco, MOBILE_NO, MESSAGE_TEMPLATE);
                                                                    sendSMS(lco, lco.phone1, MESSAGE_TEMPLATE);
                                                                }
                                                            });
    
                                                            // MOBILE_NO="7208347027";
                                                        }
                                                    }
                                                });
                                                //End
                                            }
                                        })
                                    }
                                })
                            }

                            

                        } else {
                            if(cust[0].emi==undefined || cust[0].emi==0){
                                cust[0].emi=0;
                                if(cust[0].planName.includes("15")){
                                    cust[0].emi=parseFloat(cust[0].planAmt/15);
                                }else if(cust[0].planName.includes("11")){
                                    cust[0].emi=parseFloat(cust[0].planAmt/11);
                                }else if(cust[0].planName.includes("12")){
                                    cust[0].emi=parseFloat(cust[0].planAmt/12);
                                }
                            }
                            if(parseFloat(req.body.paidAmount)!==parseFloat(cust[0].emi)){
                                res.status(200).json({ "status": 200,"success":"1"})
                            }else if(parseFloat(req.body.paidAmount)==parseFloat(cust[0].emi))
                            {
                            var form = payment[0].receiptId;
                            var integerID = parseInt(form) + 1;
                            if (integerID < 10) {
                                integerID = "0000" + integerID;
                            } else if (integerID < 100) {
                                integerID = "000" + integerID;
                            } else if (integerID < 1000) {
                                integerID = "00" + integerID;
                            } else if (integerID < 10000) {
                                integerID = "0" + integerID;
                            }
                            var receiptId = "" + integerID;
                            console.log("in all : " + receiptId);

                            var custPay = new customerPayment();
                            custPay.receiptId = receiptId;

                            var gstAmount = (parseFloat(req.body.paidAmount) * 3) / 103;
                            sgst = gstAmount / 2;
                            cgst = gstAmount / 2;


                            amt = parseFloat(req.body.paidAmount) - sgst - cgst;
                            custPay.customerId = req.body.customerId;
                            custPay.name = req.body.name;
                            custPay.address = req.body.address;
                            custPay.mobileNo = req.body.mobileNo;
                            custPay.planName = req.body.planName;
                            custPay.planAmt = req.body.planAmt;
                            custPay.paidAmount = req.body.paidAmount;
                            custPay.payMode = req.body.mode;
                            custPay.joinId = cust[0].joinId;
                            var balance = parseFloat(cust[0].balanceAmount) - parseFloat(req.body.paidAmount);
                            console.log("details : " + cust[0].balanceAmount + " and " + req.body.paidAmount + " balance : " + balance);
                            custPay.balanceAmt = balance;
                            custPay.chequeNo = req.body.chequeNo;
                            custPay.bankName = req.body.bankName;
                            custPay.agentId = req.body.agentId;
                            custPay.MSOId = req.body.MSOId;
                            custPay.LCOId = cust[0].LCOId;
                            custPay.DistId = cust[0].DistId;
                            custPay.sgst = sgst;
                            custPay.cgst = cgst;
                            custPay.amt = amt;
                            custPay.remarks = req.body.remarks;
                            custPay.chequeDate = req.body.chequeDate;
                            console.log("customer ID : " + custPay.customerId);
                            GSPCustomerPaymentModel.create(custPay, function(err, payment) {
                                if (err) {
                                    res.status(500).json({ "status": 500 });
                                } else {
                                    GSPLedgerReportModel.create(custPay, function(err, ledger) {
                                        if (err) {
                                            res.status(500).json({ "status": 500 });
                                        } else {
                                            BALANCE = custPay.balanceAmt;
                                            GSPCustomerModel.update({ customerId: custPay.customerId, MSOId: req.body.MSOId }, { $inc: { balanceAmount: -parseFloat(custPay.paidAmount), paidAmt: parseFloat(custPay.paidAmount) } },
                                                function(err, update) {
                                                    if (err) {
                                                        res.status(500).json({ "status": 500 });
                                                    } else {
                                                        GSPCustomerModel.findOne({ customerId: custPay.customerId, MSOId: req.body.MSOId }, function(err, customerDetails) {
                                                            if (err) {
                                                                res.status(500).json({ "status": 500 });
                                                            } else {
                                                                LCODataModel.find({ LCOId: custPay.LCOId, custType: "UG" }, function(err, lco) {
                                                                    if (err) {

                                                                    } else {
                                                                        console.log(" LCO 0 " + JSON.stringify(lco));
                                                                        MSODataModel.find({ MSOId: lco[0].MSOId, custType: "UG" }, function(err, mso) {

                                                                            if (err) {

                                                                            } else {
                                                                                console.log("MSO : " + JSON.stringify(mso));

                                                                                console.log(JSON.stringify(ledger));
                                                                                GSPCustomerPaymentModel.find({ receiptId: receiptId, MSOId: lco[0].MSOId }, function(err, pay_data) {
                                                                                    if (err) {

                                                                                    } else {
                                                                                        res.status(200).json({ "status": 200,success:"0", "paymentId": receiptId, "cust": cust, "mso": mso, "lco": lco, "pay_details": pay_data });
                                                                                    }
                                                                                })


                                                                            }
                                                                        });


                                                                    }
                                                                })
                                                            }
                                                        });
                                                    }
                                                });

                                            //FOR Payment SMS
                                            LCODataModel.findOne({
                                                LCOId: req.body.LCOId,
                                            }, function(err, lco) {
                                                if (err) {
                                                    console.log("Error 3" + err);

                                                } else {
                                                    if (lco) {
                                                        console.log("mobile number : " + custPay.mobileNo)
                                                        MESSAGE_TEMPLATE = 'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + custPay.paidAmount + '/- ' + ' Receipt No. :' +
                                                            receiptId + '.Your total due amount is Rs.' + BALANCE + ' as on date ' + moment().format("DD-MM-YYYY hh:mm:ss a");
                                                        // 'Dear ' + cust.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' + req.body.mode + ', Receipt No. :' +
                                                        //     receiptId + '.Your outstanding balance Rs.' + BALANCE + '/- Thanks from ' + lco.name;
                                                        //   if(custPay.mode=="Cash"){
                                                        //   MESSAGE_TEMPLATE='Dear '+req.body.name+', Received with thanks Rs. '+custPay.paidAmount+'/- In Cash, Receipt No. :'
                                                        //   +receiptId+'.Your outstanding balance Rs.'+custPay.balance+'/- Thanks from Isp network';
                                                        //   MOBILE_NO=cust.contact;
                                                        //   sendSMS(isp);
                                                        //
                                                        // }
                                                        // else{
                                                        //   MESSAGE_TEMPLATE='Dear '+req.body.name+', Received with thanks Rs. '+custPay.paidAmount+'/- In Cheque with number' +req.body.chequeNo +' , Receipt No. :'
                                                        //   +receiptId+'.Your outstanding balance Rs.'+custPay.balance+'/- Thanks from Isp network';
                                                        //   MOBILE_NO=cust.contact;
                                                        //   sendSMS(isp);
                                                        //
                                                        // }
                                                        GSPCustomerModel.findOne({ customerId: custPay.customerId, MSOId: req.body.MSOId }, function(err, customerDetails) {
                                                            if (err) {
                                                                console.log("Error 4 : " + err);
                                                            } else {
                                                                console.log("in customer Details")
                                                                console.log("updated Details : " + JSON.stringify(customerDetails));
                                                                custPay.balance = customerDetails.balanceAmount;
                                                                console.log("balance : " + custPay.balance);
                                                                BALANCE = custPay.balance;
                                                                if (custPay.mode === "Cash") {
                                                                    console.log("balance in lco : " + BALANCE)
                                                                    MESSAGE_TEMPLATE =
                                                                        // 'Dear ' + custPay.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                                        //     req.body.mode + ', Receipt No. :' +
                                                                        //     receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                                        'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + custPay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                        receiptId + '.Your total due amount is Rs.' + BALANCE + ' as on date ' +moment().format("DD-MM-YYYY hh:mm:ss a");
                                                                } else if (custPay.mode === "Cheque") {
                                                                    MESSAGE_TEMPLATE =
                                                                        // 'Dear ' + custPay.name + ', Received with thanks Rs. ' + custPay.paidAmount + '/- In ' +
                                                                        //     req.body.mode + ' (Bank Name:  ' + custPay.bankName + ', Cheque No. :' + custPay.chequeNo + '),  Receipt No. :' +
                                                                        //     receiptId + '\n Your outstanding balance Rs.' + BALANCE + '/- \n Thanks from ' + lco.name;
                                                                        'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + custPay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                        receiptId + '.Your total due amount is Rs.' + BALANCE + ' as on date ' +moment().format("DD-MM-YYYY hh:mm:ss a");
                                                                }
                                                                MOBILE_NO = custPay.mobileNo;
                                                                console.log("mob : " + MOBILE_NO)
                                                                sendSMS(lco, MOBILE_NO, MESSAGE_TEMPLATE);
                                                            }
                                                        });

                                                        // MOBILE_NO="7208347027";
                                                    }
                                                }
                                            });
                                            //End
                                        }
                                    })
                                }
                            })
                        }
                        }
                    }).sort({ $natural: -1 }).limit(1);
                } else {
                    console.log("customer not found");
                    res.status(403).json({ "status": 403 });

                }
            });
        }
    });

});

router.post('/ugchangePassword', function(req, res, next) {
    var AgentId = req.body.agentId;
    var imei = req.body.imei;
    var oldPass = req.body.oldPass;
    var newpassword = req.body.password;
    console.log("Agent ID : " + AgentId + " , old : " + oldPass + " , new : " + newpassword)

    AgentLoginModel.findOne({ agentId: AgentId, password: oldPass }, function(err, agent) {
        if (err) {
            res.status(500).json({ "status": 500 });
        } else {
            AgentLoginModel.update({ agentId: AgentId }, { $set: { password: newpassword } }, function(err, agentUpdate) {
                if (err) {
                    res.status(500).json({ "status": 500 });
                } else if (agentUpdate.length == 0) {
                    //Not found
                    res.status(403).json({ "status": 403 });
                } else {
                    AgentDataModel.update({ agentId: AgentId }, { $set: { password: newpassword } }, function(err, agentUpdate) {
                        if (err) {
                            res.status(500).json({ "status": 500 });
                        } else if (agentUpdate.length == 0) {
                            //Not found
                            res.status(403).json({ "status": 403 });
                        } else {
                            console.log("updatd : " + JSON.stringify(agentUpdate));
                            res.status(200).json({ "status": 200 });
                        }
                    });
                }
            });
        }
    });

});
router.post('/ugterminal_report', function(req, res, next) {
    console.dir("JSON: " + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: req.body.agentId, IMEI: req.body.imei }, function(err, agent) {
        if (err) {
            console.log(err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 1) {
            var agentId = req.body.agentId;
            var startDate = req.body.startDate;
            var endDate = req.body.endDate;
            startDate = moment(startDate).startOf('day').format();
            endDate = moment(endDate).endOf('day').format();
            console.log("dates : " + startDate + " and " + endDate);
            GSPCustomerPaymentModel.find({ agentId: req.body.agentId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } },
                function(err, pay) {
                    if (err) {
                        res.status(500).json({ "status": 500 });
                    } else {
                        var total_amt = 0;
                        var cash = 0;
                        var chequeCount = 0;
                        var cashCount = 0;
                        var cheque = 0;
                        console.log("payment length : " + pay.length);
                        for (var j = 0; j < pay.length; j++) {
                            if (pay[j].payMode === "Cash") {
                                cashCount = cashCount + 1;
                                cash = cash + parseFloat(pay[j].paidAmount);
                            } else if (pay[j].payMode === "Cheque") {
                                chequeCount = chequeCount + 1;
                                cheque = cheque + parseFloat(pay[j].paidAmount);
                            }
                            total_amt = total_amt + parseFloat(pay[j].paidAmount);
                        }
                        res.status(200).json({ "status": 200, "cashCount": cashCount, "chequeCount": chequeCount, "cash": cash, "cheque": cheque, "total_amt": total_amt });
                    }
                })


        }
    })
});
router.post('/ugreprint_last_transaction', function(req, res, next) {
    console.dir("JSON: " + JSON.stringify(req.body));
    AgentLoginModel.find({ agentId: req.body.agentId, IMEI: req.body.imei }, function(err, agent) {
        if (err) {
            console.log(err);
            res.status(500).json({ "status": 500 });
        } else if (agent.length == 1) {
            GSPCustomerPaymentModel.find({ agentId: req.body.agentId, status: "PAID" }, function(err, payment) {
                if (err) {
                    res.status(500).json({ "status": 500 });
                    console.log("Error in fetching Last transaction " + err);
                } else {
                    console.log("payment length : " + payment.length);
                    console.log("last transaction : " + JSON.stringify(payment));
                    var recDate = moment(payment[0].created_at).format("DD/MM/YYYY HH.mm.ss");
                    console.log("date : " + recDate);
                    res.status(200).json({ "status": 200, "reprint": payment });
                }

            }).sort({ $natural: -1 }).limit(1);
        }

    })
});

module.exports = router;