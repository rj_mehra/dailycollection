
var express = require('express');
var router = express.Router();
var fs = require("fs");
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var UGMSOLoginModel = require('../models/UGMSOLogin.js');
var UGLCOLoginModel = require('../models/UGLCOLogin.js');
var UGAgentLoginModel = require('../models/UGAgentLogin.js');
var UGDistLoginModel = require('../models/UGDistLogin.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var GSPCustomerModel = require('../models/GSPCustomerDetails.js');
var AdminLoginModel = require('../models/AdminLogin.js');
var VersionModel = require('../models/Version.js');
var GoldRateModel = require('../models/GoldRate.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var MessageLogModel = require('../models/MessageLogs.js');
var errorLogModal = require('../models/errorLog.js');
var GSPLedgerReportModel = require('../models/GSPLedgerReport.js');
var GSPCustomerPaymentModel = require('../models/GSPCustomerPayment.js');
var MAIN_URI = "http://103.15.179.45:8085/MessagingGateway/SendTransSMS?Username=CATVCGT&Password=catvt20716&MessageType=txt&Mobile=";

var request = require('request');
var SENDER_URI = "&SenderID=CATVCG&Message=";
var pureArray1 = new Array();
var pureArray = new Array();
var getUid = require('get-uid');
var Converter = require("csvtojson").Converter;
var converter = new Converter({ constructResult: false });

function isUGLCOSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.uglco == undefined)) {
        console.log("req body " + JSON.stringify(req.body));
        global.LCOId = sess.uglco.LCOId;
        global.name = sess.uglco.name;
        global.MSOId = sess.uglco.MSOId;
        loginDate = sess.uglco.date;
        if (global.agentLogin) {
            global.AgentId = sess.uglco.agentId;
        }
        return true;
    } else {
        return false;
    }
}


function sendSMS(lco, MOBILE_NO, MESSAGE_TEMPLATE) {
    console.log("MSG SEND");
    console.log(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE);
    request
        .get(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE)
        .on('response', function(response) {
            console.log(response.statusCode); // 200
            console.log(response.headers['content-type']); // 'image/png'
            if (response.statusCode == 200) {

                MessageLogModel.create({
                    MSOId: lco.MSOId,
                    LCOId: lco.LCOId,
                    sentTo: MOBILE_NO,
                    MessageContent: MESSAGE_TEMPLATE,
                    MessageType: "Payment Message",
                    status: "Success"
                }, function(err, msg) {
                    if (err) {

                    } else {
                        console.log("Payment message send");
                    }
                });

            }
            /*else{
              res.status(500).json({response:response});
            }*/
        })
}

function sendCustomerSMS(lco, MOBILE_NO, MESSAGE_TEMPLATE) {
    console.log("MSG SEND");
    console.log(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE);
    request
        .get(MAIN_URI + MOBILE_NO + SENDER_URI + MESSAGE_TEMPLATE)
        .on('response', function(response) {
            console.log(response.statusCode); // 200
            console.log(response.headers['content-type']); // 'image/png'
            if (response.statusCode == 200) {

                MessageLogModel.create({
                    MSOId: lco.MSOId,
                    LCOId: lco.LCOId,
                    sentTo: MOBILE_NO,
                    MessageContent: decodeURIComponent(MESSAGE_TEMPLATE),
                    MessageType: "New Connection Message",
                    status: "Success"
                }, function(err, msg) {
                    if (err) {

                    } else {
                        console.log("Payment message send");
                    }
                });

            }
            /*else{
              res.status(500).json({response:response});
            }*/
        })
}





function GSPCustomer() {

};

function GSPCustomerayment() {

};
router.get('/dashboard_UGLCO', function(req, res, next) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (isUGLCOSessionValid(req)) {



        res.render('UGLCO/index', {
            title: 'GSP',
            message: global.message,
            name: "Administrator" //extra added for avoiding crash
        });

    } else {
        res.redirect('/');
    }
});

//start

//gold rate


router.get('/get_update_gold_rate_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        GoldRateModel.find(function(err, rate) {
                if (err) {

                } else {
                    res.render('UGLCO/gold_rate_report', { title: 'Gold Rate Report', sectionTitle: "Gold Rate Report", sectionMessage: "", agents: rate, message: "" });
                }
            })
            //.sort({date: 1})

    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_new_gold_rate_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        res.render('UGLCO/new_gold_rate', { title: 'New Gold Rate', sectionTitle: "New Gold Rate", sectionMessage: "", mandatorymessage: "", message: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/gold_rate_add_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var goldRate = getUid();
        var rate = req.body.goldRate;
        console.log("new rate : " + rate);
        GoldRateModel.create({ id: goldRate, price: rate }, function(err, gold) {
            if (err) {
                console.log("Err : " + err);
            } else {
                GoldRateModel.find(function(err, rate) {
                    if (err) {

                    } else {
                        res.render('UGLCO/gold_rate_report', { title: 'Gold Rate Report', sectionTitle: "Gold Rate Report", message: "Rate updated successfully..", sectionMessage: "Rate updated successfully..", agents: rate });
                    }
                })

            }
        })
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_edit_gold_rate_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var rate = req.param('id');
        global.updatedId = rate;
        console.log("edit waala : " + rate)
        GoldRateModel.findOne({ id: rate },
            function(err, goldRate) {
                if (err) {
                    console.log("error : " + err);
                } else {
                    console.log("gold rate found : " + JSON.stringify(goldRate));
                    res.render('UGLCO/update_gold_rate', { title: 'Update Gold Rate', sectionTitle: "Update Gold Rate", message: "", mandatorymessage: "", sectionMessage: "", goldRate: goldRate });
                }
            })
    } else {
        res.redirect('sign-in');
    }
})

router.post('/update_gold_rate_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var rate = req.body.goldRate;
        var date = new Date();
        GoldRateModel.update({ id: global.updatedId }, { $set: { price: rate, date: date } }, function(err, rate) {
            if (err) {
                console.log("err in updating rate : " + err);
            } else {
                GoldRateModel.findOne({ id: global.updatedId }, function(err, rate) {
                    if (err) {

                    } else {
                        GoldRateModel.find(function(err, rate) {
                            if (err) {

                            } else {
                                res.render('UGLCO/gold_rate_report', { title: 'Update Gold Rate', sectionTitle: "Update Gold Rate", message: "Rate Updated Successfully", mandatorymessage: "", sectionMessage: "", agents: rate });
                            }
                        })

                    }
                })

            }
        })
    } else {
        res.redirect('sign-in');
    }
})


router.get('/get_upload_page_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        global.GSPCustomerUpload = true;
        global.DCCustomerUpload = false;
        global.GSPPlans = false;
        if (global.uploaded) {
            res.render('UGLCO/bulk_upload', { title: 'Bulk Upload', uploadMessage: "Customer Data Upload", filename: filename, filepath: filepath, message: "Data Uploaded Successfully" });
        } else {
            res.render('UGLCO/bulk_upload', { filename: undefined, title: 'Customer Data Upload', sectionTitle: "Customer Data Upload", uploadMessage: "Customer Data Upload", mandatorymessage: "", sectionMessage: "", message: "Select File to Upload" });
        }

    } else {
        res.redirect('sign-in');
    }
})

//upload customer data

router.get('/CustomerGSP/save_excel/bulkupload/:filename', function(req, res, next) {
    console.log("in GSP Customer data upload");
    filename = req.params.filename;
    console.log("file name : " + filename);
    var csvConverter = new Converter({ constructResult: true });
    pureArray1 = new Array();
    var fileStream = fs.createReadStream("public/excels/" + filename);

    csvConverter.on("end_parsed", function(jsonObj) {
        var length = Object.keys(jsonObj).length;
        console.log("json length : " + Object.keys(jsonObj).length);
        for (var i = 0; i < length; i++) {
            console.log("values : " + JSON.stringify(jsonObj[i]));
            pureArray1.push(jsonObj[i]);
        }
        console.log(pureArray1);
        console.log("Pure Array waala length starting : " + pureArray1.length);
        if (GSPCustsaveInDBBulkUpload()) {
            console.log('DONE');
            // res.redirect('http://localhost:5000/get_upload_page_LCO');
            res.redirect('http://35.154.133.118:5000/get_upload_page_LCO');
            filename = req.params.filename;
            filepath = "http://35.154.133.118:5000/excels/" + filename;
            // filepath = "http://localhost:5000/excels/"+ filename;
            console.log(filepath);
            global.uploaded = true;
        } else {
            filename = undefined;
            global.uploadMessage = "Invalid Excel for Collection Upload";

            //res.redirect('http://localhost:5000/get_upload_page_LCO');
            res.redirect('http://35.154.133.118:5000/get_upload_page_LCO');

            console.log("Invalid Excel");
        }


    });
    fileStream.pipe(csvConverter);
});


//Save the information in the database
function GSPCustsaveInDBBulkUpload() {

    console.log("Customer Details Length " + pureArray1.length);
    for (var i = 0; i < pureArray1.length; i++) {
        var row = pureArray1[i];
        var jsonData = JSON.stringify(row);
       // console.log("LCO id : " + global.LCOId);

        var GSP = new GSPCustomer();
        var planDate = new Date(row.PlanDate);
       // console.log("plan date : " + planDate);
        //console.log("data : "+jsonData);

        var custname = row.CustName;
        GSP.LCOId = global.LCOId;
        GSP.name = row.CustName;
        GSP.customerId = row.CustCode;
        GSP.address = row.Address;
        GSP.phone = row.Phone;
        GSP.mobileNo = row.Mobile;
        GSP.balanceAmount = row.Balance;
        GSP.paidAmt = row.PaidAmt;
        GSP.planName = row.PlanName;
        GSP.planAmt = row.PlanAmt;
        GSP.joinId = row.JoiningId;
        GSP.planDate = new Date(row.PlanDate);
        GSP.dueDate = new Date(row.DueDate);
        GSP.salesman = row.Salesman;
        GSP.emi = row.emi;
        GSP.tenure=row.tenure;
        GSP.emiPaid=row.emiPaid;
        GSP.noofemi=row.noofemi;        ;
        console.log("GSP Data Processing : " + i);
        GSPCustomerModel.create(GSP, function(err, gsp) {
            if (err) {
                errorLogModal.create({
                    LCOId: global.LCOId,
                    custName: GSP.name,
                    error: err
                }, function(err, errorMsg) {
                    if (err) {
                        console.log("Error create");
                    } else {

                    }
                });
                return false;
                console.log("error in creating customer : " + GSP.name + " : " + err);
            } else {
                LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
                    if (err) {
                        errorLogModal.create({
                            LCOId: global.LCOId,
                            custName: GSP.name,
                            error: err
                        }, function(err, errorMsg) {
                            if (err) {
                                console.log("Error");
                            } else {
        
                            }
                        });
                        return false;
                        console.log("error in updating customer : " + GSP.name + " : " + err);
                    } else if (!lco) {
                        return false;
                    } else {
                        console.log("in lco fetch for : " + JSON.stringify(lco));
                        console.log("operator details fo cust " + gsp.name + " is " + lco.LCOId + " ,  " + lco.MSOId + " , " + lco.DistId);

                        GSPCustomerModel.update({ customerId: gsp.customerId, LCOId: global.LCOId }, { $set: { MSOId: lco.MSOId, DistId: lco.DistId } },
                            function(err, update) {
                                if (err) {
                                    errorLogModal.create({
                                        LCOId: global.LCOId,
                                        custName: GSP.name,
                                        error: err
                                    }, function(err, errorMsg) {
                                        if (err) {
                                            console.log("Error update");
                                        } else {
                    
                                        }
                                    });
                                    return false;
                                } else {
                                    console.log("customer Successfully created : " + gsp.name);
                                }
                            })
                    }
                })

            }
        })
    }
    //console.log("Array "+JSON.stringify(pureArray1[0]));

    pureArray1 = [];
    return true;

};

router.get('/get_customer_report_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        GSPCustomerModel.find({ MSOId: global.MSOId }, function(err, cust) {
            if (err) {
                console.log("error : " + err);
            } else {
                res.render('UGLCO/customer_details', { title: 'Subscriber List', message: '', subs: cust, sectionMessage: "", mandatorymessage: "", message: "" });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})


router.get('/get_customer_payment_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        res.render('UGLCO/customer_payment', { title: 'Subscriber Payment', message: '', mandatorymessage: "" });
    } else {
        res.redirect('sign-in');
    }
})

router.post('/cust_payment_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var searchBy = req.body.r
        var search = req.body.search;
        console.log("search by : " + searchBy);
        if (searchBy === "custcode") {
            GSPCustomerModel.findOne({ customerId: search, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error cust code : " + err);
                } else if (!cust) {
                    res.render('UGLCO/customer_payment', { title: 'Subscriber Payment', message: 'Incorrect Id Entered', mandatorymessage: "" });
                    console.log("cust not found");
                } else {
                    // global.cust = cust;
                    global.customer = cust.customerId;
                    if(cust.emi==undefined || cust.emi==0){
                        //cust.emi=0;
                        var emi="NAN";
                        if(cust.planName.includes("15")){
                            emi=parseFloat(cust.planAmt/15);
                        }else if(cust.planName.includes("11")){
                            emi=parseFloat(cust.planAmt)/11;
                        }else if(cust.planName.includes("12")){
                            emi=parseFloat(cust.planAmt/12);
                        }
                        cust.emi=emi;
                    }
                    res.render('UGLCO/payment_details', { title: 'Subscriber Payment', message: '', mandatorymessage: "", sub: cust,sess:sess });
                    console.log("found cust id : " + JSON.stringify(cust));
                }
            })
        } else if (searchBy === "custname") {
            GSPCustomerModel.findOne({ name: search, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error cust name : " + err);
                } else if (!cust) {
                    res.render('UGLCO/customer_payment', { title: 'Subscriber Payment', message: 'Incorrect Customer name Entered', mandatorymessage: "" });
                    console.log("cust not found");
                } else {
                    global.cust = cust;
                    global.customer = cust.customerId;
                    if(cust.emi==undefined || cust.emi==0){
                        cust.emi=0;
                        if(cust.planName.includes("15")){
                            cust.emi=parseFloat(cust.planAmt/15);
                        }else if(cust.planName.includes("11")){
                            cust.emi=parseFloat(cust.planAmt/11);
                        }else if(cust.planName.includes("12")){
                            cust.emi=parseFloat(cust.planAmt/12);
                        }
                    }
                    res.render('UGLCO/payment_details', { title: 'Subscriber Payment', message: '', mandatorymessage: "", sub: cust,sess:sess });
                    console.log("found cust name : " + JSON.stringify(cust));
                }
            })
        } else if (searchBy === "mobile") {
            GSPCustomerModel.findOne({ mobileNo: search, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error cust mobile : " + err);
                } else if (!cust) {
                    res.render('UGLCO/customer_payment', { title: 'Subscriber Payment', message: 'Incorrect Mobile number Entered', mandatorymessage: "" });
                    console.log("cust not found");
                } else {
                    global.cust = cust;
                    global.customer = cust.customerId;
                    if(cust.emi==undefined || cust.emi==0){
                        cust.emi=0;
                        if(cust.planName.includes("15")){
                            cust.emi=parseFloat(cust.planAmt/15);
                        }else if(cust.planName.includes("11")){
                            cust.emi=parseFloat(cust.planAmt/11);
                        }else if(cust.planName.includes("12")){
                            cust.emi=parseFloat(cust.planAmt/12);
                        }
                    }
                    res.render('UGLCO/payment_details', { title: 'Subscriber Payment', message: '', mandatorymessage: "", sub: cust,sess:sess });
                    console.log("found cust mobile : " + JSON.stringify(cust));
                }
            })
        }
    } else {
        res.redirect('sign-in');
    }
})

router.post('/payment_sub_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var sgst, cgst, amt;
        console.log("in customer payment of customer : " + req.body.custId);
        var paidAmt = req.body.paidAmt;
        if(sess.uglco.LCOId=="780002"){

        }else{
            GSPCustomerModel.findOne({ customerId: req.body.custId, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error in finding cust for payment " + err);
                } else if (!cust) {
                    res.render('UGLCO/customer_payment', { title: 'Subscriber Payment', message: 'Customer Not Found', mandatorymessage: "" });
                } else {
                    var EMICUSTOMER;
                    if(cust.emi==undefined || cust.emi==0){
                        cust.emi=0;
                        if(cust.planName.includes("15")){
                            EMICUSTOMER=parseFloat(cust.planAmt)/15;
                        }else if(cust.planName.includes("11")){
                            EMICUSTOMER=parseFloat(cust.planAmt)/11;
                        }else if(cust.planName.includes("12")){
                            EMICUSTOMER=parseFloat(cust.planAmt)/12;
                        }
                    }else{
                        EMICUSTOMER=cust.emi;
                    }
                    console.log(EMICUSTOMER+" and  "+cust.emi);
                    if(parseFloat(paidAmt)!==parseFloat(EMICUSTOMER)){
                        res.render('UGLCO/customer_payment', { title: 'Subscriber Payment', message: 'Paid Amount must equal to EMI', mandatorymessage: "" });
                    }else if(parseFloat(paidAmt)==parseFloat(EMICUSTOMER)){
                        if (parseFloat(paidAmt) <= parseFloat(cust.balanceAmount)) {
                            var gstAmount = (parseFloat(req.body.paidAmt) * 3) / 103;
                            sgst = gstAmount / 2;
                            cgst = gstAmount / 2;
        
        
                            amt = parseFloat(req.body.paidAmt) - sgst - cgst;
                            GSPCustomerPaymentModel.find(function(err, payment) {
                                if (err) {
        
                                } else if (payment.length == 0) {
                                    var receiptId = "00001";
                                    console.log("in 0 : " + receiptId);
                                    var payMode = req.body.r;
                                    var balanceAmount = parseFloat(cust.balanceAmount) - parseFloat(req.body.paidAmt);
                                    console.log("amt : " + req.body.paidAmt + " and " + balanceAmount);
        
                                    console.log("customer : " + JSON.stringify(cust));
                                    global.receipt = receiptId;
                                    var pay = new GSPCustomerayment();
                                    pay.receiptId = receiptId;
                                    pay.payMode = payMode;
                                    pay.MSOId = cust.MSOId;
                                    pay.LCOId = global.LCOId;
                                    pay.DistId = cust.DistId;
                                    pay.customerId = cust.customerId;
                                    pay.name = cust.name;
                                    pay.mobileNo = cust.mobileNo;
                                    pay.balanceAmt = balanceAmount;
                                    pay.paidAmount = req.body.paidAmt;
                                    pay.joinId = cust.joinId;
                                    pay.sgst = sgst;
                                    pay.cgst = cgst;
                                    pay.amt = amt;
                                    pay.planAmt = cust.planAmt;
                                    pay.planName = cust.planName;
                                    if (global.agentLogin) {
                                        pay.agentId = global.AgentId;
                                    } else {
                                        pay.agentId = global.LCOId;
                                    }
        
                                    pay.address = cust.address;
                                    pay.remarks = req.body.remarks;
                                    if (payMode === "Cheque") {
                                        pay.bankName = req.body.bankName;
                                        pay.chequeNo = req.body.chequeNo;
                                        pay.chequeDate = req.body.chequeDate;
                                    }
                                    GSPCustomerModel.update({ customerId: req.body.custId, MSOId: global.MSOId }, { $inc: { balanceAmount: -req.body.paidAmt, paidAmt: req.body.paidAmt } }, function(err, update) {
                                        if (err) {
                                            console.log("Error update : " + err);
                                        } else {
                                            GSPCustomerPaymentModel.create(pay, function(err, payment) {
                                                if (err) {
        
                                                } else {
                                                    GSPLedgerReportModel.create(pay, function(err, ledger) {
                                                        if (err) {
        
                                                        } else {
                                                            LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
                                                                if (err) {
                                                                    res.render('error');
                                                                } else {
                                                                    console.log("payment done successfully");
                                                                    MESSAGE_TEMPLATE = 'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + pay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                        receiptId + '.Your total due amount is Rs.' + pay.balanceAmt + ' as on date ' + moment().format("DD-MM-YYYY");
                                                                    console.log("Template : " + MESSAGE_TEMPLATE);
                                                                    if (lco.msgLimit > 0) {
                                                                        sendSMS(lco, cust.mobileNo, MESSAGE_TEMPLATE);
                                                                        lco.msgLimit = lco.msgLimit - 1;
                                                                    }
                                                                    LCODataModel.update({ LCOId: lco.LCOId }, { $set: { msgLimit: lco.msgLimit } }, function(err, update) {
                                                                        if (err) {
        
                                                                        } else {
                                                                            res.redirect('get_receipt_LCO/' + req.body.custId);
                                                                        }
                                                                    })
        
                                                                }
                                                            })
        
                                                            //res.render('UGLCO/payment_confirmation',{ title: 'Payment Receipt',message:'',mandatorymessage:"" ,sub:payment});
        
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                } else {
                                    var form = payment[0].receiptId;
                                    var integerID = parseInt(form) + 1;
                                    if (integerID < 10) {
                                        integerID = "0000" + integerID;
                                    } else if (integerID < 100) {
                                        integerID = "000" + integerID;
                                    } else if (integerID < 1000) {
                                        integerID = "00" + integerID;
                                    } else if (integerID < 10000) {
                                        integerID = "0" + integerID;
                                    }
                                    var receiptId = "" + integerID;
                                    console.log("in all : " + receiptId);
                                    var payMode = req.body.r;
                                    var paidAmt = req.body.paidAmt;
                                    var balanceAmount = parseFloat(cust.balanceAmount) - parseFloat(paidAmt);
                                    console.log("amt : " + paidAmt + " and " + balanceAmount);
        
                                    console.log("customer : " + JSON.stringify(cust));
                                    global.receipt = receiptId;
                                    var pay = new GSPCustomerayment();
                                    pay.receiptId = receiptId;
                                    pay.payMode = payMode;
                                    pay.MSOId = cust.MSOId;
                                    pay.LCOId = global.LCOId;
                                    pay.DistId = cust.DistId;
                                    pay.customerId = cust.customerId;
                                    pay.name = cust.name;
                                    pay.sgst = sgst;
                                    pay.cgst = cgst;
                                    pay.amt = amt;
                                    pay.joinId = cust.joinId;
                                    pay.mobileNo = cust.mobileNo;
                                    pay.balanceAmt = balanceAmount;
                                    pay.paidAmount = paidAmt;
                                    pay.planAmt = cust.planAmt;
                                    pay.planName = cust.planName;
                                    if (global.agentLogin) {
                                        pay.agentId = global.AgentId;
                                    } else {
                                        pay.agentId = global.LCOId;
                                    }
                                    pay.address = cust.address;
                                    pay.remarks = req.body.remarks;
                                    if (payMode === "Cheque") {
                                        pay.bankName = req.body.bankName;
                                        pay.chequeNo = req.body.chequeNo;
                                        pay.chequeDate = req.body.chequeDate;
                                    }
                                    GSPCustomerModel.update({ customerId: req.body.custId, MSOId: global.MSOId }, { $inc: { balanceAmount: -paidAmt, paidAmt: paidAmt } }, function(err, update) {
                                        if (err) {
        
                                        } else {
                                            GSPCustomerPaymentModel.create(pay, function(err, payment) {
                                                if (err) {
        
                                                } else {
                                                    GSPLedgerReportModel.create(pay, function(err, ledger) {
                                                        if (err) {
        
                                                        } else {
        
                                                            LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
                                                                if (err) {
                                                                    res.render('error');
                                                                } else {
                                                                    console.log("payment done successfully");
                                                                    MESSAGE_TEMPLATE = 'Dear Customer, Thanks your jeweltree monthly installment received of instalment Rs. ' + pay.paidAmount + '/- ' + ' Receipt No. :' +
                                                                        receiptId + '.Your total due amount is Rs.' + pay.balanceAmt + ' as on date ' + moment().format("DD-MM-YYYY");
                                                                    console.log("Template : " + MESSAGE_TEMPLATE);
                                                                    //sendSMS(lco, cust.mobileNo, MESSAGE_TEMPLATE);
                                                                    if (lco.msgLimit > 0) {
                                                                        sendSMS(lco, cust.mobileNo, MESSAGE_TEMPLATE);
                                                                        lco.msgLimit = lco.msgLimit - 1;
                                                                    }
                                                                    LCODataModel.update({ LCOId: lco.LCOId }, { $set: { msgLimit: lco.msgLimit } }, function(err, update) {
                                                                        if (err) {
        
                                                                        } else {
                                                                            res.redirect('get_receipt_LCO/' + req.body.custId);
                                                                        }
                                                                    });
                                                                    //res.redirect('get_receipt_LCO/' + req.body.custId);
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            }).sort({ $natural: -1 }).limit(1);
                        } else {
                            res.render('UGLCO/payment_details', { title: 'Subscriber Payment', message: 'Payment Must be Less than Balance Amount', mandatorymessage: "", sub: global.cust ,sess:sess});
                        }
                    }
                    
    
    
                }
            });
        }

       
    } else {
        res.redirect('sign-in');
    }
})

//Receipt Using Cust Id Params..

router.get('/get_receipt_LCO/:custId', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        GSPCustomerPaymentModel.findOne({ receiptId: global.receipt, customerId: req.params.custId }, function(err, payment) {
            if (err) {

            } else {
                GSPCustomerModel.findOne({ customerId:  req.params.custId, MSOId: global.MSOId }, function(err, customer) {
                    if (err) {

                    } else {
                        LCODataModel.findOne({ LCOId: sess.uglco.LCOId, custType: "UG" }, function(err, lco) {
                            if (err) {

                            } else {
                                MSODataModel.findOne({ MSOId: lco.MSOId, custType: "UG" }, function(err, msoData) {
                                    if (err) {

                                    } else {
                                        console.log("customer : " + JSON.stringify(customer));
                                        res.render('UGLCO/payment_confirmation', { title: 'Payment Receipt', message: '', mandatorymessage: "", sub: payment, lcos: lco, cust: customer, mso: msoData });
                                    }
                                })

                            }
                        })

                    }
                })

            }
        })

    } else {
        res.redirect('sign-in');
    }
})

router.get('/uglco_transaction', function(req, res, next) {
    if (isUGLCOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            var startDate = moment().startOf('day');
            var endDate = moment().endOf('day');

            console.log("dates in undefined " + startDate + " and " + endDate);
        } else {
            var startDate = moment(startDate).startOf('day');
            var endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        //console.log("Date : "+startDate);

        GSPCustomerPaymentModel.find({ LCOId: global.LCOId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('UGLCO/transaction_report', { title: 'Transaction Report', pays: cust, startDate: startDate, endDate: endDate, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});
router.post('/uglco_transaction', function(req, res, next) {
    if (isUGLCOSessionValid(req)) {
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === '' && endDate === '') {
            var startDate = moment().startOf('day');
            var endDate = moment().endOf('day');
            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            var startDate = moment(startDate).startOf('day');
            var endDate = moment(endDate).endOf('day');

            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);

        GSPCustomerPaymentModel.find({ LCOId: global.LCOId, status: "PAID", created_at: { $gte: startDate, $lte: endDate } }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                res.render('UGLCO/transaction_report', { title: 'Transaction Report', pays: cust, startDate: startDate, endDate: endDate, message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }
});

router.get('/uglcosignout', function(req, res, next) {
    var sess = req.session;
    if (sess.uglco != undefined) {
        sess.uglco = undefined;
    } else if (sess.center != undefined) {
        sess.center = undefined;
    }
    res.render('signout');
})

//uplaod GSP plans

router.get('/get_upload_plans_page_LCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        global.GSPCustomerUpload = false;
        global.DCCustomerUpload = false;
        global.GSPPlans = true;
        if (global.uploaded) {
            res.render('UGLCO/bulk_upload', { title: 'Bulk Upload', uploadMessage: "Customer Data Upload", filename: filename, filepath: filepath, message: "Data Uploaded Successfully" });
        } else {
            res.render('UGLCO/bulk_upload', { filename: undefined, title: 'Customer Data Upload', sectionTitle: "Customer Data Upload", uploadMessage: "Customer Data Upload", mandatorymessage: "", sectionMessage: "", message: "Select File to Upload" });
        }
    } else {
        res.render('signout');
    }
})

//add cust

router.get('/add_customer_lco', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        res.render('UGLCO/add_customer', { title: 'Add Customer', sectionTitle: "Add Customer", mandatorymessage: "", sectionMessage: "", message: "Add Customer" });
    } else {
        res.render('signout');
    }
});

router.post('/customer_add_lco', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
            if (err) {
                console.log("lco error : " + err);
            } else {
                var id = req.body.id;
                var name = req.body.name;
                var phone = req.body.phone;
                var mobileNo = req.body.mobileNo;
                var joinId = req.body.joinId;
                var salesman = req.body.salesman;
                var address = req.body.address;
                var planName = req.body.planName;
                var planAmt = req.body.planAmt;
                var planDate = new Date(req.body.planDate);
                var dueDate = new Date(req.body.dueDate);
                var emi = req.body.emi;
                var emiType = req.body.emiType;
                var sgst = req.body.sgst;
                var cgst = req.body.cgst;
                var igst = req.body.igst;
                var cust = new GSPCustomer();
                cust.customerId = id.trim();
                cust.name = name;
                cust.phone = phone;
                cust.mobileNo = mobileNo;
                cust.joinId = joinId;
                cust.address = address;
                cust.salesman = salesman;
                cust.planName = planName;
                cust.planAmt = planAmt;
                cust.sgst = sgst;
                cust.cgst = cgst;
                cust.igst = igst;
                cust.balanceAmount = planAmt;
                cust.planDate = planDate;
                cust.dueDate = dueDate;
                cust.emi = emi;
                cust.emiType = emiType;
                cust.LCOId = global.LCOId;
                cust.planType=req.body.taxType;
                cust.MSOId = lco.MSOId;
                cust.DistId = lco.DistId;
                console.log("cust : " + JSON.stringify(cust));
                GSPCustomerModel.find({ customerId: cust.customerId, LCOId: global.LCOId }, function(err, custid) {
                    if (err) {
                        console.log("err1 : " + err);
                    } else if (custid.length > 0) {
                        res.render('UGLCO/add_customer', { title: 'Add Customer', sectionTitle: "Add Customer", mandatorymessage: "", sectionMessage: "Customer Id already Exists", message: "Add Customer" });
                    } else {
                        GSPCustomerModel.create(cust, function(err, customer) {
                            if (err) {
                                console.log("err2 : " + err);
                            } else {
                                console.log("cust added ");
                                LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
                                    if (err) {

                                    } else {
                                        if (lco.MSOId == "78001") {
                                            // MESSAGE_TEMPLATE = "New Account Created in " + global.LCOId +
                                            //     "\nCustomer Name : " + cust.name + "\n" +
                                            //     "Customer Id : " + cust.customerId + "\n" +
                                            //     "Regards,\nCableguy";
                                            //cust.planName = cust.planName.replace("+", "&#x2b;");

                                            //cust.planName = encodeURI(cust.planName);
                                            if (lco.msgLimit > 0) {
                                                MESSAGE_TEMPLATE = "Hi " + cust.name + ", Punjabi Saraf is delighted" +
                                                    " to open your new Jeweltree plan " + cust.planName + " of Rs. " + cust.emi +
                                                    " /- per Month. Your ID is " + cust.customerId + " for any assistance please " +
                                                    "call Vibha 9893039936 ";
                                                console.log(MESSAGE_TEMPLATE);
                                                MESSAGE_TEMPLATE = encodeURIComponent(MESSAGE_TEMPLATE);
                                                //MESSAGE_TEMPLATE = encodeURI(MESSAGE_TEMPLATE);
                                                console.log(MESSAGE_TEMPLATE);
                                                sendCustomerSMS(lco, cust.mobileNo, MESSAGE_TEMPLATE);
                                                sendCustomerSMS(lco, "9425319448", MESSAGE_TEMPLATE);
                                                sendCustomerSMS(lco, "9893039936", MESSAGE_TEMPLATE);
                                                lco.msgLimit = lco.msgLimit - 3;
                                            }

                                        }else if (lco.MSOId == "78002") {
                                            // MESSAGE_TEMPLATE = "New Account Created in " + global.LCOId +
                                            //     "\nCustomer Name : " + cust.name + "\n" +
                                            //     "Customer Id : " + cust.customerId + "\n" +
                                            //     "Regards,\nCableguy";
                                            //cust.planName = cust.planName.replace("+", "&#x2b;");

                                            //cust.planName = encodeURI(cust.planName);
                                            if (lco.msgLimit > 0) {
                                                MESSAGE_TEMPLATE = "Hi " + cust.name + ", Verma Jewellers is delighted" +
                                                    " to open your new Jeweltree plan " + cust.planName + " of Rs. " + cust.emi +
                                                    " /- per Month. Your ID is " + cust.customerId ;
                                                    // + " for any assistance please " +
                                                    // "call Vibha 9893039936 ";
                                                console.log(MESSAGE_TEMPLATE);
                                                MESSAGE_TEMPLATE = encodeURIComponent(MESSAGE_TEMPLATE);
                                                //MESSAGE_TEMPLATE = encodeURI(MESSAGE_TEMPLATE);
                                                console.log(MESSAGE_TEMPLATE);
                                                sendCustomerSMS(lco, cust.mobileNo, MESSAGE_TEMPLATE);
                                                sendCustomerSMS(lco, "9816400454", MESSAGE_TEMPLATE);
                                                //sendCustomerSMS(lco, "9893039936", MESSAGE_TEMPLATE);
                                                lco.msgLimit = lco.msgLimit - 2;
                                            }

                                        }
                                        LCODataModel.update({ LCOId: lco.LCOId }, { $set: { msgLimit: lco.msgLimit } }, function(err, update) {
                                            if (err) {

                                            } else {
                                                res.render('UGLCO/add_customer', { title: 'Add Customer', sectionTitle: "Add Customer", mandatorymessage: "", sectionMessage: "Customer Added Successfully", message: "Add Customer" });
                                            }
                                        })


                                    }
                                })

                            }
                        });
                    }
                })
            }
        })

    } else {
        res.render('signout');
    }
});
//Chnage Passsword\

router.get('/get_password_UGLCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        res.render('UGLCO/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "" });
    } else {
        res.redirect('sign-in');
    }
});

router.post('/change_password_UGLCO', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var password = req.body.oldpassword;
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;

        if (newpassword !== confirmpassword) {
            res.render('UGLCO/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "Password and confirm password do no match" });
        } else {
            LCOLoginModel.findOne({ LCOId: global.LCOId, custType: "UG" }, function(err, mso) {
                if (err) {

                } else {
                    if (password !== mso.password) {
                        res.render('UGLCO/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "Incorrect Password entered" });
                    } else {
                        LCOLoginModel.update({ LCOId: global.LCOId, custType: "UG" }, { $set: { password: newpassword } }, function(err, msoUpdate) {
                            if (err) {

                            } else {
                                LCODataModel.update({ LCOId: global.LCOId, custType: "UG" }, { $set: { password: newpassword } }, function(err, data) {
                                    if (err) {

                                    } else {
                                        global.changePassword = true;
                                        res.redirect('sign-in');
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    } else {
        res.redirect('sign-in');
    }
});

router.get('/edit_customer', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var customer_id = req.query.cust_id;

        GSPCustomerModel.findOne({ customerId: customer_id, LCOId: global.LCOId }, function(err, customer) {

            if (err) {
                console.log("customer edit error : " + err);
            } else {
                if (global.custEdited) {
                    res.render('UGLCO/edit_customer', { title: 'Edit Customer', sectionTitle: "Edit Customer", sectionMessage: "Customer Updated Successfully", cust: customer });
                } else {
                    res.render('UGLCO/edit_customer', { title: 'Edit Customer', sectionTitle: "Edit Customer", sectionMessage: "", cust: customer });
                }

            }

        });


    } else {
        res.redirect('sign-in');
    }
});


router.post('/customer_edit_lco', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        LCODataModel.findOne({ LCOId: global.LCOId }, function(err, lco) {
            if (err) {
                console.log("lco error : " + err);
            } else {
                var id = req.body.id;
                var name = req.body.name;
                var phone = req.body.phone;
                var mobileNo = req.body.mobileNo;
                var joinId = req.body.joinId;
                var salesman = req.body.salesman;
                var planName = req.body.planName;
                var planAmt = req.body.planAmt;
                var address = req.body.address;
                var planDate = new Date(req.body.planDate);
                var dueDate = new Date(req.body.dueDate);
                var emi = req.body.emi;
                var paidAmt = req.body.paidAmt;
                var totalPlanAmt = req.body.total;
                var emiType = req.body.emiType;
                var sgst = req.body.sgst;
                var cgst = req.body.cgst;
                var igst = req.body.igst;
                var cust = new GSPCustomer();
                cust.customerId = id.trim();
                cust.name = name;
                cust.phone = phone;
                cust.mobileNo = mobileNo;
                cust.joinId = joinId;
                cust.salesman = salesman;
                cust.planName = planName;
                cust.planAmt = totalPlanAmt;
                cust.sgst = sgst;
                cust.cgst = cgst;
                cust.igst = igst;
                cust.address = address;
                cust.paidAmt = paidAmt;
                cust.balanceAmount = parseFloat(totalPlanAmt) - parseFloat(paidAmt);
                cust.planDate = planDate;
                cust.dueDate = dueDate;
                cust.emi = emi;
                cust.emiType = emiType;
                cust.LCOId = global.LCOId;
                cust.MSOId = lco.MSOId;
                cust.DistId = lco.DistId;
                console.log("cust : " + JSON.stringify(cust));
                GSPCustomerModel.find({ customerId: id, LCOId: global.LCOId }, function(err, custid) {
                    if (err) {
                        console.log("err1 : " + err);
                    } else {
                        GSPCustomerModel.update({ customerId: id, LCOId: global.LCOId }, cust, function(err, customer) {
                            if (err) {
                                console.log("err2 : " + err);
                            } else if (!customer) {
                                global.custEdited = false;
                                res.redirect('edit_customer?cust_id=' + id);
                            } else {
                                global.custEdited = true;
                                res.redirect('edit_customer?cust_id=' + id);

                            }
                        });
                    }
                })
            }
        })
    } else {
        res.redirect('sign-in');
    }
})


/**
 * For Customer delete from Admin Panel On 14 Sep 2017
 */

router.get('/lco_customer_delete', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        res.render('UGLCO/customer_delete', { title: "", sectionTitle: "Customer List", sectionMessage: "" })
    } else {
        res.redirect('sign-in');
    }
});

router.post('/lco_customer_delete', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var custId = req.body.custId;
        GSPCustomerModel.find({ customerId: custId }, function(err, cust) {
            if (err) {
                res.status(200).json({ success: false, msg: "Server Error" });
            } else if (cust.length == 0) {
                res.status(200).json({ success: false, msg: "No Customer Found" });
            } else {
                res.status(200).json({ success: true, data: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});

router.get('/lco_delete_customer/:cust_id/:MSOId', function(req, res, next) {
    var sess = req.session;
    if (isUGLCOSessionValid(req)) {
        var cust_id = req.params.cust_id;
        var mso_id = req.params.MSOId;
        // ErrorLogModel.create({cust})
        GSPCustomerModel.remove({ customerId: cust_id, MSOId: mso_id }, function(err, deleteCust) {
            if (err) {
                res.status(200).json({ success: false, msg: "Error While removing customer .. please try again" });
            } else if (!deleteCust) {
                res.status(200).json({ success: true, msg: "Error While removing customer .. please try again" })
            } else {

                GSPCustomerModel.find({ customerId: cust_id }, function(err, cust) {
                    if (err) {
                        res.status(200).json({ success: false, msg: "Server Error" });
                    } else {
                        res.status(200).json({ success: true, msg: "Customer Removed Successfully", data: [] })
                    }
                })
            }
        })
    } else {
        res.redirect('sign-in');
    }
})


module.exports = router;