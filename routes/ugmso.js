var express = require('express');
var router = express.Router();
var fs = require("fs");
var MSOLoginModel = require('../models/MSOLogin.js');
var MSODataModel = require('../models/MSOData.js');
var UGMSOLoginModel = require('../models/UGMSOLogin.js');
var UGLCOLoginModel = require('../models/UGLCOLogin.js');
var UGAgentLoginModel = require('../models/UGAgentLogin.js');
var UGDistLoginModel = require('../models/UGDistLogin.js');
var LedgerReportModel = require('../models/LedgerReport.js');
var GSPCustomerModel = require('../models/GSPCustomerDetails.js');
var AdminLoginModel = require('../models/AdminLogin.js');
var VersionModel = require('../models/Version.js');
var GoldRateModel = require('../models/GoldRate.js');
var LCOLoginModel = require('../models/LCOLogin.js');
var LCODataModel = require('../models/LCOData.js');
var MessageLogModel = require('../models/MessageLogs.js');
var GSPLedgerReportModel = require('../models/GSPLedgerReport.js');
var GSPCustomerPaymentModel = require('../models/GSPCustomerPayment.js');
var pureArray1 = new Array();
var pureArray = new Array();
var getUid = require('get-uid');
var Converter = require("csvtojson").Converter;
var converter = new Converter({ constructResult: false });

function isUGMSOSessionValid(req) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (!(sess.ugmso == undefined)) {
        console.log("req body " + JSON.stringify(req.body));
        global.MSOId = sess.ugmso.MSOId;
        global.name = sess.ugmso.name;
        loginDate = sess.ugmso.date;
        return true;
    } else {
        return false;
    }
}

function GSPCustomer() {

};

function GSPCustomerayment() {

};
router.get('/dashboard_UGMSO', function(req, res, next) {
    var sess = req.session;
    console.log(JSON.stringify(req.session));
    if (isUGMSOSessionValid(req)) {



        res.render('UGMSO/index', {
            title: 'GSP',
            message: global.message,
            name: "" //extra added for avoiding crash
        });

    } else {
        res.redirect('/');
    }
});

router.get('/get_customer_report_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        LCODataModel.find({ custType: "UG", MSOId: global.MSOId }, function(err, lco) {
            if (err) {
                console.log("Error " + err);
            } else {
                res.render('UGMSO/lco_customerlist', { title: 'Customer Report', sectionTitle: "Customer Report", lco: lco, sectionMessage: "", mandatorymessage: "", message: "" });
            }
        })
    } else {
        res.redirect('/sign-in');
    }
})

router.get('/ugmso_customer', function(req, res) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var lcoId = req.param('lcoId');
        console.log(lcoId);
        GSPCustomerModel.find({ MSOId: global.MSOId }, function(err, cust) {
            if (err) {
                console.log(err);
            } else {
                res.render('UGMSO/subscriber_list', { title: 'Customer Report', subs: cust, sectionMessage: "", mandatorymessage: "", message: "" });

            }
        });
    } else {
        res.redirect('/sign-in');
    }

});

router.get('/get_transaction_report_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        LCODataModel.find({ custType: "UG", MSOId: global.MSOId }, function(err, lco) {
            if (err) {
                console.log("Error " + err);
            } else {
                res.render('UGMSO/lco_trans', { title: 'Transaction Report', sectionTitle: "Transaction Report", lco: lco, sectionMessage: "", mandatorymessage: "", message: "" });
            }
        })
    } else {
        res.redirect('/sign-in');
    }
})

router.get('/ugmso_transaction', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var lcoId = req.param('lcoId');
        global.selectedLCO = lcoId;
        console.log(lcoId);
        console.log("LCO : " + global.selectedLCO);
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            startDate = moment().startOf('day');
            endDate = moment().endOf('day');

            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            startDate = moment(startDate).startOf('day');
            endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);
        // global.selectedLCO=req.body.lcoValue;
        // console.log("Selected LCO : "+global.selectedLCO);
        //Changes by RAJ #1
        //Changed from LCOId: global.selectedLCO to MSOId : global.MSOId
        GSPCustomerPaymentModel.find({ LCOId: global.selectedLCO, status: "PAID", created_at: { $gte: startDate, $lte: endDate } },
            function(err, cust) {
                if (err) {
                    console.log(err);
                } else {
                    startDate = moment(startDate).format("YYYY-MM-DD");
                    endDate = moment(endDate).format("YYYY-MM-DD");

                    res.render('UGMSO/transaction_list', { title: 'Transaction Report', startDate: startDate, endDate: endDate, pays: cust, sectionMessage: "", mandatorymessage: "", message: "" });

                }
            });
    } else {
        res.redirect('/sign-in');
    }

});
router.post('/ugmso_transaction', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        console.log("LCO : " + global.selectedLCO);
        var startDate = req.body.startDate;
        var endDate = req.body.endDate;
        console.log("dates " + startDate + " and " + endDate);
        if (startDate === undefined && endDate === undefined) {
            startDate = moment().startOf('day');
            endDate = moment().endOf('day');

            console.log("dates in undefined" + startDate + " and " + endDate);
        } else {
            startDate = moment(startDate).startOf('day');
            endDate = moment(endDate).endOf('day');
            console.log("dates " + startDate + " and " + endDate);
        }
        console.log("Date : " + startDate);
        // global.selectedLCO=req.body.lcoValue;
        // console.log("Selected LCO : "+global.selectedLCO);
        GSPCustomerPaymentModel.find({ LCOId: global.selectedLCO, status: "PAID", created_at: { $gte: startDate, $lte: endDate } },
            function(err, cust) {
                if (err) {
                    console.log(err);
                } else {
                    startDate = moment(startDate).format("YYYY-MM-DD");
                    endDate = moment(endDate).format("YYYY-MM-DD");

                    res.render('UGMSO/transaction_list', { title: 'Transaction Report', startDate: startDate, endDate: endDate, pays: cust, sectionMessage: "", mandatorymessage: "", message: "" });

                }
            });
    } else {
        res.redirect('/sign-in');
    }

});


router.get('/get_customer_payment_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        res.render('UGMSO/customer_payment', { title: 'Subscriber Payment', message: '', mandatorymessage: "" });
    } else {
        res.redirect('sign-in');
    }
});

router.post('/cust_payment_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var searchBy = req.body.r
        var search = req.body.search;
        console.log("search by : " + searchBy);
        if (searchBy === "custcode") {
            GSPCustomerModel.findOne({ customerId: search, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error cust code : " + err);
                } else if (!cust) {
                    res.render('UGMSO/customer_payment', { title: 'Subscriber Payment', message: 'Incorrect Id Entered', mandatorymessage: "" });
                    console.log("cust not found");
                } else {
                    global.customer = cust.customerId;
                    res.render('UGMSO/payment_details', { title: 'Subscriber Payment', message: '', mandatorymessage: "", sub: cust });
                    console.log("found cust id : " + JSON.stringify(cust));
                }
            })
        } else if (searchBy === "custname") {
            GSPCustomerModel.findOne({ name: search, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error cust name : " + err);
                } else if (!cust) {
                    res.render('UGMSO/customer_payment', { title: 'Subscriber Payment', message: 'Incorrect Customer name Entered', mandatorymessage: "" });
                    console.log("cust not found");
                } else {
                    global.customer = cust.customerId;
                    res.render('UGMSO/payment_details', { title: 'Subscriber Payment', message: '', mandatorymessage: "", sub: cust });
                    console.log("found cust name : " + JSON.stringify(cust));
                }
            })
        } else if (searchBy === "mobile") {
            GSPCustomerModel.findOne({ mobileNo: search, MSOId: global.MSOId }, function(err, cust) {
                if (err) {
                    console.log("error cust mobile : " + err);
                } else if (!cust) {
                    res.render('UGMSO/customer_payment', { title: 'Subscriber Payment', message: 'Incorrect Mobile number Entered', mandatorymessage: "" });
                    console.log("cust not found");
                } else {
                    global.customer = cust.customerId;
                    res.render('UGMSO/payment_details', { title: 'Subscriber Payment', message: '', mandatorymessage: "", sub: cust });
                    console.log("found cust mobile : " + JSON.stringify(cust));
                }
            })
        }
    } else {
        res.redirect('sign-in');
    }
});

router.post('/payment_sub_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        console.log("in customer payment of customer : " + global.customer);
        GSPCustomerModel.findOne({ customerId: global.customer, MSOId: global.MSOId }, function(err, cust) {
            if (err) {
                console.log("error in finding cust for payment " + err);
            } else {
                GSPCustomerPaymentModel.find(function(err, payment) {
                    if (err) {

                    } else if (payment.length == 0) {
                        var receiptId = "00001";
                        console.log("in 0 : " + receiptId);
                        var payMode = req.body.r;
                        var paidAmt = req.body.paidAmt;
                        var balanceAmount = parseFloat(cust.balanceAmount) - parseFloat(paidAmt);
                        console.log("amt : " + paidAmt + " and " + balanceAmount);

                        console.log("customer : " + JSON.stringify(cust));
                        global.receipt = receiptId;
                        var pay = new GSPCustomerayment();
                        pay.receiptId = receiptId;
                        pay.payMode = payMode;
                        pay.MSOId = cust.MSOId;
                        pay.LCOId = cust.LCOId;
                        pay.DistId = cust.DistId;
                        pay.customerId = cust.customerId;
                        pay.name = cust.name;
                        pay.mobileNo = cust.mobileNo;
                        pay.balanceAmt = balanceAmount;
                        pay.paidAmount = paidAmt;
                        pay.joinId = cust.joinId;
                        pay.planAmt = cust.planAmt;
                        pay.planName = cust.planName;
                        pay.agentId = global.MSOId;
                        pay.address = cust.address;
                        pay.remarks = req.body.remarks;
                        if (payMode === "Cheque") {
                            pay.bankName = req.body.bankName;
                            pay.chequeNo = req.body.chequeNo;
                            pay.chequeDate = req.body.chequeDate;
                        }
                        GSPCustomerModel.update({ customerId: cust.customerId, MSOId: global.MSOId }, { $inc: { balanceAmount: -paidAmt, paidAmt: paidAmt } }, function(err, update) {
                            if (err) {

                            } else {
                                GSPCustomerPaymentModel.create(pay, function(err, payment) {
                                    if (err) {

                                    } else {
                                        GSPLedgerReportModel.create(pay, function(err, ledger) {
                                            if (err) {

                                            } else {

                                                //res.render('UGLCO/payment_confirmation',{ title: 'Payment Receipt',message:'',mandatorymessage:"" ,sub:payment});
                                                console.log("payment done successfully");
                                                res.redirect('get_receipt_LCO');
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    } else {
                        var form = payment[0].receiptId;
                        var integerID = parseInt(form) + 1;
                        if (integerID < 10) {
                            integerID = "0000" + integerID;
                        } else if (integerID < 100) {
                            integerID = "000" + integerID;
                        } else if (integerID < 1000) {
                            integerID = "00" + integerID;
                        } else if (integerID < 10000) {
                            integerID = "0" + integerID;
                        }
                        var receiptId = "" + integerID;
                        console.log("in all : " + receiptId);
                        var payMode = req.body.r;
                        var paidAmt = req.body.paidAmt;
                        var balanceAmount = parseFloat(cust.balanceAmount) - parseFloat(paidAmt);
                        console.log("amt : " + paidAmt + " and " + balanceAmount);

                        console.log("customer : " + JSON.stringify(cust));
                        global.receipt = receiptId;
                        var pay = new GSPCustomerayment();
                        pay.receiptId = receiptId;
                        pay.payMode = payMode;
                        pay.MSOId = cust.MSOId;
                        pay.LCOId = cust.LCOId;
                        pay.DistId = cust.DistId;
                        pay.customerId = cust.customerId;
                        pay.name = cust.name;
                        pay.joinId = cust.joinId;
                        pay.mobileNo = cust.mobileNo;
                        pay.balanceAmt = balanceAmount;
                        pay.paidAmount = paidAmt;
                        pay.planAmt = cust.planAmt;
                        pay.planName = cust.planName;
                        pay.agentId = global.MSOId;
                        pay.address = cust.address;
                        pay.remarks = req.body.remarks;
                        if (payMode === "Cheque") {
                            pay.bankName = req.body.bankName;
                            pay.chequeNo = req.body.chequeNo;
                            pay.chequeDate = req.body.chequeDate;
                        }
                        GSPCustomerModel.update({ customerId: cust.customerId, MSOId: global.MSOId }, { $inc: { balanceAmount: -paidAmt, paidAmt: paidAmt } }, function(err, update) {
                            if (err) {

                            } else {
                                GSPCustomerPaymentModel.create(pay, function(err, payment) {
                                    if (err) {

                                    } else {
                                        GSPLedgerReportModel.create(pay, function(err, ledger) {
                                            if (err) {

                                            } else {

                                                //res.render('UGLCO/payment_confirmation',{ title: 'Payment Receipt',message:'',mandatorymessage:"" ,sub:payment});
                                                console.log("payment done successfully");
                                                res.redirect('get_receipt_MSO');
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }
                }).sort({ $natural: -1 }).limit(1);


            }
        });
    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_receipt_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        GSPCustomerPaymentModel.findOne({ receiptId: global.receipt }, function(err, payment) {
            if (err) {

            } else {
                GSPCustomerModel.findOne({ customerId: payment.customerId }, function(err, customer) {
                    if (err) {

                    } else {
                        LCODataModel.findOne({ LCOId: customer.LCOId, custType: "UG" }, function(err, lco) {
                            if (err) {

                            } else {
                                console.log("customer : " + JSON.stringify(customer));
                                res.render('UGMSO/payment_confirmation', { title: 'Payment Receipt', message: '', mandatorymessage: "", sub: payment, lcos: lco, cust: customer });
                            }
                        })

                    }
                })

            }
        })

    } else {
        res.redirect('sign-in');
    }
})

router.get('/get_password_UGMSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        res.render('UGMSO/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "" });
    } else {
        res.redirect('sign-in');
    }
});

router.post('/change_password_UGMSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var password = req.body.oldpassword;
        var newpassword = req.body.newpassword;
        var confirmpassword = req.body.confirmpassword;

        if (newpassword !== confirmpassword) {
            res.render('UGMSO/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "Password and confirm password do no match" });
        } else {
            MSOLoginModel.findOne({ MSOId: global.MSOId, custType: "UG" }, function(err, mso) {
                if (err) {

                } else {
                    if (password !== mso.password) {
                        res.render('UGMSO/change_password', { title: 'Change Password', sectionTitle: "Change Password", sectionMessage: "Incorrect Password entered" });
                    } else {
                        MSOLoginModel.update({ MSOId: global.MSOId, custType: "UG" }, { $set: { password: newpassword } }, function(err, msoUpdate) {
                            if (err) {

                            } else {
                                MSODataModel.update({ MSOId: global.MSOId, custType: "UG" }, { $set: { password: newpassword } }, function(err, data) {
                                    if (err) {

                                    } else {
                                        global.changePassword = true;
                                        res.redirect('sign-in');
                                    }
                                });
                            }
                        });
                    }
                }
            });
        }
    } else {
        res.redirect('sign-in');
    }
});

//For User wise collection report

var async = require('asyncawait/async');
var await = require('asyncawait/await');

router.get('/get_user_details_coll_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var startDate = moment().startOf('day').toISOString();
        var endDate = moment().endOf('day').toISOString();

        console.log("Start Date : " + startDate);
        console.log("End Date : " + endDate);
        // GSPCustomerPaymentModel.find({
        //     MSOId: global.MSOId,
        //     status: "PAID",
        //     created_at: { $gte: startDate, $lt: endDate }
        // }, function(err, data) {
        //     if (err) {
        //         console.log("Error : " + err);
        //     } else {
        //         console.log("Data Found : " + JSON.stringify(data));
        //     }
        // });
        GSPCustomerPaymentModel.aggregate([{
                $match: {
                    MSOId: global.MSOId,
                    status: "PAID",
                    created_at: { $gte: new Date(startDate), $lt: new Date(endDate) }
                }
            },
            {
                $group: {
                    _id: "$agentId",
                    totalCollection: { $sum: '$paidAmount' },
                    count: { $sum: 1 }
                }
            }
        ], function(err, user) {
            if (err) {

            } else {

                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                var totalPaidAmt = 0;
                var totalCount = 0;
                for (var i = 0; i < user.length; i++) {
                    var paidAmount = user[i].totalCollection;
                    totalPaidAmt = parseFloat(totalPaidAmt) + parseFloat(paidAmount);
                    totalCount = parseInt(totalCount) + parseInt(user[i].count);

                }
                //user.push({ "_id": "All", "totalCollection": totalPaidAmt, "count": totalCount });
                console.log("Data : " + JSON.stringify(user));
                res.render('UGMSO/user_trans_list', { title: 'Transaction Report', sectionTitle: "Transaction Report", user: user, startDate: startDate, endDate: endDate, sectionMessage: "", mandatorymessage: "", message: "" });
            }
        })

    } else {
        res.redirect('sign-in');
    }
});


router.post('/get_user_details_coll_MSO', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var startDate = moment(req.body.startDate).startOf('day').toISOString();
        var endDate = moment(req.body.endDate).endOf('day').toISOString();

        console.log("Start Date : " + startDate);
        console.log("End Date : " + endDate);
        // GSPCustomerPaymentModel.find({
        //     MSOId: global.MSOId,
        //     status: "PAID",
        //     created_at: { $gte: startDate, $lt: endDate }
        // }, function(err, data) {
        //     if (err) {
        //         console.log("Error : " + err);
        //     } else {
        //         console.log("Data Found : " + JSON.stringify(data));
        //     }
        // });
        GSPCustomerPaymentModel.aggregate([{
                $match: {
                    MSOId: global.MSOId,
                    status: "PAID",
                    created_at: { $gte: new Date(startDate), $lt: new Date(endDate) }
                }
            },
            {
                $group: {
                    _id: "$agentId",
                    totalCollection: { $sum: '$paidAmount' },
                    count: { $sum: 1 }

                }
            }
        ], function(err, user) {
            if (err) {

            } else {

                startDate = moment(startDate).format("YYYY-MM-DD");
                endDate = moment(endDate).format("YYYY-MM-DD");
                var totalPaidAmt = 0;
                var totalCount = 0;
                console.log("User")
                for (var i = 0; i < user.length; i++) {
                    var paidAmount = user[i].totalCollection;
                    totalPaidAmt = parseFloat(totalPaidAmt) + parseFloat(paidAmount);
                    totalCount = parseInt(totalCount) + parseInt(user[i].count);

                }
                //user.push({ "_id": "All", "totalCollection": totalPaidAmt, "count": totalCount });
                console.log("Data : " + JSON.stringify(user));
                res.render('UGMSO/user_trans_list', { title: 'Transaction Report', sectionTitle: "Transaction Report", user: user, startDate: startDate, endDate: endDate, sectionMessage: "", mandatorymessage: "", message: "" });
            }
        })

    } else {
        res.redirect('sign-in');
    }
});


router.get('/user_collection_report', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var Request = req.query;
        var userId = Request.userId;
        var startDate = moment(Request.startDate).startOf('day').toISOString();
        var endDate = moment(Request.endDate).endOf('day').toISOString();
        var pagingFlag = true;

        console.log("User : " + userId);
        console.log("SD : " + startDate);
        console.log("ED : " + endDate);
        var reqParam = {
            "agentId": userId,
            "MSOId": sess.ugmso.MSOId,
            "created_at": { $gte: new Date(startDate), $lt: new Date(endDate) }
        }
        if (userId == "All") {
            pagingFlag = false;
            reqParam = {
                "MSOId": sess.ugmso.MSOId,
                "created_at": { $gte: new Date(startDate), $lt: new Date(endDate) }
            }
        }
        GSPCustomerPaymentModel.find(
            reqParam,
            function(err, report) {
                if (err) {

                } else {
                    //pays
                    res.render('UGMSO/user_transaction_list', { title: 'Transaction Report', startDate: startDate, endDate: endDate, pays: report, sectionMessage: "", mandatorymessage: "", message: "", paging: pagingFlag });
                }
            })

    } else {
        res.redirect('sign-in');
    }
})


// For Reprint Option


router.get('/payment_receipt', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        //duplicate_rec_search
        res.render('UGMSO/duplicate_rec_search', { title: 'Duplicate Payment Receipt', sectionTitle: "Duplicate Payment Receipt", sectionMessage: "", mandatorymessage: "" });
    } else {
        res.redirect('sign-in');
    }
})


router.get('/edit_customer_mso', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var customer_id = req.query.cust_id;

        GSPCustomerModel.findOne({ customerId: customer_id, MSOId: global.MSOId }, function(err, customer) {

            if (err) {
                console.log("customer edit error : " + err);
            } else {
                if (global.custEdited) {
                    res.render('UGMSO/edit_customer', { title: 'Edit Customer', sectionTitle: "Edit Customer", sectionMessage: "Customer Updated Successfully", cust: customer });
                } else {
                    res.render('UGMSO/edit_customer', { title: 'Edit Customer', sectionTitle: "Edit Customer", sectionMessage: "", cust: customer });
                }

            }

        });


    } else {
        res.redirect('sign-in');
    }
});


router.post('/customer_edit_mso', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        GSPCustomerModel.findOne({ customerId: req.body.id, MSOId: global.MSOId }, function(err, lcoData) {
            if (err) {

            } else {
                LCODataModel.findOne({ LCOId: lcoData.LCOId }, function(err, lco) {
                    if (err) {
                        console.log("lco error : " + err);
                    } else {
                        var id = req.body.id;
                        var name = req.body.name;
                        var phone = req.body.phone;
                        var mobileNo = req.body.mobileNo;
                        var joinId = req.body.joinId;
                        var salesman = req.body.salesman;
                        var planName = req.body.planName;
                        var planAmt = req.body.planAmt;
                        var planDate = new Date(req.body.planDate);
                        var dueDate = new Date(req.body.dueDate);
                        var emi = req.body.emi;
                        var paidAmt = req.body.paidAmt;
                        var totalPlanAmt = req.body.total;
                        var emiType = req.body.emiType;
                        var sgst = req.body.sgst;
                        var cgst = req.body.cgst;
                        var igst = req.body.igst;
                        var cust = new GSPCustomer();
                        cust.customerId = id.trim();
                        cust.name = name;
                        cust.phone = phone;
                        cust.mobileNo = mobileNo;
                        cust.joinId = joinId;
                        cust.salesman = salesman;
                        cust.planName = planName;
                        cust.planAmt = totalPlanAmt;
                        cust.sgst = sgst;
                        cust.cgst = cgst;
                        cust.igst = igst;
                        cust.paidAmt = paidAmt;
                        cust.balanceAmount = parseFloat(totalPlanAmt) - parseFloat(paidAmt);
                        cust.planDate = planDate;
                        cust.dueDate = dueDate;
                        cust.emi = emi;
                        cust.emiType = emiType;
                        cust.LCOId = lcoData.LCOId;
                        cust.MSOId = sess.ugmso.MSOId;
                        cust.DistId = lco.DistId;
                        console.log("cust : " + JSON.stringify(cust));
                        GSPCustomerModel.find({ customerId: id, LCOId: lcoData.LCOId }, function(err, custid) {
                            if (err) {
                                console.log("err1 : " + err);
                            } else {
                                GSPCustomerModel.update({ customerId: id, LCOId: lcoData.LCOId }, cust, function(err, customer) {
                                    if (err) {
                                        console.log("err2 : " + err);
                                    } else if (!customer) {
                                        global.custEdited = false;
                                        res.redirect('edit_customer_mso?cust_id=' + id);
                                    } else {
                                        global.custEdited = true;
                                        res.redirect('edit_customer_mso?cust_id=' + id);

                                    }
                                });
                            }
                        })
                    }
                })
            }
        })

    } else {
        res.redirect('sign-in');
    }
})

/**
 * For Customer delete from Admin Panel On 14 Sep 2017
 */

router.get('/mso_customer_delete', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        res.render('UGMSO/customer_delete', { title: "", sectionTitle: "Customer List", sectionMessage: "" })
    } else {
        res.redirect('sign-in');
    }
});

router.post('/mso_customer_delete', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var custId = req.body.custId;
        GSPCustomerModel.find({ customerId: custId }, function(err, cust) {
            if (err) {
                res.status(200).json({ success: false, msg: "Server Error" });
            } else if (cust.length == 0) {
                res.status(200).json({ success: false, msg: "No Customer Found" });
            } else {
                res.status(200).json({ success: true, data: cust });
            }
        })
    } else {
        res.redirect('sign-in');
    }
});

router.get('/mso_delete_customer/:cust_id/:MSOId', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        var cust_id = req.params.cust_id;
        var mso_id = req.params.MSOId;
        // ErrorLogModel.create({cust})
        GSPCustomerModel.remove({ customerId: cust_id, MSOId: mso_id }, function(err, deleteCust) {
            if (err) {
                res.status(200).json({ success: false, msg: "Error While removing customer .. please try again" });
            } else if (!deleteCust) {
                res.status(200).json({ success: true, msg: "Error While removing customer .. please try again" })
            } else {

                GSPCustomerModel.find({ customerId: cust_id }, function(err, cust) {
                    if (err) {
                        res.status(200).json({ success: false, msg: "Server Error" });
                    } else {
                        res.status(200).json({ success: true, msg: "Customer Removed Successfully", data: [] })
                    }
                })
            }
        })
    } else {
        res.redirect('sign-in');
    }
})


router.get('/message_log', function(req, res, next) {
    var sess = req.session;
    if (isUGMSOSessionValid(req)) {
        MessageLogModel.find({ MSOId: sess.ugmso.MSOId}, function(err, msgLog) {
            if (err) {
                console.log("err " + err);
            } else {
                res.render('UGMSO/message_log', { title: '', sectionTitle: 'Message Log', msg: msgLog, sectionMessage: 'Message Log' });
            }
        })
    } else {
        res.redirect('sign-in');
    }
})


module.exports = router;